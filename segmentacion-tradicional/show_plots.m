%function show_plots(plot, imgs, window_bound, titles)
function show_plots(plot, imgs, w_b, titles)

    if ~exist('titles','var')     
          %titles = repmat(' ',[1 length(imgs)]);
          titles = cell(length(imgs));
    end
    if ~exist('w_b','var')     
          w_b = [.1 .8 .1 .8];
    end

    monitor_dim = get(0, 'MonitorPositions');  % get the monitor dimension
    %cuanto se desplaza a la derecha
    %cuanto se mueve perdo desde abajo
    %ancho
    %alto
    figure_position = [monitor_dim(3)*w_b(1),monitor_dim(4)*w_b(3),monitor_dim(3)*w_b(2),monitor_dim(4)*w_b(4)];
    
    figure('position',figure_position);    
    for i=1:length(imgs) 
        if(i == 1)
            subaxis(plot(1),plot(2),1,'SH',0,'SV',0,'M',0,'PT',.02);
        else
            subaxis(i);        
        end    
        imshow(imgs{i},[]);
        title(sprintf('%s',titles{i})); 
    end
end

