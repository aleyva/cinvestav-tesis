function J = getcontour(I,phi_)
id = phi_<=0;
R = I;
G = I;
B = I;
R(id) = 1;
G(id) = 0;
B(id) = 0;
J = cat(3,R,G,B);