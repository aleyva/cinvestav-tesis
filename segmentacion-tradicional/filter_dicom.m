%% filter a dicom image with bound threshold
function img = filter_dicom(dicom, bound)    
    dicom(dicom<bound(1)) = 0;
    dicom(dicom>bound(2)) = 0;
    dicom = dicom - bound(1) - 1; 
    img = uint8(255*mat2gray(dicom));
end