%%
close all;
clear all;

path = './';
img_name = 'dicom/IMG-004.dcm';

% files = dir(fullfile(path,'*.tif')); 
file = fullfile(path,img_name); 

% read image
dicom = dicomread(file);

%% Segmentation by Houndsfield
% define limits to clean not interesting information
i_muscle = filter_dicom(dicom,[950,1120]); 
i_bone = filter_dicom(dicom,[1100,2300]); 
i_all = filter_dicom(dicom,[950,2300]); 

if(0)
    %function show_plots(plot, imgs, window_bound, titles)   
    images = {dicom, i_muscle, i_bone, i_all};
    titles = {'DICOM', 'muscle','bone', 'muscle and bone'};
    window_bound = [.3 .45 .5 .85];
    show_plots([2 2],images,window_bound,titles);
end

%% define morfological structured elements
se1 = strel('disk',1);
se2 = strel('disk',2);
se3 = strel('disk',3);
se4 = strel('disk',4);
se5 = strel('disk',5);
se6 = strel('disk',6);


%% find the interest region and crop the original image

i_region = i_all;

%quitar lineas de ruido (las de la mesa)
i_region = imclose(i_region, se2);
%figure, imshow(i_region);
i_region = imopen(i_region, se6);
%figure, imshow(i_region);
i_region(i_region>0) = 1;

% calculate the crop region and crop the original image
% Get all the blob properties.  
% Can only pass in originalImage in version R2008a and later.
mucle_prop = regionprops(i_region, 'BoundingBox');
crop_rect = mucle_prop.BoundingBox + [-10,-5,20,10];

i_muscle_crop = imcrop(i_muscle, crop_rect);
i_bone_crop = imcrop(i_bone, crop_rect);
i_all_crop = imcrop(i_all, crop_rect);

if(0)
    %function show_plots(plot, imgs, window_bound, titles)   
    images = {i_muscle_crop, i_bone_crop, i_all_crop};
    titles = {'muscle','bone', 'muscle and bone'};
    window_bound = [.1 .85 .35 .5];
    show_plots([1 3],images,window_bound,titles);
end

%% Fue eliminada una parte de operaciones morfologicas


%% substract the bone of the original img
i_bone_bw = i_bone_crop;
i_bone_bw(i_bone_bw>0) = 1;

%quitar lineas de ruido
i_bone_mf = imopen(i_bone_bw, se1);
i_bone_mf = imfill(i_bone_mf,'holes');
%figure, imshow(i_bone_mf, [0 1]);

i_muscle_filt = i_muscle_crop .* uint8(~i_bone_mf);
%figure, imshow(i_muscle_filt);


%% segmenta el musculo

%morfologico
img_morf1 = i_muscle_filt;    

img_morf1 = imopen(img_morf1, se3);
img_morf2 = imfill(img_morf1,'holes');

img_morf2(img_morf2~=0)=1;
img_morf2 = imerode(img_morf2, se1);

% labeled the bit image
img_lab = bwlabel(img_morf2);

if(0)
    %function show_plots(plot, imgs, window_bound, titles)   
    images = {i_muscle_filt, img_morf1, img_morf2, img_lab};
    titles = {'i_muscle_filt', 'img_morf1','img_morf2', 'img_lab and bone'};
    window_bound = [.12 .77 .1 .8]; % width, height
    show_plots([2 2],images,window_bound,titles);    
end

%% chan-vese

% leer la mascara
mask = imread('mask/mask4.jpg');
[mask_m,mask_n,z] = size(i_muscle_filt);
mask = imresize(mask,[mask_m mask_n]);
mask_bw = imbinarize(rgb2gray(mask));
f = 0; % Visualizar evolucion f=1, no visualizar f=0
mask_chv = chanvese(i_muscle_filt,mask_bw,100,f);

% mejorar la mascara
% cerrar agujeros
mask_morf = imopen(mask_chv, se2);
% eliminar detalles pequeños
mask_morf = imclose(mask_morf, se2);
% juntar estructuras
mask_morf = imopen(mask_morf, se6);

% Encontrar y eliminar tejidos (estructuras inservibles)
organs = i_muscle_filt .* uint8(mask_morf);
organs = imclose(organs, se2);
organs2 = imopen(organs, se2);
 
organs2(organs2>0) = 1;
limpio = i_muscle_filt .* uint8(~organs2);
limpio2 = imopen(limpio, se2);
limpio2 = imclose(limpio2, se4);

if(0)
    %function show_plots(plot, imgs, window_bound, titles)   
    images = {mask_chv, mask_morf, organs, organs2, limpio, limpio2};
    titles = {'limpio', 'mask_morf','organs', 'organs2', 'limpio', 'limpio2'};
    window_bound = [.2 .65 .1 .85];
    show_plots([2 3],images,window_bound,titles);    
end

%se_rec1 = strel('rectangle',[3 50]);
%imshow(imdilate(limpio2, se_rec1));



% Find boundry of image
% cc = bwconncomp(mask_chv);
% lblMatrix = labelmatrix(cc);
% edgeMatrix=edge(lblMatrix,'roberts',0);
% edgeMatrix1 = double(edgeMatrix);
% edgeMatrix1(end, end, 3) = 0;
% figure;
% imshow(edgeMatrix1);

% i_edge = imclose(i_muscle_filt, se2);
% figure, imshow(edge(i_edge));


if(0)        
    % Get the centroids and plot
    img_lab = bwlabel(limpio2);
    prop2 = regionprops(img_lab,'Centroid');
    centroids = cell2mat(struct2cell(prop2)');
    color = {'green'};
    img_with_mark = insertMarker(i_muscle_filt,centroids,'x','color',color,'size',5);
    figure, imshow(img_with_mark);
end



i_skel = bwmorph(limpio2,'shrink',Inf);
prop = regionprops(i_skel,'Centroid');
centroids = cell2mat(struct2cell(prop)');

if(1)                
    color = {'green'};
    img_with_mark = insertMarker(i_muscle_filt,centroids,'x','color',color,'size',5);
    figure, imshow(img_with_mark);
end


%nuevo = imclose(i_muscle_filt,se1);
nuevo = imcrop(dicom, crop_rect);

% crecimiento de region por puntos
multi_mask = zeros(size(i_muscle_filt));
for i = 1 : size(centroids(:,1))
	mask_rg = region_growing(nuevo, centroids(i,2), centroids(i,1), 30);	
    multi_mask = multi_mask + (mask_rg .* i);    
end
figure, imshow(multi_mask,[]);




i1 = imbinarize(i_muscle_filt);
i2 = imclose(i1, se1);
i3 = watershed(i1);
figure, imshow(i3,[]);

mask = imread('mask/mask3.jpg');
[mask_m,mask_n,z] = size(~i3);
mask = imresize(mask,[mask_m mask_n]);
mask_bw = imbinarize(rgb2gray(mask));
f = 1; % Visualizar evolucion f=1, no visualizar f=0
mask_chv = chanvese(~i3,mask_bw,100,f);

i4 = imfill(~mask_chv, 'holes');
i5 = imopen(i4, se2);
