%%
close all;
clear all;

path = './';
img_name = 'dicom/1.3.12.2.1107.5.1.4.96848.30000019110814020641200049704';

% files = dir(fullfile(path,'*.tif')); 
file = fullfile(path,img_name); 

% read image
dicom = dicomread(file);

%% Segmentation by Houndsfield
% define limits to clean not interesting information
i_muscle = filter_dicom(dicom,[950,1120]); 
i_bone = filter_dicom(dicom,[1100,2300]); 
i_all = filter_dicom(dicom,[950,2300]); 

if(0)
    %function show_plots(plot, imgs, window_bound, titles)   
    images = {dicom, i_muscle, i_bone, i_all};
    titles = {'DICOM', 'muscle','bone', 'muscle and bone'};
    window_bound = [.3 .45 .5 .85];
    show_plots([2 2],images,window_bound,titles);
end

%% define morfological structured elements
se1 = strel('disk',1);
se2 = strel('disk',2);
se3 = strel('disk',3);
se4 = strel('disk',4);
se5 = strel('disk',5);
se6 = strel('disk',6);


%% find the interest region and crop the original image

i_region = i_all;

%quitar lineas de ruido (las de la mesa)
i_region = imclose(i_region, se2);
%figure, imshow(i_region);
i_region = imopen(i_region, se6);
%figure, imshow(i_region);
i_region(i_region>0) = 1;

% calculate the crop region and crop the original image
% Get all the blob properties.  
% Can only pass in originalImage in version R2008a and later.
mucle_prop = regionprops(i_region, 'BoundingBox');
crop_rect = mucle_prop.BoundingBox + [-10,-5,20,10];

i_muscle_crop = imcrop(i_muscle, crop_rect);
i_bone_crop = imcrop(i_bone, crop_rect);
i_all_crop = imcrop(i_all, crop_rect);
dicom_crop = imcrop(dicom, crop_rect);

if(0)
    %function show_plots(plot, imgs, window_bound, titles)   
    images = {i_muscle_crop, i_bone_crop, i_all_crop};
    titles = {'muscle','bone', 'muscle and bone'};
    window_bound = [.1 .85 .35 .5];
    show_plots([1 3],images,window_bound,titles);
end


%% substract the bone of the original img
i_bone_bw = i_bone_crop;
i_bone_bw(i_bone_bw>0) = 1;

% eliminar pixeles dispersos
i_bone_mf = imopen(i_bone_bw, se1);
% rellena agujeros
i_bone_mf = imfill(i_bone_mf,'holes');

% apply bw
%i_muscle_filt = i_muscle_crop .* uint8(~i_bone_mf);
i_muscle_filt = i_muscle_crop;
i_muscle_filt(logical(i_bone_mf)) = 0;

if(0)
    %function show_plots(plot, imgs, window_bound, titles)   
    images = {i_bone_bw,imopen(i_bone_bw, se1), i_bone_mf, i_muscle_filt};
    titles = {'hueso','sin ruido','sin agujeros', 'musculo sin hueso'};
    window_bound = [.3 .5 .5 .7];
    show_plots([2 2],images,window_bound,titles);
end

%% chan-vese para refinar máscara

% leer la mascara
atlas_mask = imread('mask/mask4.jpg');
[M,N,z] = size(i_muscle_filt);
atlas_mask = imresize(atlas_mask,[M N]);
atlas_mask_bw = imbinarize(rgb2gray(atlas_mask));
f = 0; % Visualizar evolucion f=1, no visualizar f=0
iterations = 50;
atlas_mask_chv = activecontour(i_muscle_filt, atlas_mask_bw, iterations, 'Chan-Vese');
%atlas_mask_chv2 = chanvese(i_muscle_filt,atlas_mask_bw,iterations,f);
%imshowpair(atlas_mask_chv, atlas_mask_chv2, 'montage');

showOverlay(i_muscle_filt, ~atlas_mask_bw, [M,N]);

% mejorar la mascara
% cerrar agujeros
atlas_mask_morf = imopen(atlas_mask_chv, se2);
% eliminar detalles pequeños
atlas_mask_morf = imclose(atlas_mask_morf, se2);
% juntar estructuras
atlas_mask_morf = imopen(atlas_mask_morf, se6);

% Encontrar y eliminar tejidos (estructuras inservibles)
organs_mask = i_muscle_filt .* uint8(atlas_mask_morf);
organs_mask = imclose(organs_mask, se2);
organs_mask_2 = imopen(organs_mask, se2);
 
organs_mask_2(organs_mask_2>0) = 1;
mask = i_muscle_filt .* uint8(~organs_mask_2);
mask2 = imopen(mask, se2);
mask2 = imclose(mask2, se4);

if(0)
    %function show_plots(plot, imgs, window_bound, titles)   
    images = {atlas_mask_chv, atlas_mask_morf, organs_mask, organs_mask_2, mask, mask2};
    titles = {'limpio', 'mask_morf','organs', 'organs2', 'limpio', 'limpio2'};
    window_bound = [.2 .65 .1 .85];
    show_plots([2 3],images,window_bound,titles);    
end

%% chan-vese
iterations = 20;
segment = activecontour(i_muscle_filt, mask2, iterations, 'Chan-Vese');

% Create masked image.
maskedImage = i_muscle_filt;
maskedImage(~segment) = 0;

if(0)
    %function show_plots(plot, imgs, window_bound, titles)   
    images = {i_muscle_filt, segment, maskedImage};
    titles = {'i_muscle_filt','BW', 'maskedImage'};
    window_bound = [.1 .85 .35 .5];
    show_plots([1 3],images,window_bound,titles);
end


%showOverlay(i_muscle_filt, segment, [M,N]);

