%%
close all;
clear all;

path = './';
img_name = 'dicom/1.3.12.2.1107.5.1.4.96848.30000019110814020641200045570';

% files = dir(fullfile(path,'*.tif')); 
file = fullfile(path,img_name); 

% read image
dicom = dicomread(file);

%% Segmentation by Houndsfield
% define limits to clean not interesting information
i_muscle = filter_dicom(dicom,[950,1120]); 
i_bone = filter_dicom(dicom,[1100,2300]); 
i_all = filter_dicom(dicom,[950,2300]); 

if(0)
    %function show_plots(plot, imgs, window_bound, titles)   
    images = {dicom, i_muscle, i_bone, i_all};
    titles = {'DICOM', 'muscle','bone', 'muscle and bone'};
    window_bound = [.3 .45 .5 .85];
    show_plots([2 2],images,window_bound,titles);
end

%% define morfological structured elements
se1 = strel('disk',1);
se2 = strel('disk',2);
se3 = strel('disk',3);
se4 = strel('disk',4);
se5 = strel('disk',5);
se6 = strel('disk',6);


%% find the interest region and crop the original image

i_region = i_all;

%quitar lineas de ruido (las de la mesa)
i_region = imclose(i_region, se2);
%figure, imshow(i_region);
i_region = imopen(i_region, se6);
%figure, imshow(i_region);
i_region(i_region>0) = 1;

% calculate the crop region and crop the original image
% Get all the blob properties.  
% Can only pass in originalImage in version R2008a and later.
mucle_prop = regionprops(i_region, 'BoundingBox');
crop_rect = mucle_prop.BoundingBox + [-10,-5,20,10];

i_muscle_crop = imcrop(i_muscle, crop_rect);
i_bone_crop = imcrop(i_bone, crop_rect);
i_all_crop = imcrop(i_all, crop_rect);
dicom_crop = imcrop(dicom, crop_rect);

if(0)
    %function show_plots(plot, imgs, window_bound, titles)   
    images = {i_muscle_crop, i_bone_crop, i_all_crop};
    titles = {'muscle','bone', 'muscle and bone'};
    window_bound = [.1 .85 .35 .5];
    show_plots([1 3],images,window_bound,titles);
end

%% Fue eliminada una parte de operaciones morfologicas

%% substract the bone of the original img
i_bone_bw = i_bone_crop;
i_bone_bw(i_bone_bw>0) = 1;

% eliminar pixeles dispersos
i_bone_mf = imopen(i_bone_bw, se1);
% rellena agujeros
i_bone_mf = imfill(i_bone_mf,'holes');

i_muscle_filt = i_muscle_crop .* uint8(~i_bone_mf);

if(0)
    %function show_plots(plot, imgs, window_bound, titles)   
    images = {i_bone_bw,imopen(i_bone_bw, se1), i_bone_mf, i_muscle_filt};
    titles = {'hueso','sin ruido','sin agujeros', 'musculo sin hueso'};
    window_bound = [.3 .5 .5 .7];
    show_plots([2 2],images,window_bound,titles);
end


%% fue eliminada la parte de segmenta el musculo


%% nueva idea de codigo

i1 = imbinarize(i_muscle_filt);
i2 = imclose(i1, se1);
i3 = watershed(i1);
figure, imshow(i3,[]);

%prepare mask
mask = imread('mask/mask6.jpg');
[mask_m,mask_n,z] = size(~i3);
mask = imresize(mask,[mask_m mask_n]);
mask_bw = imbinarize(rgb2gray(mask));

i4 = uint8(i3) .* uint8(mask_bw);
figure, imshow(i4,[]);

grid_mask = imread('mask/mask3.jpg');
grid_mask = imresize(grid_mask,[mask_m mask_n]);
grid_mask_bw = imbinarize(rgb2gray(grid_mask));

%chan-vese for water shade
f = 1; % Visualizar evolucion f=1, no visualizar f=0
chanv_water = chanvese(~i4,grid_mask_bw,100,f);
water_s = chanv_water .* mask_bw;

figure, imshow(water_s,[]);

water_fill = imfill(water_s,'holes');
water_morp = imopen(water_fill, se1);


% obtener puntos de regiones importantes
points = bwmorph(water_morp,'shrink',Inf);
prop = regionprops(points,'Centroid');
centroids = cell2mat(struct2cell(prop)');

if(1)                
    color = {'green'};
    img_with_mark = insertMarker(i_muscle_filt,centroids,'x','color',color,'size',5);
    figure, imshow(img_with_mark);
end

% crear mascara de crecimiento de regiones importantes

multi_mask = zeros(size(i_muscle_filt));
for i = 1 : size(centroids(:,1))
	mask_rg = region_growing(i_muscle_filt, centroids(i,2), centroids(i,1), 30);	
    %mask_rg = region_growing(dicom_crop, centroids(i,2), centroids(i,1), 30);	
    multi_mask = multi_mask + (mask_rg .* i);    
end
figure, imshow(multi_mask,[]);

%%%%%%%%%%%%%%%%%%%%%%%%%
% tengo problemas para detectar la parte interior, creo que lo mejor es
% hacerlo separado

%% chan-vese (otra idea de solucion)

% leer la mascara
mask = imread('mask/mask4.jpg');
[mask_m,mask_n,z] = size(i_muscle_filt);
mask = imresize(mask,[mask_m mask_n]);
mask_bw = imbinarize(rgb2gray(mask));
f = 0; % Visualizar evolucion f=1, no visualizar f=0
mask_chv = chanvese(i_muscle_filt,mask_bw,100,f);

% mejorar la mascara
% cerrar agujeros
mask_morf = imopen(mask_chv, se2);
% eliminar detalles pequeños
mask_morf = imclose(mask_morf, se2);
% juntar estructuras
mask_morf = imopen(mask_morf, se6);

% Encontrar y eliminar tejidos (estructuras inservibles)
organs = i_muscle_filt .* uint8(mask_morf);
organs = imclose(organs, se2);
organs2 = imopen(organs, se2);
 
organs2(organs2>0) = 1;
limpio = i_muscle_filt .* uint8(~organs2);
limpio2 = imopen(limpio, se2);
limpio2 = imclose(limpio2, se4);

if(1)
    %function show_plots(plot, imgs, window_bound, titles)   
    images = {mask_chv, mask_morf, organs, organs2, limpio, limpio2};
    titles = {'limpio', 'mask_morf','organs', 'organs2', 'limpio', 'limpio2'};
    window_bound = [.2 .65 .1 .85];
    show_plots([2 3],images,window_bound,titles);    
end

if(1)
    %function show_plots(plot, imgs, window_bound, titles)   
    images = {limpio2, multi_mask};
    titles = {'limpio', 'mask_morf'};
    window_bound = [.2 .65 .1 .85];
    show_plots([1 2],images,window_bound,titles);    
end

iterations = 50;
BW = activecontour(i_muscle_filt, limpio2, iterations, 'Chan-Vese');

% Create masked image.
maskedImage = i_muscle_filt;
maskedImage(~BW) = 0;



if(1)
    %function show_plots(plot, imgs, window_bound, titles)   
    images = {i_muscle_filt, BW, maskedImage};
    titles = {'i_muscle_filt','BW', 'maskedImage'};
    window_bound = [.1 .85 .35 .5];
    show_plots([1 3],images,window_bound,titles);
end
% 
% 
% 
% % Find boundry of image
% % cc = bwconncomp(mask_chv);
% % lblMatrix = labelmatrix(cc);
% % edgeMatrix=edge(lblMatrix,'roberts',0);
% % edgeMatrix1 = double(edgeMatrix);
% % edgeMatrix1(end, end, 3) = 0;
% % figure;
% % imshow(edgeMatrix1);
% 
% % i_edge = imclose(i_muscle_filt, se2);
% % figure, imshow(edge(i_edge));
% 
% 
% if(0)        
%     % Get the centroids and plot
%     img_lab = bwlabel(limpio2);
%     prop2 = regionprops(img_lab,'Centroid');
%     centroids = cell2mat(struct2cell(prop2)');
%     color = {'green'};
%     img_with_mark = insertMarker(i_muscle_filt,centroids,'x','color',color,'size',5);
%     figure, imshow(img_with_mark);
% end
% 
% 
% 
% i_skel = bwmorph(limpio2,'shrink',Inf);
% prop = regionprops(i_skel,'Centroid');
% centroids = cell2mat(struct2cell(prop)');
% 
% if(1)                
%     color = {'green'};
%     img_with_mark = insertMarker(i_muscle_filt,centroids,'x','color',color,'size',5);
%     figure, imshow(img_with_mark);
% end
% 
% 
% %nuevo = imclose(i_muscle_filt,se1);
% nuevo = imcrop(dicom, crop_rect);
% 
% % crecimiento de region por puntos
% multi_mask = zeros(size(i_muscle_filt));
% for i = 1 : size(centroids(:,1))
% 	mask_rg = region_growing(nuevo, centroids(i,2), centroids(i,1), 30);	
%     multi_mask = multi_mask + (mask_rg .* i);    
% end
% figure, imshow(multi_mask,[]);
% 
% 
% 