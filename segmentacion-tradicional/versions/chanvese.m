function BW = chanvese(u0,mask,tmax,opt)
    if nargin < 3
        opt = 0;
    end
    % Parametros
    dt = 0.5;
    mu_ = 0.2;
    nu_ = 0;
    l1 = 1;
    l2 = 1;
    % Imagen de entrada
    u0 = im2double(u0);
    % Funcion de distancia con signo
    phi_ = bwdist(mask) - bwdist(1-mask) + im2double(mask) - 0.5;
    %figure, imshow(phi_);
    % Iteraciones
    if opt
        figure;
        imshow(getcontour(u0,phi_));
        title(sprintf("Iteracion %d/%d",0,tmax));
        pause(0.3);
    end
    for i = 1:tmax
        H_phi = H(phi_);
        a1 = sum(sum(u0.*H_phi))/(sum(sum(H_phi))+eps); % Contraste interno
        a2 = sum(sum(u0.*(1-H_phi)))/(sum(sum(1-H_phi))+eps); % Contraste externo
        k  = kappa(phi_);
        E = (mu_*k - nu_ - l1*(u0 - a1).^2 + l2*(u0 - a2).^2); % Funcional de energia
        phi_ = phi_ + dt*E; % Actualiza la funcion level set
        if opt && (mod(i,20)==0)
            imshow(getcontour(u0,phi_));
            title(sprintf("Iteracion %d/%d",i,tmax));
            pause(0.2);
        end
    end
    BW = phi_<=0;
    if opt
        close(gcf);
        pause(0.1);
    end
    
%**************************************************************
% Funcion Heaviside
function H_ = H(z)
H_ = 0.5*(1+(2/pi)*atan(z));
H_(H_>1) = 1;
H_(H_<0) = 0;

%**************************************************************
% Curvatura
function k = kappa(I)
[Y,X] = size(I);
ym = [2:Y, Y];
yp = [1, 1:Y-1];
xp = [1, 1:X-1];
xm = [2:X, X];
fy  = 0.5*(I(ym,:) - I(yp,:));
fy2 = fy.*fy;
fx  = 0.5*(I(:,xm) - I(:,xp));
fx2 = fx.*fx;
fyy = I(ym,:) - 2*I + I(yp,:);
fxx = I(:,xm) - 2*I + I(:,xp);
fxy = 0.25*(I(ym,xm) - I(yp,xm) + I(ym,xp) - I(yp,xp));
k = (fxx.*fy2 - 2*fx.*fy.*fxy + fyy.*fx2)./...
    (fx2 + fy2 + eps).^(1.5);
k = k./max(abs(k(:)));
k(:,1) = eps; k(:,X) = eps; k(1,:) = eps; k(Y,:) = eps;