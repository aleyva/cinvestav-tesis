function final = imoverlay(img, bw, show)
%     figure, imshow(img, 'InitialMagnification',400);
%     % Make a truecolor all-green image. 
%     green = cat(3, zeros(size), ones(size), zeros(size) );
%     hold on 
%     h = imshow(green); 
%     hold off 
%     % Use our influence map as the 
%     % AlphaData for the solid green image. 
%     set(h, 'AlphaData', bw)
%     
    size_img = size(img);
    final = cat(3, img, img, img);
    green = cat(3, zeros(size_img), ones(size_img), zeros(size_img) ) * 255;
    final = final + uint8(green .* bw);
    final(final>255) = 255;
    if(show)
        figure, imshow(final, 'InitialMagnification',400);
    end
end