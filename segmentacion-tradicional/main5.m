%%
close all;
clear all;


dicom_path = '/mnt/Home/Proyectos/Maestria/14-tesis/dicoms/evaluacion-tesis/dataset/dicom';
dicoms = dir(fullfile(dicom_path,'*.dcm')); 

%dicom_solo = 8;
%for num_dic = dicom_solo:dicom_solo
for num_dic = 1:numel(dicoms)    

    img_path = fullfile(dicom_path,dicoms(num_dic).name);

    % read image
    dicom = dicomread(img_path);
    [M,N,~] = size(dicom);

    %% Segmentation by Houndsfield
    % define limits to clean not interesting information
    i_muscle = filter_dicom(dicom,[950,1120]); 
    i_bone = filter_dicom(dicom,[1100,2300]); 
    i_all = filter_dicom(dicom,[950,2300]); 

    if(0)
        %function show_plots(plot, imgs, window_bound, titles)   
        images = {dicom, i_muscle, i_bone, i_all};
        titles = {'DICOM', 'Muscle [20, 40] HF','Bone [1000, inf) HF', 'Muscle and bone [20, inf) HF'};
        window_bound = [.3 .45 .5 .85];
        show_plots([2 2],images,window_bound,titles);
    end

    %% define morfological structured elements
    se1 = strel('disk',1);
    se2 = strel('disk',2);
    se3 = strel('disk',3);
    se4 = strel('disk',4);
    se5 = strel('disk',5);
    se6 = strel('disk',6);


    %% find the interest region and crop the original image

    i_region = i_all;

    %quitar lineas de ruido (las de la mesa)
    i_region = imclose(i_region, se2);
    %figure, imshow(i_region);
    i_region = imopen(i_region, se6);
    %figure, imshow(i_region);
    i_region(i_region>0) = 1;

    % calculate the crop region and crop the original image
    % Get all the blob properties.  
    % Can only pass in originalImage in version R2008a and later.
    mucle_prop = regionprops(i_region, 'BoundingBox');
    crop_rect = mucle_prop.BoundingBox + [-10,-5,20,10];
    crop_rect = uint16(crop_rect);
    
    i_muscle_crop = imcrop(i_muscle, crop_rect);
    i_bone_crop = imcrop(i_bone, crop_rect);
    i_all_crop = imcrop(i_all, crop_rect);
    dicom_crop = imcrop(dicom, crop_rect);
    
    %re-defining croping points
    x1 = crop_rect(1,1);
    y1 = crop_rect(1,2);
    x2 = crop_rect(1,1)+crop_rect(1,3);
    y2 = crop_rect(1,2)+crop_rect(1,4);    
    if(x1 == 0) x1 = 1; x2 = x2+1; end
    if(y1 == 0) y1 = 1; y2 = y2+1; end    
    if(x2 > M) x2 = M; end
    if(y2 > N) y2 = N; end
     
    
    
    % get the size of the new image
    [M_c,N_c,~] = size(i_muscle_crop);
    
    if(0)       
        %horizontal superior, horizontal inferior, vertical izquierda,
        %vertical derecha
        overlay_rect = zeros(size(i_all));
        overlay_rect(y1,x1:x2) = 1;
        overlay_rect(y2,x1:x2) = 1;
        overlay_rect(y1:y2,x1) = 1;
        overlay_rect(y1:y2,x2) = 1;
        
        %function show_plots(plot, imgs, window_bound, titles)   
        images = {imoverlay(i_muscle, logical(overlay_rect), 0), i_muscle_crop};
        titles = {'Original','Cropped'};
        window_bound = [.1 .85 .35 .85];
        show_plots([1 2],images,window_bound,titles);
    end


    %% substract the bone of the original img
    i_bone_bw = i_bone_crop;
    i_bone_bw(i_bone_bw>0) = 1;

    % eliminar pixeles dispersos
    i_bone_mf = imopen(i_bone_bw, se1);
    % rellena agujeros
    i_bone_mf = imfill(i_bone_mf,'holes');

    % apply bw
    %i_muscle_filt = i_muscle_crop .* uint8(~i_bone_mf);
    i_muscle_filt = i_muscle_crop;
    i_muscle_filt(logical(i_bone_mf)) = 0;

    if(0)
        %function show_plots(plot, imgs, window_bound, titles)   
        images = {i_bone_bw,imopen(i_bone_bw, se1), i_bone_mf, i_muscle_filt};
        titles = {'Bone BW','Bone MP Open','Bone MP fill', 'Muscle without bone'};
        window_bound = [.3 .5 .5 .7];
        show_plots([2 2],images,window_bound,titles);
    end


    % %% substract the vertebra and align with the mask
    % mask_spine = imread('mask/mask-spine.jpg');
    % mask_spine_resize = imresize(mask_spine,[M_c N_c]);
    % mask_spine_bw = rgb2gray(mask_spine_resize);
    % 
    % i_bone_bw = i_bone_crop .* mask_spine_bw ;
    % i_bone_bw(i_bone_bw>0) = 1;
    % 
    % % eliminar pixeles dispersos
    % i_bone_mf = imopen(i_bone_bw, se1);
    % % rellena agujeros
    % i_bone_mf = imfill(i_bone_mf,'holes');
    % 
    % % registraton estimator
    % % guardar imagen de hueso
    % %imwrite(imadjust(i_bone_bw),'align/hueso-gray.png');
    % %imwrite(imadjust(i_bone_mf),'align/hueso-bw.png');
    % 
    % fix_spine = imread('align/r001-spine-bw.jpg');
    % fix_spine = rgb2gray(fix_spine);
    % 
    % i_muscle_reg = registerImages2(fix_spine,i_bone_mf,i_muscle_filt);
    % i_muscle_filt2 = i_muscle_reg.RegisteredImage;
    % 
    % imshowpair(i_muscle_filt, imresize(i_muscle_filt2,[M_c N_c]), 'montage');



    %% chan-vese para refinar máscara

    % leer la mascara
    atlas_mask = imread('mask/mask8.jpg');
    atlas_mask = imresize(atlas_mask,[M_c N_c]);
    atlas_mask_bw = imbinarize(rgb2gray(atlas_mask));
    f = 0; % Visualizar evolucion f=1, no visualizar f=0
    iterations = 50;
    atlas_mask_chv = activecontour(i_muscle_filt, atlas_mask_bw, iterations, 'Chan-Vese');
    %atlas_mask_chv2 = chanvese(i_muscle_filt,atlas_mask_bw,iterations,f);
    
    if(0)
        imoverlay(i_muscle_filt, ~atlas_mask_bw, 1);    
    end


    % mejorar la mascara
    % cerrar agujeros
    atlas_mask_morp = imopen(atlas_mask_chv, se2);
    % eliminar detalles pequeños
    atlas_mask_morp = imclose(atlas_mask_morp, se2);
    % juntar estructuras
    atlas_mask_morp = imopen(atlas_mask_morp, se6);

    % Encontrar y eliminar tejidos (estructuras inservibles)
    organs_mask = i_muscle_filt .* uint8(atlas_mask_morp);
    organs_mask = imclose(organs_mask, se2);
    organs_mask_2 = imopen(organs_mask, se2);

    organs_mask_2(organs_mask_2>0) = 1;
    mask = i_muscle_filt .* uint8(~organs_mask_2);
    mask2 = imopen(mask, se2);
    mask2 = imclose(mask2, se4);

    if(0)
        %function show_plots(plot, imgs, window_bound, titles)   
        images = {atlas_mask_chv, atlas_mask_morp, organs_mask, organs_mask_2, mask, mask2};
        titles = {'Chan-Vese', 'Chan-Vese MP Close Open','Organs', 'Organs MP Close Open', 'Muscle - Organs', 'Mask'};
        window_bound = [.2 .65 .1 .85];
        show_plots([2 3],images,window_bound,titles);    
    end

    %% chan-vese
    iterations = 30;
    segment = activecontour(i_muscle_filt, mask2, iterations, 'Chan-Vese');

    % Create masked image.
    maskedImage = i_muscle_filt;
    maskedImage(~segment) = 0;

    if(0)
        %function show_plots(plot, imgs, window_bound, titles)   
        images = {mask2, segment};
        titles = {'First Chan-Vese','Chan-Vese Refined'};
        window_bound = [.1 .85 .35 .85];
        show_plots([1 2],images,window_bound,titles);
    end
    
    %final = imoverlay(i_muscle_crop, segment, 1);      
    
    %% Making the mask, as dicom original size         
    final_mask = zeros(M,N,3);    
    final_mask(y1:y2, x1:x2, 1) = segment;
    
    %final = imoverlay(i_muscle, final_mask, 1);
    
    %% guardar el resultado    
    if(1)
        filename_dicom = sprintf('%04d-L3-dicom.jpg',num_dic);
        filename_mask = sprintf('%04d-L3-mask.jpg',num_dic);
        file_dicom = fullfile('./result-v3/',filename_dicom);        
        file_mask = fullfile('./result-v3/',filename_mask);        
        imwrite(i_muscle,file_dicom);
        imwrite(final_mask,file_mask);
    end
    
end