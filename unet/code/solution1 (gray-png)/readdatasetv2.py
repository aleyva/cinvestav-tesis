'''
conda install numpy scikit-image spyder
conda install -c conda-forge python-lmdb
conda install -c anaconda protobuf
conda install -c anaconda tensorflow
'''
import os 
import numpy as np
import random
from tqdm import tqdm
from skimage.io import imread, imshow #, imsave
from skimage.transform import resize 
#from sklearn.utils import shuffle
#import pydicom


def read_gray_dataset(train_img_path, train_mask_path, test_dic_path, img_width, img_height, number_channels):

    # Get train and test IDs
    print("traing_img_path: " + train_img_path)
    train_ids = next(os.walk(train_img_path))[2]
    test_ids = next(os.walk(test_dic_path))[2]

    #Create array with zeros of the same size as the resize image size.
    X_train = np.zeros((len(train_ids), img_height, img_width, number_channels), dtype=np.uint8) 
    Y_train = np.zeros((len(train_ids), img_height, img_width, 1), dtype=np.bool)
    X_test = np.zeros((len(test_ids), img_height, img_width, number_channels), dtype=np.uint8)


    if 0 :
        mask = imread( '/home/spidey/Git/cinvestav-tesis/code/Unet/train/mask/image002-mask.jpg.png'  )[:,:,1]
        mask = np.expand_dims(resize(mask, (img_height, img_width), mode='constant',  preserve_range=True), axis=-1)
        mask[mask < 40] = 0
        Y_train[0] = mask
        imshow(np.squeeze(Y_train[0]))


    print('\nReading and resizing training DICOM images ... ')
    for index, file in tqdm(enumerate(sorted(os.listdir(train_img_path)))) :      
        #print(os.path.join(train_img_path,file))
        img = imread( os.path.join(train_img_path,file) )
        img = np.expand_dims(resize(img, (img_height, img_width), mode='constant',  preserve_range=True), axis=-1)    
        img = resize(img, (img_height, img_width), mode='constant', preserve_range=True)
        X_train[index] = img #Fill empty X_train with values from img
        

    print('\nReading and resizing training masks ... ')    
    print(train_img_path)    
    for index, file in tqdm(enumerate(sorted(os.listdir(train_mask_path)))) :      
        # print(os.path.join(train_img_path,file))                
        mask = imread(  os.path.join(train_mask_path,file)  )
        mask = np.expand_dims(resize(mask, (img_height, img_width), mode='constant',  preserve_range=True), axis=-1)
        mask[mask < 40] = 0   #clean the mask
        Y_train[index] = mask
        

    print('\nReading and resizing test images ... ')
    for index, file in tqdm(enumerate(sorted(os.listdir(test_dic_path)))) :     
        img = imread( os.path.join(test_dic_path,file) )
        img = np.expand_dims(resize(img, (img_height, img_width), mode='constant',  preserve_range=True), axis=-1)    
        img = resize(img, (img_height, img_width), mode='constant', preserve_range=True)
        X_test[index] = img #Fill empty X_train with values from img
        


    data_train = []
    data_train.append(X_train)
    data_train.append(Y_train)

    data_test = []
    data_test.append(X_test)
    # data_test.append(Y_test)
    
    return data_train, data_test


# the train dataset and the test dataset are in the same folder
def read_gray_dataset_v2(img_path, mask_path, img_width, img_height, number_channels, train_fraction):
    
    print("images path: " + img_path)
    print("masks path: " + mask_path)
    # count the number of files
    len_imgs = len(os.listdir(img_path))    
    print(str(len_imgs) + "," + str(len(os.listdir(mask_path))) )
    assert len_imgs == len(os.listdir(mask_path)), 'El número de imagenes y de mascaras deben coincidir'
    assert len_imgs > 2, 'no existen suficientes imágenes para entrenar'
    

    #Create array with zeros of the same size as the resize image size.
    imgs = np.zeros((len_imgs, img_height, img_width, number_channels), dtype=np.uint8)
    masks = np.zeros((len_imgs, img_height, img_width, 1), dtype=np.bool)
    

    print('\nReading and resizing training DICOM images ... ')
    for index, file in tqdm(enumerate(sorted(os.listdir(img_path)))) :              
        img = imread( os.path.join(img_path,file) )
        img = np.expand_dims(resize(img, (img_height, img_width), mode='constant',  preserve_range=True), axis=-1)    
        #img = resize(img, (img_height, img_width), mode='constant', preserve_range=True)
        imgs[index] = img #Fill empty X_train with values from img
        

    print('\nReading and resizing training masks ... ')    
    for index, file in tqdm(enumerate(sorted(os.listdir(mask_path)))) :              
        mask = imread(  os.path.join(mask_path,file)  )[:,:,1]
        #mask = imread(  os.path.join(mask_path,file)  )
        mask = np.expand_dims(resize(mask, (img_height, img_width), mode='constant',  preserve_range=True), axis=-1)
        mask[mask < 40] = 0   #clean the mask
        masks[index] = mask
        
    # suffle img and mask without breaking the correspondance
    #train, test = shuffle(train, test)    

    #shuffle(imgs,masks)    
    
    # divide datasets into test and train
    idx = int(train_fraction * len_imgs)
    train_imgs = imgs[0:idx]
    train_masks = masks[0:idx]
    test_imgs = imgs[idx:]
    test_masks = masks[idx:]
    
    return train_imgs, train_masks, test_imgs, test_masks




# the train dataset and the test dataset are in the same folder
def read_test_dataset(img_path, img_width, img_height, number_channels):
    
    print("images path: " + img_path)    
    # count the number of files
    len_imgs = len(os.listdir(img_path))        
    
    #Create array with zeros of the same size as the resize image size.
    imgs = np.zeros((len_imgs, img_height, img_width, number_channels), dtype=np.uint8)    
    filename_imgs = []

    print('\nReading and resizing training DICOM images ... ')
    for index, file in tqdm(enumerate(sorted(os.listdir(img_path)))) :              
        img = imread( os.path.join(img_path,file) )
        img = np.expand_dims(resize(img, (img_height, img_width), mode='constant',  preserve_range=True), axis=-1)    
        img = resize(img, (img_height, img_width), mode='constant', preserve_range=True)        
        imgs[index] = img #Fill empty X_train with values from img
        filename_imgs.append(file)
    
    return imgs, filename_imgs


'''
def read_dicom_dataset(train_img_path, train_mask_path, test_dic_path, img_width, img_height, number_channels):

    # Get train and test IDs
    print("traing_img_path: " + train_img_path)
    train_ids = next(os.walk(train_img_path))[2]
    test_ids = next(os.walk(test_dic_path))[2]


    #Create array with zeros of the same size as the resize image size.
    X_train = np.zeros((len(train_ids), img_height, img_width, number_channels), dtype=np.uint16) 
    Y_train = np.zeros((len(train_ids), img_height, img_width, 1), dtype=np.bool)
    X_test = np.zeros((len(test_ids), img_height, img_width, number_channels), dtype=np.uint16)


    print('\nResizing training DICOM images ... ')
    for index, file in tqdm(enumerate(sorted(os.listdir(train_img_path)))) :      
        img = pydicom.dcmread( os.path.join(train_img_path,file) ).pixel_array
        img = np.expand_dims(resize(img, (img_height, img_width), mode='constant',  preserve_range=True), axis=-1)    
        img = resize(img, (img_height, img_width), mode='constant', preserve_range=True)
        X_train[index] = img #Fill empty X_train with values from img
        
    
    print('\nResizing training masks ... ')    
    for index, file in tqdm(enumerate(sorted(os.listdir(train_mask_path)))) :              
        mask = imread(  os.path.join(train_mask_path,file)  )[:,:,1]
        mask = np.expand_dims(resize(mask, (img_height, img_width), mode='constant',  preserve_range=True), axis=-1)
        mask[mask < 40] = 0
        Y_train[index] = mask
        
    
    print('\nResizing test images ... ')
    for index, file in tqdm(enumerate(sorted(os.listdir(test_dic_path)))) :     
        img = pydicom.dcmread( os.path.join(test_dic_path,file) ).pixel_array
        img = np.expand_dims(resize(img, (img_height, img_width), mode='constant',  preserve_range=True), axis=-1)    
        img = resize(img, (img_height, img_width), mode='constant', preserve_range=True)
        X_test[index] = img #Fill empty X_train with values from img
        
    print('\nDone !!!')


    data_train = []
    data_train.append(X_train)
    data_train.append(Y_train)

    data_test = []
    data_test.append(X_test)
    #data_test.append(Y_test)
    
    return data_train, data_test
'''


def shuffle(a,b):
    assert len(a) == len(b)
    start_state = random.getstate()
    random.shuffle(a)
    random.setstate(start_state)
    random.shuffle(b)
