
#import random
import numpy as np
from tqdm import tqdm
import imgaug.augmenters as iaa
from imgaug.augmentables.segmaps import SegmentationMapsOnImage
#import imgaug.augmentables.segmaps as ias



class Dataset:

    def __init__(self, imgs, masks, train_fraction = 0, cross_validation = 0):
        
        assert len(imgs) == len(masks), 'Images and masks must be iqual'   
        
        np.random.seed(1)               # initialize random seed
        np.random.rand()                     # discart the first number generated                
        self.total_imgs = len(imgs)     # Total images recived for divide the dataset into train and test
        self.len_train_imgs_ba = None   # Number of images assigned to the train dataset and before the agumentation process
        self.len_train_imgs_aa = None   # Total images in the train dataset (before the agumentation process)
        self.train_imgs = None          # dataset (assigned later)
        self.train_masks = None         # dataset (assigned later)
        self.test_imgs = None           # dataset (assigned later)
        self.test_masks = None          # dataset (assigned later)
        
        assert (train_fraction != 0 or cross_validation != 0), 'Must use train_fraction or cross_validation for divide sets'
            
        # divide the data set into 'train_imgs', 'train_masks', 'test_imgs', 'test_masks'
        self.divide_dataset(imgs, masks, train_fraction, cross_validation)
        
        self.len_train_imgs_ba = len(self.train_imgs)           
        self.len_test_imgs = len(self.test_imgs)                   
        #self.print_var_summary()
        
    # divide datasets into test and train            
    def divide_dataset(self, imgs, masks, train_fraction, cross_validation):
                
        if(train_fraction != 0):
            #devide by fraction
            assert train_fraction >= 0 and train_fraction < 1, 'train fraction must be a real between 0 and 1'            
            idx = int(train_fraction * self.total_imgs)                                            
            train_indices = np.array(range(0,idx))
            test_indices = np.array(range(idx,self.total_imgs))
        elif(cross_validation != 0):
            #devide by chunks for cross validation
            assert cross_validation['chunk'] >0, 'Chunk must be greater than 0'
            assert cross_validation['total_chunks'] >= cross_validation['chunk'], 'Chunk must be less or equal than total_chunks'            
            chunk_size = int(self.total_imgs / cross_validation['total_chunks'])            
            start_idx = int(chunk_size * (cross_validation['chunk']-1) )        
            if(  cross_validation['total_chunks'] == cross_validation['chunk'] ):
                end_idx = self.total_imgs
            else:
                end_idx = int(start_idx + chunk_size)
            train_indices = np.array(range(0,start_idx),dtype=np.uint16)
            train_indices = np.append(train_indices, np.array( range(end_idx,self.total_imgs) ,dtype=np.uint16) )
            test_indices = np.array(range(start_idx,end_idx))                        
            
        #print('train_indices : {}', train_indices)            
        #print('test_indices : {}', test_indices)            
        self.train_imgs = imgs[train_indices]
        self.train_masks = masks[train_indices]
        self.test_imgs = imgs[test_indices]
        self.test_masks = masks[test_indices]
        

    def print_var_summary(self):
        print('')
        print('---------- Dataset  summary ----------')    
        print('original imgs and masks = {}'.format(self.total_imgs) )
        print('train imgs and masks before augmentation = {}'.format(self.len_train_imgs_ba) )
        print('train imgs and masks after augmentation = {}'.format(self.len_train_imgs_aa) )
        print('test imgs and masks = {}'.format( self.len_test_imgs ) )        
        print('imgs shape = {}'.format(self.train_imgs.shape) )
        print('masks shape = {}'.format(self.train_masks.shape) )
        print('--------------------------------------')
        
    
    def train_suffle(self):        
        '''
        start_state = random.getstate()
        random.shuffle(self.train_imgs)
        random.setstate(start_state)
        random.shuffle(self.train_masks)
        '''
        start_state = np.random.get_state()
        np.random.shuffle(self.train_imgs)
        np.random.set_state(start_state)
        np.random.shuffle(self.train_masks)
        
    
    def augment(self,
        augmentation_times = 0,
        prob_scale = 0,
        prob_rotate = 0,
        prob_shear = 0,
        prob_translate = 0,
        prob_flipx = 0,
        prob_perspective = 0,
        prob_piecewise = 0,
        prob_blur = 0,
        prob_sharpen = 0,
        prob_noise = 0,
        prob_coarse_dropout = 0,
        val_scale = (0.8, 1.1), 
        val_rotate = (-20, 20),
        val_shear = (-16, 16),
        val_translate_percent= {"x": (-0.05, 0.05), "y": (-0.1, 0.1)},
        val_perspective = (0.03, 0.075),
        val_piecewise = (0.01, 0.05),
        val_blur = (.5, 2.5),
        val_sharpen = (0.03, 0.12),
        val_noise = 0.05,
        val_coarse_dropout = (0.08, 0.1),
        val_coarse_dropout_percent = (0.1, 0.4)
    ):        
        
        self.augmentation_times = augmentation_times
    
        if(self.augmentation_times <= 1):
            print ("No augmentations will be perform")    
            self.print_var_summary()        
            return
    
            
        #------------------
        #   sequence all transformations
        #------------------
        # all the transformations will be apply with some probability, if the probability is 0, the transformation will not be applied
        seq = iaa.Sequential([    
            iaa.Sometimes( prob_scale, iaa.Affine( scale=val_scale ) ),
            iaa.Sometimes( prob_rotate, iaa.Affine( rotate=val_rotate ) ),
            iaa.Sometimes( prob_shear, iaa.Affine( shear=val_shear ) ),
            iaa.Sometimes( prob_translate, iaa.Affine( translate_percent=val_translate_percent ) ),
            iaa.Fliplr( prob_flipx ) ,
            iaa.Sometimes( prob_perspective, iaa.PerspectiveTransform( scale=val_perspective ) ),
            iaa.Sometimes( prob_piecewise, iaa.PiecewiseAffine( scale=val_piecewise ) ),    
            iaa.Sometimes( prob_blur, iaa.GaussianBlur(sigma=val_blur) ),     
            iaa.Sometimes( prob_sharpen, iaa.Sharpen(alpha=val_sharpen) ),
            #iaa.Sometimes( prob_noise, iaa.AdditiveGaussianNoise(scale=(0, 0.1*255)) ),  # AdditiveGaussianNoise    
            #iaa.Sometimes( prob_noise, iaa.AdditiveLaplaceNoise(scale=(0, 0.08*255)) ),  # AdditiveLaplaceNoise   between salt and peper and gaussian    
            #iaa.Sometimes( prob_noise, iaa.ReplaceElementwise(0.02, [0, 255]) ),         # ReplaceElementwise noise    
            #iaa.Sometimes( prob_noise, iaa.SaltAndPepper(0.05) ),                        # SaltAndPepper    
            #iaa.Sometimes( prob_noise, iaa.Salt(0.03) ),                                   # Salt noise    
            iaa.Sometimes( prob_noise, iaa.Pepper(val_noise) ),                             # Pepper noise
            iaa.Sometimes( prob_coarse_dropout, iaa.CoarseDropout(val_coarse_dropout, size_percent=val_coarse_dropout_percent) ),
        ], random_order=True)
    
        
        img1 = self.train_imgs[0]
        mask1 = self.train_masks[0]
        #resize the datasets considering the augmentations that will be performed        
        
        self.train_imgs = np.resize( self.train_imgs , self.get_shape_augmentation( img1.shape) )
        self.train_masks = np.resize( self.train_masks , self.get_shape_augmentation( mask1.shape) )
        
   
        for index in tqdm(range(self.len_train_imgs_ba, len(self.train_imgs))):    
            # create the segmap                
            # segmap = segmap.astype(np.uint8) # mandatory conversion if mask is not np.uint8
            #segmap = SegmentationMapsOnImage(self.train_masks[index], shape=img1.shape)            
            segmap = SegmentationMapsOnImage(self.train_masks[index], shape=img1.shape)            
               
            # Perform image and segmap augmentation
            image_aug, segmap_aug = seq(image=self.train_imgs[index], segmentation_maps=segmap)                
            
            # save augmentations            
            self.train_imgs[index] = image_aug
            self.train_masks[index] = segmap_aug.get_arr()
        
       
        self.len_train_imgs_aa = len(self.train_imgs)                   
        self.print_var_summary()
        
        
 
        
        
    def get_shape_augmentation(self, shape):
        if( len(shape) == 2):        
            return ( self.len_train_imgs_ba * self.augmentation_times, shape[0], shape[1])
        else:
            return ( self.len_train_imgs_ba * self.augmentation_times, shape[0], shape[1], shape[2])
        
            
    def get_train_example(self, range_=None):                
        if(range_ == None):
            range_= (0,self.len_train_imgs_aa-1)
        index = np.random.randint(range_[0],range_[1])        
        return self.train_imgs[index], self.train_masks[index]
       
   

    '''
    def get_image_tensor_shape(self):
        # HWC to CHW        
        return [self.image_size[2], self.image_size[0], self.image_size[1]]

    def get_label_tensor_shape(self):
        return [self.image_size[0], self.image_size[1]]
    '''

    

        
        