import os 
import numpy as np
from tqdm import tqdm
from pydicom import dcmread
from skimage.io import imread
from skimage.transform import resize 


  
class Reader:
    
    def __init__(self, files_path, type_, img_width = 512, img_height = 512):
        
        # copy inputs to class variables
        self.files_path = files_path                
        self.filenames = []        
        self.len_imgs = None
        self.img_width = img_width
        self.img_height = img_height                
        self.channels = None
        self.type_ = type_                  # 'dicom', 'mask'
        self.extension = None               # '.dcm', '.jpg', '.png' ...
        self.dtype = self.get_dtype(type_)  # float32 or uint8
        self.dicoms = []                    # array of dicoms, true file, not only the image
        
                
        # confirm that the folder exist        
        assert os.path.exists(self.files_path), 'Folder {} does not exist'.format(self.files_path)        

        # count the number of files
        self.len_imgs = len(os.listdir(self.files_path))
                
        first_file = os.path.join( self.files_path, os.listdir(self.files_path)[0] )        
        
        # get the extension of the first file in the directory        
        self.extension = self.get_file_type(first_file)
        assert self.is_valid_filetype() == True, 'Invalid extension, only suport .dcm, .jpeg, .jpg, .png'

        # get the number of channels, based in the first file     
        self.channels = self.get_channels(first_file)
        
        #Create array with zeros of the same size as the resize image size.
        if(self.channels  == 0):
            self.imgs = np.zeros( (self.len_imgs, self.img_height, self.img_width), self.dtype)
        else:        
            self.imgs = np.zeros( (self.len_imgs, self.img_height, self.img_width, self.channels), self.dtype)
            

        print('\nReading and resizing {} images ... '.format(self.extension))
        
        for index, file in tqdm(enumerate(sorted(os.listdir(self.files_path)))) :                      
            self.imgs[index] = self.read_img( os.path.join(self.files_path,file) )               
            self.filenames.append(file)
           
                
        
    def is_valid_filetype(self):
        if( self.extension == '.jpeg' or self.extension == '.jpg' or self.extension == '.png' or self.extension == '.dcm'):        
            return True
        else:
            return False
        

    def read_img(self, file_path):        
        # Analyzing a total of 86 dicom files, an upper limit of 2500 was obtained. 
        # Some data goes above this limit but are cut to unify the limits in dicom images.
        threshold_dicom = 2500
        threshold_mask = 100
        if( self.type_ == 'dicom'):
            dicom = dcmread( file_path )            
            dicom.pixel_array[dicom.pixel_array>threshold_dicom] = threshold_dicom   # mandatory step to normalize the dynamic range            
            self.dicoms.append(dicom)
            img = dicom.pixel_array            
        # _NOTE if mask is colored or multi segmentation colored, need to manage in code        
        elif( self.type_ == 'mask_gray'):            
            img = imread( file_path )                        
            img = np.where(img > threshold_mask, 1, 0)
        elif( self.type_ == 'mask_green'):                                  
            img = imread(  file_path  )[:,:,1]   
            img = np.where(img > threshold_mask, 1, 0)
            
        # resize
        img = resize(img, (self.img_height, self.img_width), mode='constant',  preserve_range=True)
        
        # Normalization and the dims expansion, will do in unet                
        return img
  
    
    def get_dtype(self, type_):
        if(type_ == 'dicom'):
            return np.float32
        else:
            return np.uint8
        

    # get the extension of the first file in the list
    def get_file_type(self, file):
        split_file_name = os.path.splitext( file )          
        file_type = split_file_name[len(split_file_name)-1]                
        return file_type.lower()
        
        
    # get the number of channels, posible return values {2 or 3}
    def get_channels(self, file):            
        '''      
        Shape dicom (:,:)
        Shape png and jpg - gray (:,:)
        Shape png and jpg - color (:,:, 3 or 4)        
        '''
        if(self.type_ == 'mask_color'):
            return 3 #hardcoded the channel
        else:
            # dicom, mask_green, mask_gray
            return 0
        
        '''
        # past code
        if(self.extension == '.dcm'):
            return 0            
        else:
            #read the firs image
            img = imread( file )
            if( len(img.shape) == 2 ):
                return 0
            elif ( len(img.shape) == 3 ):
                return img.shape[2]
        '''

    
    def get_image_shape(self):
        if(self.channels==0):
            return (self.img_height, self.img_width)
        else:
            return (self.img_height, self.img_width, self.channels)
    
    '''
    def get_image_size(self):
        return self.img_height, self.img_width
        
    def normalize(self, img):
        return img.astype(np.float32)
    
    def get_image_count(self):        
        return self.len_imgs
    '''
    
