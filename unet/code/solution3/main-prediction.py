# -*- coding: utf-8 -*-

"""Load packages"""

import os
import sys
import numpy as np
import reader
import model
import plot
import evaluation
import matplotlib.pyplot as plt
from skimage.transform import resize 
from pydicom.pixel_data_handlers.util import apply_modality_lut
from skimage.io import imsave


assert sys.version_info[0] == 3, 'Python 3.x required'


local = True        # true for runing in local computer, false to run on google colab
if (local == False):     
    from google.colab import drive
    drive.mount('/content/gdrive')



def print_var_summary():
    print('')
    print('---------------------- Variable  summary ----------------------')
    print('Image height: {} - width: {}'.format(img_height,img_width))
    print('Image (dicom) dataset path = {}'.format(img_dataset_path))    
    print('Predictions path = {}'.format(prediction_path))    
    print('Model filename = {}'.format(model_file)) 
    print('---------------------------------------------------------------')


def get_patient_info(dicom):
    age_str = dicom.PatientAge    
    patient_age = int( age_str.replace("Y", "") )
    patient_sex = dicom.PatientSex
    return patient_age, patient_sex 


def filter_dicom(dicom, bound):    
    new_d = np.copy(dicom)
    base_value = np.min(new_d)
    new_d = new_d - base_value #convert the min value to 0    
    new_d[new_d < bound[0] + (base_value*-1) ] = 0;
    new_d[new_d > bound[1] + (base_value*-1) ] = 0;    
    return new_d; 


def get_dicom_pixel_spacing(dicom):        
    #print( str(dicom.PixelSpacing[0]) + ' / ' + str(dicom.PixelSpacing[1])  )
    return dicom.PixelSpacing[0] * dicom.PixelSpacing[1]    



"""Set variables"""


# ************************************
# ************************************
# ************************************
# MODIFY THESE OPTIONS


img_width = 512             # The desired value for the width of the images
img_height = 512            # The desired value for the height of the images       

if(local):
    base_path = '/home/spidey/Git/cinvestav-tesis/unet'
else:
    base_path = 'gdrive/My Drive/Sarcopenia-TC-L3/dataset-tesis'    
#img_dataset_path =  os.path.join(base_path,  'dataset/v3/dicom', "")
img_dataset_path = '/mnt/Home/Proyectos/Maestria/14-tesis/dicoms/dicoms-v3/dataset/dicom2'
#prediction_path = os.path.join(base_path, 'dataset/v3_2/predict', "")
prediction_path = '/mnt/Home/Proyectos/Maestria/14-tesis/dicoms/dicoms-v3/entrenamiento/temp3/'
model_path =     os.path.join(base_path, "model", "")
model_file = os.path.join(model_path,'model_dicom_v4_92.h5')

tissue_hu = [-29, 150]


# END MODIFY THESE OPTIONS
# ************************************
# ************************************
# ************************************

print_var_summary()


"""Read the dataset"""


print('\n******************************************************')
print('Reading images...')


img_reader = reader.Reader( 
    files_path = img_dataset_path,
    type_ = 'dicom',   # 2 types posible {'dicom', 'mask'}
    img_width = img_width, 
    img_height = img_height    
)


"""model - load weights and predict"""

print('\n******************************************************')
print('Prediction...')

# create unet model
unet = model.UNet(
    shape = img_reader.imgs[0].shape,         
    local = local
)

# load checkpoint if model_file exist
if( os.path.exists( model_file ) ):
    results = unet.load_checkpoint(model_file)  
else:
     raise AssertionError('Model could not be loaded')

    

"""calculate the results"""

print('\n******************************************************')
print('Results...')

# Predict images
predictions = unet.predict(img_reader.imgs, verbose=1)

# apply threshold
pred_masks = evaluation.pred_to_mask(predictions)


# first array for male data, second array for female, third for unknown
genders = ['male', 'female', 'unknown']
colors = ['blue', 'red', 'green']
xs =  [[] for _ in range(3)] 
ys =  [[] for _ in range(3)]


for index in range(img_reader.len_imgs):

    # convert to hu values
    dicom = img_reader.dicoms[index]
    dicom_hu = apply_modality_lut(dicom.pixel_array, dicom)
    # dicom1_hu = convert_to_hu(dicom)  #conversion propia, parece no funcionar
    
    # filter dicom with hounsfield units of lean muscle 
    dicom_hu_fil = filter_dicom(dicom_hu, tissue_hu)   

    #resize
    dicom_hu_fil = resize(dicom_hu_fil, (img_height, img_width), mode='constant',  preserve_range=True)
       
    
    # apply the mask
    mask = pred_masks[index]
    img_seg = dicom_hu_fil * mask    
    
    # convert to bool
    img_bool = np.where(img_seg > 0, 1, 0)
    
    # measure the cm2    
    per_pixel_area = get_dicom_pixel_spacing(dicom)
    num_white_pixels = np.count_nonzero(img_bool)
    
    # tomar en cuenta que el pixel spacing puede no corresponder con la imagen recortada, 
    # hacer calculos tomando el cuenta el recorte de la imagen
    cm = num_white_pixels * per_pixel_area
    
    cm2 = cm**(1/2)     
    print ('cm2: ' + str(cm2))
    
    patient_age, patient_sex = get_patient_info(dicom)
    if(patient_sex == 'M'):
        arr_i=0
    elif(patient_sex == 'F'):
        arr_i=1
    else:
        arr_i=2

    ys[arr_i].append(cm2)
    xs[arr_i].append(patient_age)



    #save, images
    #plot.imsave_res(dicom_hu_fil, mask, prediction_path, img_reader.filenames[index])    
    plot.imsave_res(dicom.pixel_array, mask, prediction_path, img_reader.filenames[index])    
    

# Create plot
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
for gender, color, x, y in zip(genders, colors, xs, ys):    
    ax.scatter(np.array(x), np.array(y), alpha=0.8, c=color, edgecolors='none', s=30, label=gender)

plt.title('Relation Lean muscle cm2 - age')
plt.legend(loc=2)
plt.show()


