# -*- coding: utf-8 -*-

"""Load packages"""

import os
import sys
##---------
import h5py
import numpy as np
#or
import reader 
##---------
import dataset
import plot
import model
import evaluation

assert sys.version_info[0] == 3, 'Python 3.x required'


local = True        # true for runing in local computer, false to run on google colab
if (local == False):    
    from google.colab import drive
    drive.mount('/content/gdrive')


def get_normal_shape(ds):
    shape = ds[0].shape           
    if( len(shape) == 3):
        if( shape[2] == 1):        
            ds = np.squeeze(ds)
    return ds

def print_var_summary():
    #cambiar las variables
    print('')
    print('---------------------- Variable  summary ----------------------')
    print('Image height: {} - width: {}'.format(img_height,img_width))
    print('Image (dicom) dataset path = {}'.format(img_dataset_path))    
    print('Masks dataset path = {}'.format(mask_dataset_path))
    print('H5py filename = {}'.format(h5py_dataset_file))   
    print('Predictions path = {}'.format(prediction_path))    
    print('Model filename = {}'.format(model_file))
    print('Results filename = {}'.format(results_file))
    
    
    print('Train fraction = {}'.format(train_fraction))
    print('Cross validation= {}'.format(cross_validation))    
    print('Augmentation times = {}'.format(augmentation_times))
    print('Shuffle train dataset = {}'.format(shuffle))    
        
    print('Batch size = {}'.format(batch_size))
    print('Learning rate = {}'.format(learning_rate))    
    print('Max epochs = {}'.format(max_epochs))
    print('Patience = {}'.format(patience))
    print('---------------------------------------------------------------')


"""Set variables"""


# ************************************
# ************************************
# ************************************
# MODIFY THESE OPTIONS


#------------------
#   image variables
#------------------
img_width = 512             # The desired value for the width of the images
img_height = 512            # The desired value for the height of the images       
number_classes = 2        # how many classes exist in your training dataset (e.g. 2 for binary segmentation)
                            # right now, the number_classes is not working, the model only works with 2 classes (binary mode)


#  paths
#------------------------
if(local):
    base_path = '/home/spidey/Git/cinvestav-tesis/unet'
else:
    base_path = 'gdrive/My Drive/Sarcopenia-TC-L3/dataset-tesis'    
img_dataset_path =  os.path.join(base_path, 'dataset/v3_3/dicom', '')
mask_dataset_path = os.path.join(base_path, 'dataset/v3_3/mask', '')
h5py_dataset_file = os.path.join(base_path, 'dataset/v3_3/dataset_v2.hdf5')
prediction_path =   os.path.join(base_path, 'dataset/v3_3/predict', '')
model_path =        os.path.join(base_path, 'model', '')
model_file =        os.path.join(model_path,'model_v4.h5')
results_file =      os.path.join(base_path, 'results', '01_baseline.txt')


#  dataset variables
#------------------------
train_fraction = 0  # define the fraction of the dataset for train the model
cross_validation  =  {"chunk":3, "total_chunks": 5} # ex. {"chunk": 1, "total_chunks": 5}
shuffle = True


#  augmenters variables
#------------------------
augmentation_times = 0      # number of times that the data set will be augmented, 0 for none
# set value of following augmenters between 0 and 1, ex 0.3
prob_scale = .5
prob_rotate = 1
prob_shear = .2
prob_translate = .4
prob_flipx = .3
prob_perspective = .3
prob_piecewise = .2
prob_blur = 0  # Be careful, blur and sharpen could be applied in the same image
                # no puede aplicarse a imagenes con 2 canales
prob_sharpen = 0 # esta haciendo las imagenes grises en el fondo
prob_noise = 1
prob_coarse_dropout = .2  # cutout, dropout or coarseDropout?

#   augmenters parameters
#----------------------------
val_scale = (0.8, 1.1) # ¿separar en x y y?  scale={"x": (0.5, 1.5), "y": (0.5, 1.5)}
val_rotate = (-20, 20)
val_shear = (-16, 16)
val_translate_percent= {"x": (-0.05, 0.05), "y": (-0.1, 0.1)}
val_perspective = (0.03, 0.075)
val_piecewise = (0.01, 0.05)
val_blur = (4, 7)  #programar para aplicar blur o sharpen
val_sharpen = (0.03, 0.12)
val_noise = 0.05
val_coarse_dropout = (0.08, 0.1)
val_coarse_dropout_percent = (0.1, 0.4)

#   u-net hyper-parameters
#----------------------------
learning_rate = 1e-4      # what learning rate should the network use
batch_size = 16
max_epochs = 150
patience = 3


# END MODIFY THESE OPTIONS
# ************************************
# ************************************
# ************************************


print_var_summary()

"""Read the dataset"""

print('\n******************************************************')
print('Reading images...')

if( os.path.exists(h5py_dataset_file) ):    

    with h5py.File(h5py_dataset_file, 'r') as hf:
        imgs = np.array(hf.get('imgs'))
        masks = np.array(hf.get('masks'))
        hf.close()    

    #imgs = imgs.astype(np.float32)    
    #masks = masks.astype(np.uint8)
    
    print('Creating and dividing dataset...')
    # create dataset train and test
    ds = dataset.Dataset(imgs, masks,                       
                         train_fraction = train_fraction, cross_validation = cross_validation)


else: 
    img_reader = reader.Reader( 
        files_path = img_dataset_path,
        type_ = 'dicom',   # 3 types posible {'dicom', 'mask_gray', 'mask_green'}
        img_width = img_width, 
        img_height = img_height    
    )
    
    mask_reader = reader.Reader(         
        files_path = mask_dataset_path,         
        type_ = 'mask_green',  
        img_width = img_width, 
        img_height = img_height     
    )    
    
    # create hp5y file    
    with h5py.File(h5py_dataset_file, 'w') as hf:
        #hf.create_dataset('dataset_1', data=d1, compression="gzip", compression_opts=9)
        hf.create_dataset("imgs", data=img_reader.imgs)
        hf.create_dataset("masks", data=mask_reader.imgs)
        hf.close()       
    
    print('Creating and dividing dataset...')
    # create dataset train and test
    ds = dataset.Dataset(img_reader.imgs, mask_reader.imgs,                       
                         train_fraction = train_fraction, cross_validation = cross_validation)



img_ex, mask_ex = ds.get_train_example( range_=(0,ds.len_train_imgs_ba-1) ) #original example
plot.imshow_overlay(img_ex, mask_ex)




"""Perform data augmentation"""



print('\n******************************************************')
print('Doing the augmentation...')


ds.augment(        
    augmentation_times = augmentation_times,    
    prob_scale = prob_scale,
    prob_rotate = prob_rotate,
    prob_shear = prob_shear,
    prob_translate = prob_translate,
    prob_flipx = prob_flipx,
    prob_perspective = prob_perspective,
    prob_piecewise = prob_piecewise,
    prob_blur = prob_blur,
    prob_sharpen = prob_sharpen,
    prob_noise = prob_noise,
    prob_coarse_dropout = prob_coarse_dropout,
    val_scale = val_scale,
    val_rotate = val_rotate,
    val_shear = val_shear,
    val_translate_percent = val_translate_percent,
    val_perspective = val_perspective,
    val_piecewise = val_piecewise,
    val_blur = val_blur,
    val_sharpen = val_sharpen,
    val_noise = val_noise,
    val_coarse_dropout = val_coarse_dropout,
    val_coarse_dropout_percent = val_coarse_dropout_percent
)


"""plot examples"""    
#img_ex, mask_ex = ds.get_train_example( range_=(ds.len_train_imgs_ba,ds.len_train_imgs_aa-1) ) #augmented example
#plot.imshow_overlay(img_ex, mask_ex)
'''
plot.imshow_pair(img_ex, mask_ex)
plot.histogram(mask_ex,250)
plot.histogram(img_ex,2500)
plot.histogram(img_ex,255,xlabel='Intensity Value', ylabel='Count') # histogram for image in color
'''



# suffle img and mask datasets without breaking the correspondance
if(shuffle):
    ds.train_suffle()
    

"""model - training"""

print('\n******************************************************')

# create unet model
unet = model.UNet(
    #shape = img_reader.get_image_shape(),
    shape = imgs[0].shape,
    number_classes = number_classes,    
    learning_rate = learning_rate,
    local = local
)

#unet.summary()

# load checkpoint if model_file exist
if( os.path.exists( model_file ) ):
    results = unet.load_checkpoint(model_file)  
else:
    print('Training model...')
    results = unet.fit(
            ds.train_imgs,
            ds.train_masks,            
            model_path = model_path,
            model_file = model_file,
            validation_split=0.1, 
            batch_size = batch_size, 
            epochs = max_epochs, 
            patience = patience
        )


print('\n******************************************************')
print('Evaluation... and gauging the similarity (jaccard)')

# Predict images
predictions = unet.predict(ds.test_imgs, verbose=1)

pred_masks = evaluation.pred_to_mask(predictions)

if(results_file != None and results_file != ''):  
    file_r = open(results_file, "a")  
    file_r.write('cross validation: {}'.format(cross_validation))
    total_score = evaluation.jaccard(ds.test_masks, pred_masks, file_r)      
    file_r.write('\n\n*************************************\n\n')
    file_r.close()
else:
    total_score = evaluation.jaccard(ds.test_masks, pred_masks) 



# plot the images
index = 0
title_img = '(left) groundtruth, (center) prediction, (right) overlapped'
plot.imshow_prediction(ds.test_imgs[index], ds.test_masks[index], pred_masks[index], title = title_img)
