import numpy as np
from tqdm import tqdm
from sklearn.metrics import jaccard_score
from skimage.filters import threshold_multiotsu
import plot

"""predict"""

# scores
# jaccard
# Dice Coefficient
# Sensitivity
# Specificity

    
def get_normal_shape(ds):
    shape = ds[0].shape           
    if( len(shape) == 3):
        if( shape[2] == 1):        
            ds = np.squeeze(ds)
    return ds
        

def pred_to_mask(predictions):        
    '''    
    print(skimage.filters.threshold_isodata(x))
    print(skimage.filters.threshold_li(x))
    print(skimage.filters.threshold_mean(x))
    print(skimage.filters.threshold_minimum(x))
    print(skimage.filters.threshold_multiotsu(x)[0]) #best
    print(skimage.filters.threshold_otsu(x))
    print(skimage.filters.threshold_triangle(x))
    print(skimage.filters.threshold_yen(x))
    '''
    '''
    from skimage import exposure        
    p1, p2 = np.percentile(x, (0.0009692,  0.00290759))
    img_rescale = exposure.rescale_intensity(x, in_range=(p1, p2))
    
    # Equalization
    img_eq = exposure.equalize_hist(x)
    
    # Adaptive Equalization
    img_adapteq = exposure.equalize_adapthist(x, clip_limit=0.03)
    '''
    
    #pred_mask = get_normal_shape(predictions)
    pred_mask = np.copy(predictions)
    for index, mask in enumerate(pred_mask):                
        #get the low part of multiotsu and then substract 5% of the total value
        th_otsu = threshold_multiotsu(mask)[0]
        threshold = th_otsu - (np.max(mask)*.05)
        pred_mask[index] = mask>threshold
        #print('th_otsu_low_value: ', th_otsu, ' threshold: ', threshold)            
    
    return np.squeeze(pred_mask.astype(np.uint8))
   
   

def jaccard(ground_truth, pred_mask, file_r = None):    
    # convert predictions to normal shape    
    y_true = get_normal_shape(ground_truth).astype(np.bool)
    y_pred = get_normal_shape(pred_mask).astype(np.bool)        

    total_score = 0
    for index, img in enumerate(y_true):
        #programar para que muestre el nombre de cada archivo
        #score = jaccard_score(test_masks, y_pred, labels=None, pos_label=1, average='binary', sample_weight=None)
        score = jaccard_score(y_true[index].ravel(), y_pred[index].ravel(), average='binary')
        total_score += score        
        if(file_r != None):
            file_r.write('\n    mask {:03d} jaccard score: {}'.format(index, str(score)))
        else:
            print('    mask {:03d} jaccard score: {}'.format(index, str(score)))    
    if(file_r != None):
        file_r.write('\navg jaccard score of {} masks is : {}'.format(len(y_true), total_score / len(y_true)))
    else:
        print('avg jaccard score of {} masks is : {}'.format(len(y_true), total_score / len(y_true)))
    
    return total_score / len(y_true)
