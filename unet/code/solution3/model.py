import os
import numpy as np
import tensorflow as tf
from tensorflow import keras

assert int(tf.__version__.split('.')[0]) == 2, 'Tensorflow 2.x.x required'

gpus = tf.config.experimental.list_physical_devices('GPU')
if (gpus):         
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
      try:          
          tf.config.experimental.set_memory_growth                     
          #tf.config.experimental.set_virtual_device_configuration(gpus[0], [tf.config.experimental.VirtualDeviceConfiguration(memory_limit=1024)])          
      except RuntimeError as e:
        print(e)            
        
'''
import tensorflow as tf
tf.compat.v1.disable_eager_execution()
hello = tf.constant('Hello, Welcome')
sess = tf.compat.v1.Session()
print(sess.run(hello))
'''

'''
with tf.compat.v1.Session() as sess:
    sess.close()
'''

class UNet():
    
    def __init__(self, shape, number_classes = 2, learning_rate=3e-4, local = True):        
        
    
        self.shape = self.get_shape_for_unet(shape)
        self.number_classes = number_classes
        self.learning_rate = learning_rate        
             
        self.model = self.build_model()
        #self.loss_fn = tf.keras.losses.CategoricalCrossentropy(from_logits=False, label_smoothing=label_smoothing, reduction=tf.keras.losses.Reduction.NONE)                
        
        optimizer = tf.keras.optimizers.Adam(learning_rate=self.learning_rate)
        self.model.compile(optimizer=optimizer, loss="binary_crossentropy", metrics=["acc"])
        
    
    '''start of model definition'''
    @staticmethod
    def _conv_layer(input, filters, kernel_size=(3, 3), padding="same", activation=tf.keras.activations.relu, stride=1, k_i='he_normal'):
        output = tf.keras.layers.Conv2D(filters=filters,
                                        kernel_size=kernel_size,                                        
                                        padding=padding,
                                        activation=activation,
                                        kernel_initializer=k_i,
                                        strides=stride)(input)                                        
        #output = tf.keras.layers.BatchNormalization(axis=1)(output)
        return output    
    

    def down_block(self, x, filters):
        c = UNet._conv_layer(x, filters)        
        c = tf.keras.layers.Dropout(0.1)(c)    
        c = UNet._conv_layer(c, filters)                
        #p = tf.keras.layers.MaxPooling2D((2, 2))(c)
        p = keras.layers.MaxPool2D((2, 2), (2, 2))(c)        
        return c, p
    
    
    def up_block(self, x, skip, filters):
        #us = tf.keras.layers.Conv2DTranspose(128, (2, 2), strides=(2, 2), padding='same')(x) 
        us = keras.layers.UpSampling2D((2, 2))(x)
        #u6 = tf.keras.layers.concatenate([u6, c4])
        concat = keras.layers.Concatenate()([us, skip])        
        c = UNet._conv_layer(concat, filters)                
        c = tf.keras.layers.Dropout(0.2)(c)
        c = UNet._conv_layer(c, filters)                        
        return c        
    
    
    def bottleneck(self, x, filters):
        c = UNet._conv_layer(x, filters)          
        c = tf.keras.layers.Dropout(0.3)(c)
        c = UNet._conv_layer(c, filters)  
        return c
       
    
    def build_model(self):
        f = [16, 32, 64, 128, 256]        
        
        # self.inputs = tf.keras.Input(shape=(number_channels, None, None))
        inputs = keras.layers.Input(self.shape)
        p0 = inputs
        c1, p1 = self.down_block(p0, f[0]) #128 -> 64
        c2, p2 = self.down_block(p1, f[1]) #64 -> 32
        c3, p3 = self.down_block(p2, f[2]) #32 -> 16
        c4, p4 = self.down_block(p3, f[3]) #16->8
        
        bn = self.bottleneck(p4, f[4])
        
        u1 = self.up_block(bn, c4, f[3]) #8 -> 16
        u2 = self.up_block(u1, c3, f[2]) #16 -> 32
        u3 = self.up_block(u2, c2, f[1]) #32 -> 64
        u4 = self.up_block(u3, c1, f[0]) #64 -> 128
        
        outputs = keras.layers.Conv2D(1, (1, 1), padding="same", activation="sigmoid")(u4)
        #model = tf.keras.Model(inputs=[inputs], outputs=[outputs])
        model = keras.models.Model(inputs, outputs)                
        return model
    '''end of model definition'''


    def fit(self, imgs, masks, model_path='.', model_file='sarco_model.h5', validation_split=0.1, batch_size=1, epochs=100, patience=8):
        
        if not os.path.exists(model_path):
            print('Creating folder {}'.format(model_path))
            os.makedirs(model_path)
                
        #Tensorboard callback
        callbacks = [
            tf.keras.callbacks.EarlyStopping(patience=patience, monitor='val_loss', verbose=0), #Early stopping 
            # tf.keras.callbacks.TensorBoard(log_dir='logs'),            
            tf.keras.callbacks.ModelCheckpoint(model_file, verbose=0, save_best_only=True) # Model checkpoint - Save best only
        ]        
        
        imgs_ = self.convert_ds_as_unet_shape(imgs)
        masks_ = self.convert_ds_as_unet_shape(masks)
        result = self.model.fit(imgs_, masks_, validation_split=validation_split, 
                                batch_size=batch_size, epochs=epochs, callbacks=callbacks)        
        return result
                              


    def predict(self, test_imgs, verbose=1):
        test_imgs_ = self.convert_ds_as_unet_shape(test_imgs)
        return self.model.predict(test_imgs_, verbose=verbose)
    
    def summary(self):
        self.model.summary()
    
    
    def load_checkpoint(self, model_file: str):
        self.model.load_weights(model_file)
        #checkpoint = tf.train.Checkpoint(optimizer=self.optimizer, model=self.model)
        #checkpoint.restore(checkpoint_filepath).expect_partial()
  

    def convert_ds_as_unet_shape(self, dataset):
        if(dataset[0].max()>255):
            max_val = 2500
        else:
            max_val = 255        
        shape_ = ( len(dataset), self.shape[0], self.shape[1], self.shape[2] )
        
        #mejorar esta parte del codigo
        #¿create new array or resize and convert with astype?
        ds_unet = np.zeros( shape_, np.float32)        
        
        if(self.shape[2] == 1):
            for index, img in enumerate(dataset):
                #normalize values from 0 to 1 and expand dims from HW to HWC
                ds_unet[index] = np.expand_dims( img / max_val, axis=-1)  
        else:             
            for index, img in enumerate(dataset):
                #only normalize values from 0 to 1
                ds_unet[index] = img / max_val        
        
        ds_unet = ds_unet.astype(np.float32)
        return ds_unet
        
    
    def get_shape_for_unet(self, shape):        
        if( len(shape) == 2 ):        
            shape_ = ( shape[0], shape[1], 1)
        else:
            shape_ = ( shape[0], shape[1], shape[2])
        return shape_
       
 
        

    '''
    def _build_model(self):
        # convert layers integers to float
        inputs = tf.keras.layers.Input((self.img_height, self.img_width, self.number_channels)) 
        s = tf.keras.layers.Lambda(lambda x: x / 255)(inputs)

        # Contracting Path
        c1 = tf.keras.layers.Conv2D(16, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(s)
        c1 = tf.keras.layers.Dropout(0.1)(c1)
        c1 = tf.keras.layers.Conv2D(16, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c1)
        p1 = tf.keras.layers.MaxPooling2D((2, 2))(c1)

        c2 = tf.keras.layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(p1)
        c2 = tf.keras.layers.Dropout(0.1)(c2)
        c2 = tf.keras.layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c2)
        p2 = tf.keras.layers.MaxPooling2D((2, 2))(c2)

        c3 = tf.keras.layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(p2)
        c3 = tf.keras.layers.Dropout(0.2)(c3)
        c3 = tf.keras.layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c3)
        p3 = tf.keras.layers.MaxPooling2D((2, 2))(c3)

        c4 = tf.keras.layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(p3)
        c4 = tf.keras.layers.Dropout(0.2)(c4)
        c4 = tf.keras.layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c4)
        p4 = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(c4)

        c5 = tf.keras.layers.Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(p4)
        c5 = tf.keras.layers.Dropout(0.3)(c5)
        c5 = tf.keras.layers.Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c5)

        # Expansive path
        u6 = tf.keras.layers.Conv2DTranspose(128, (2, 2), strides=(2, 2), padding='same')(c5) 
        u6 = tf.keras.layers.concatenate([u6, c4])
        c6 = tf.keras.layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(u6)
        c6 = tf.keras.layers.Dropout(0.2)(c6)
        c6 = tf.keras.layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c6)

        u7 = tf.keras.layers.Conv2DTranspose(64, (2, 2), strides=(2, 2), padding='same')(c6) 
        u7 = tf.keras.layers.concatenate([u7, c3])
        c7 = tf.keras.layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(u7)
        c7 = tf.keras.layers.Dropout(0.2)(c7)
        c7 = tf.keras.layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c7)

        u8 = tf.keras.layers.Conv2DTranspose(32, (2, 2), strides=(2, 2), padding='same')(c7) 
        u8 = tf.keras.layers.concatenate([u8, c2])
        c8 = tf.keras.layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(u8)
        c8 = tf.keras.layers.Dropout(0.1)(c8)
        c8 = tf.keras.layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c8)

        u9 = tf.keras.layers.Conv2DTranspose(16, (2, 2), strides=(2, 2), padding='same')(c8) 
        u9 = tf.keras.layers.concatenate([u9, c1], axis=3)
        c9 = tf.keras.layers.Conv2D(16, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(u9)
        c9 = tf.keras.layers.Dropout(0.1)(c9)
        c9 = tf.keras.layers.Conv2D(16, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c9)

        outputs = tf.keras.layers.Conv2D(1, (1, 1), activation='sigmoid')(c9)

        unet = tf.keras.Model(inputs=[inputs], outputs=[outputs])
        unet.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy']) 
        # model.summary()
        
        return unet
'''   