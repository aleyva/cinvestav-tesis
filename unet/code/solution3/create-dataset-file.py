# -*- coding: utf-8 -*-

"""Load packages"""

import os
import sys
##---------
import h5py
import numpy as np
#or
import reader 
##---------
import dataset
import plot
import model
import evaluation

assert sys.version_info[0] == 3, 'Python 3.x required'


local = True        # true for runing in local computer, false to run on google colab
if (local == False):    
    from google.colab import drive
    drive.mount('/content/gdrive')


def get_normal_shape(ds):
    shape = ds[0].shape           
    if( len(shape) == 3):
        if( shape[2] == 1):        
            ds = np.squeeze(ds)
    return ds

def print_var_summary():
    #cambiar las variables
    print('')
    print('---------------------- Variable  summary ----------------------')
    print('Image height: {} - width: {}'.format(img_height,img_width))
    print('Image (dicom) dataset path = {}'.format(img_dataset_path))    
    print('Masks dataset path = {}'.format(mask_dataset_path))
    print('H5py filename = {}'.format(h5py_dataset_file))       
    
    
    print('Train fraction = {}'.format(train_fraction))
    print('Cross validation= {}'.format(cross_validation))        
    print('Shuffle train dataset = {}'.format(shuffle))            
    
    print('---------------------------------------------------------------')


"""Set variables"""


# ************************************
# ************************************
# ************************************
# MODIFY THESE OPTIONS


#------------------
#   image variables
#------------------
img_width = 512             # The desired value for the width of the images
img_height = 512            # The desired value for the height of the images       
number_classes = 2        # how many classes exist in your training dataset (e.g. 2 for binary segmentation)
                            # right now, the number_classes is not working, the model only works with 2 classes (binary mode)


#  paths
#------------------------
if(local):
    base_path = '/home/spidey/Git/cinvestav-tesis/unet'    
else:
    base_path = 'gdrive/My Drive/Sarcopenia-TC-L3/dataset-tesis'    
img_dataset_path =  os.path.join('/mnt/Home/Proyectos/Maestria/14-tesis/dicoms/dicoms-v3/dataset-model-expertSeg/mask-v3_6 (actual)', 'dicom', '')
mask_dataset_path = os.path.join('/mnt/Home/Proyectos/Maestria/14-tesis/dicoms/dicoms-v3/dataset-model-expertSeg/mask-v3_6 (actual)', 'mask', '')
h5py_dataset_file = os.path.join('/mnt/Home/Proyectos/Maestria/14-tesis/dicoms/dicoms-v3/dataset-model-expertSeg/mask-v3_6 (actual)', 'dataset_v3_6.hdf5')



#  dataset variables
#------------------------
train_fraction = .9  # define the fraction of the dataset for train the model
cross_validation  =  0#{"chunk":3, "total_chunks": 5} # ex. {"chunk": 1, "total_chunks": 5}
shuffle = True


# END MODIFY THESE OPTIONS
# ************************************
# ************************************
# ************************************


print_var_summary()

"""Read the dataset"""

print('\n******************************************************')
print('Create dataset file...')

img_reader = reader.Reader( 
    files_path = img_dataset_path,
    type_ = 'dicom',   # 3 types posible {'dicom', 'mask_gray', 'mask_green'}
    img_width = img_width, 
    img_height = img_height    
)

mask_reader = reader.Reader(         
    files_path = mask_dataset_path,         
    type_ = 'mask_green',  
    img_width = img_width, 
    img_height = img_height     
)    

# create hp5y file    
with h5py.File(h5py_dataset_file, 'w') as hf:
    #hf.create_dataset('dataset_1', data=d1, compression="gzip", compression_opts=9)
    hf.create_dataset("imgs", data=img_reader.imgs)
    hf.create_dataset("masks", data=mask_reader.imgs)
    hf.close()       

print('Creating and dividing dataset...')
# create dataset train and test
ds = dataset.Dataset(img_reader.imgs, mask_reader.imgs,                       
                     train_fraction = train_fraction, cross_validation = cross_validation)




img_ex, mask_ex = ds.get_train_example( range_=(0,ds.len_train_imgs_ba-1) ) #original example
plot.imshow_overlay(img_ex, mask_ex)


