# -*- coding: utf-8 -*-

"""Load packages"""

import os
import sys
import numpy as np
import reader
import model
import plot
from skimage.transform import resize 
from pydicom.pixel_data_handlers.util import apply_modality_lut



assert sys.version_info[0] == 3, 'Python 3.x required'


local = True        # true for runing in local computer, false to run on google colab
if (local == False):     
    from google.colab import drive
    drive.mount('/content/gdrive')




def print_var_summary():
    print('')
    print('---------------------- Variable  summary ----------------------')
    print('Image height: {} - width: {}'.format(img_height,img_width))
    print('Image (dicom) dataset path = {}'.format(img_dataset_path))    
    print('Predictions path = {}'.format(prediction_path))    
    print('Model filename = {}'.format(model_file)) 
    print('---------------------------------------------------------------')


def print_dicom_info(dataset):
    # Normal mode:
    print()    
    print("Storage type.....:", dataset.SOPClassUID)    
    display_name =  dataset.PatientName.family_name + ", " +  dataset.PatientName.given_name
    print("Patient's name...:", display_name)
    print("Patient id.......:", dataset.PatientID)
    print("Modality.........:", dataset.Modality)
    print("Study Date.......:", dataset.StudyDate)    
    if 'PixelData' in dataset:
        rows = int(dataset.Rows)
        cols = int(dataset.Columns)
        print("Image size.......: {rows:d} x {cols:d}, {size:d} bytes".format(
            rows=rows, cols=cols, size=len(dataset.PixelData)))
        if 'PixelSpacing' in dataset:
            print("Pixel spacing....:", dataset.PixelSpacing)
    
    # use .get() if not sure the item exists, and want a default value if missing
    print("Slice location...:", dataset.get('SliceLocation', "(missing)"))    
    data = dataset.pixel_array
    print('slice shape..........: ' + str(data.shape) )
    print('slice data type......: ' + str(data.dtype) )
    print('min and max in array.: {} - {}'.format(str(np.amin(data)),str(np.amax(data))) )

'''
def get_age(dicom):    
    age_str = dicom.PatientAge    
    age = age_str.replace("Y", "")    
    return int(age)
'''


def get_patient_info(dicom)
    age_str = dicom.PatientAge    
    patient_age = int( age_str.replace("Y", "") )
    patient_sex = dicom.PatientSex
    return patient_age, patient_sex 


def filter_dicom(dicom, bound):    
    new_d = np.copy(dicom)
    base_value = np.min(new_d)
    new_d = new_d - base_value #convert the min value to 0    
    new_d[new_d < bound[0] + (base_value*-1) ] = 0;
    new_d[new_d > bound[1] + (base_value*-1) ] = 0;    
    return new_d; 


def get_dicom_pixel_spacing(dicom):        
    #print( str(dicom.PixelSpacing[0]) + ' / ' + str(dicom.PixelSpacing[1])  )
    return dicom.PixelSpacing[0] * dicom.PixelSpacing[1]    


# Normal option
# hu = pixel_value * slope + intercept
# hu_img = dataset.pixel_array*dataset.RescaleSlope+dataset.RescaleIntercept

# Direct option, only pydicom
# hu = apply_modality_lut(dataset.pixel_array, dataset)

# HU = CTNumber (7FE0,0010) * rescale slope (0028, 1053) + y intercept (0028, 1052). 
# These HU values have to be converted to pixel intensity f(x,y) using f(x,y) = ((HU-P1)*pow(2,i-1))/W, 
# where P1 is the window left border, i is the radiometric resolution and W is the window width. 

# Other methoth not using pydicom
def convert_to_hu(dataset):
    # image = np.stack([s.pixel_array for s in scans])
    image = dataset.pixel_array
    
    # Convert to int16 (from sometimes int16), 
    # should be possible as values should always be low enough (<32k)
    image = image.astype(np.int16)

    # Set outside-of-scan pixels to 1
    # The intercept is usually -1024, so air is approximately 0
    image[image == -2000] = 0
    
    # Convert to Hounsfield units (HU)
    intercept = dataset.RescaleIntercept
    slope = dataset.RescaleSlope
    
    if slope != 1:
        image = slope * image.astype(np.float64)
        image = image.astype(np.int16)
        
    image += np.int16(intercept)
    
    return np.array(image, dtype=np.int8)




"""Set variables"""


# ************************************
# ************************************
# ************************************
# MODIFY THESE OPTIONS


img_width = 512             # The desired value for the width of the images
img_height = 512            # The desired value for the height of the images       

if(local):
    base_path = '/home/spidey/Git/cinvestav-tesis/unet'
else:
    base_path = 'gdrive/My Drive/Sarcopenia-TC-L3/dataset-tesis'    
#img_dataset_path =  os.path.join(base_path,  'dataset/v3/dicom', "")
img_dataset_path = '/mnt/Home/Proyectos/Maestria/14-tesis/dicoms/dicoms-v3/dicom'
#prediction_path = os.path.join(base_path, 'dataset/v3_2/predict', "")
prediction_path = '/mnt/Home/Proyectos/Maestria/14-tesis/dicoms/dicoms-v3/resultado-entrenamiento-02/'
model_path =     os.path.join(base_path, "model", "")
model_file = os.path.join(model_path,'model_v3.h5')

tissue_hu = [-29, 150]
threshold = 0.002          # this is the optimal threschold for the dynamic range [0, 2500]


# END MODIFY THESE OPTIONS
# ************************************
# ************************************
# ************************************

print_var_summary()


"""Read the dataset"""


print('\n******************************************************')
print('Reading images...')


img_reader = reader.Reader( 
    files_path = img_dataset_path,
    type_ = 'dicom',   # 2 types posible {'dicom', 'mask'}
    img_width = img_width, 
    img_height = img_height    
)


"""model - load weights and predict"""

print('\n******************************************************')
print('Prediction...')

# create unet model
unet = model.UNet(
    shape = img_reader.imgs[0].shape,         
    local = local
)

# load checkpoint if model_file exist
if( os.path.exists( model_file ) ):
    results = unet.load_checkpoint(model_file)  
else:
     raise AssertionError('Model could not be loaded')

# Predict images
predictions = unet.predict(img_reader.imgs, verbose=1)

"""calculate the results"""

print('\n******************************************************')
print('Results...')

# apply threshold
preds_test = np.squeeze( (predictions > threshold).astype(np.uint8) )

'''
matrix = []
matrix.append([])
matrix.append([])
matrix[0].append(2)
matrix[1].append(3)
'''
# first array for male data, second array for female, third for unknown
genders = ['male', 'female', 'unknown']
colors = ['blue', 'red', 'green']
xs =  [[] for _ in range(3)] 
ys =  [[] for _ in range(3)]


for index in range(img_reader.len_imgs):

    # convert to hu values
    dicom = img_reader.dicoms[index]
    dicom_hu = apply_modality_lut(dicom.pixel_array, dicom)
    # dicom1_hu = convert_to_hu(dicom)  #conversion propia, parece no funcionar
    
    # filter dicom with hounsfield units of lean muscle 
    dicom_hu_fil = filter_dicom(dicom_hu, tissue_hu)    
    
    #resize
    dicom_hu_fil = resize(dicom_hu_fil, (img_height, img_width), mode='constant',  preserve_range=True)
    
    # apply the mask
    mask = preds_test[index]
    img_seg = dicom_hu_fil * mask    
    
    # convert to bool
    img_bool = np.where(img_seg > 0, 1, 0)
    
    # measure the cm2    
    per_pixel_area = get_dicom_pixel_spacing(dicom)
    num_white_pixels = np.count_nonzero(img_bool)
    
    # tomar en cuenta que el pixel spacing puede no corresponder con la imagen recortada, 
    # hacer calculos tomando el cuenta el recorte de la imagen
    cm = num_white_pixels * per_pixel_area
    
    cm2 = cm**(1/2)     
    print ('cm2: ' + str(cm2))
    
    patient_age, patient_sex = get_patient_info(dicom)
    if(patient_sex = 'M'):
        arr_i=0
    elif(patient_sex = 'F'):
        arr_i=1
    else:
        arr_i=2

    ys[arr_i].append(cm2)
    xs[arr_i].append(patient_age)

    #save, images
    #plot.imsave_res(dicom.pixel_array, mask, prediction_path, img_reader.filenames[index])    
    


'''
import matplotlib.pyplot as plt
plt.plot(np.array(x), np.array(y), 'o', color='black');
plt.xlabel('Age')
plt.ylabel('cm2')
plt.title('Relation Lean muscle cm2 - age')
plt.show()
'''
# Create plot
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1, axisbg="1.0")
for gender, color, x, y in zip(genders, colors, xs, ys):    
    ax.scatter(np.array(x), np.array(y), alpha=0.8, c=color, edgecolors='none', s=30, label=gender)

plt.title('Relation Lean muscle cm2 - age')
plt.legend(loc=2)
plt.show()