
import sys
if sys.version_info[0] < 3:
    raise RuntimeError('Python3 required')
from tqdm import tqdm 
import numpy as np

import tensorflow as tf
tf_version = tf.__version__.split('.')
'''
if int(tf_version[0]) != 2:
    raise RuntimeError('Tensorflow 2.x.x required')
'''

#own functions and classes
import readdataset
import augment
import model


img_width = 512 
img_height = 512
number_channels = 1


# --------------------
#  read the data set
# --------------------
# ¿Save the data set to file?

# images parameters
base_path = '/home/spidey/Git/cinvestav-tesis/code/unet'

#¿Es necesario saber el canal de las imagenes?

data_train_f, data_test_f = readdataset.read_gray_dataset( 
    train_img_path = base_path + '/dataset/v2-train/gray', 
    train_mask_path = base_path + '/dataset/v2-train/mask',     
    test_dic_path = base_path + '/dataset/v2-test/gray', 
    img_width = img_width, 
    img_height = img_height,
    number_channels = number_channels
)


# ------------------------
#  make data augmentation
# ------------------------

# augmentation parameters        
reflection_flag = True
rotation_flag = True
jitter_augmentation_severity = 0.1  # x% of a FOV
noise_augmentation_severity = 0.00  # vary noise by x% of the dynamic range present in the image
scale_augmentation_severity = 0.1  # vary size by x%
blur_max_sigma = 0  # pixels
intensity_augmentation_severity = None  # vary intensity by x% of the dynamic range present in the image


np.empty([2, 2], dtype=int)

x_train_aug = []
y_train_aug = []


for times in range(1,2):
    for index, file in tqdm(enumerate(data_train_f[0])) :      
        I = data_train_f[0][index]
        M = data_train_f[1][index]
            
        I = I.astype(np.float32)

        
        # perform image data augmentation
        I_, M_ = augment.augment_image(I, M,
            reflection_flag=reflection_flag,
            rotation_flag=rotation_flag,
            jitter_augmentation_severity=jitter_augmentation_severity,
            noise_augmentation_severity=noise_augmentation_severity,
            scale_augmentation_severity=scale_augmentation_severity,
            blur_augmentation_max_sigma=blur_max_sigma,
            intensity_augmentation_severity=intensity_augmentation_severity)
        
        # format the image into a tensor
        # reshape into tensor (CHW)
        I_ = I_.transpose((2, 0, 1))
        # I_ = I_.astype(np.float32)
        # I_ = zscore_normalize(I_)
        
        # M_ = M.astype_(np.int32)
        x_train_aug.append(I_)
        y_train_aug.append(M_)
        # plot the image using matplotlib
     



'''
index = random.randint(0, len(X_train)-1)
print('Dicom')
plt.imshow(np.squeeze(I))
plt.show()
print('New Dicom')
plt.imshow(np.squeeze(I_))
plt.show()

print('mask')
plt.imshow(np.squeeze(M))
plt.show()
print('New mask')
plt.imshow(np.squeeze(M_))
plt.show()
'''
        

# -----------
#   model - training
# -----------

'''
# how many classes exist in your training dataset (e.g. 2 for binary segmentation)
number_classes=2

# what learning rate should the network use
learning_rate=1e-4

use_augmentation=1 # {0, 1}
balance_classes=1 # whether to balance classes [0 = false, 1 = true]

reader_count = 1 #how many threads to use for disk I/O and augmentation per gpu
'''

model = UNet(img_width, img_height, number_channels)

# Model checkpoint - save model after every epoch.
# Save best only

checkpointer = tf.keras.callbacks.ModelCheckpoint(base_path + '/model-file/l3_model.h5', verbose=1, save_best_only=True)

#Early stopping 
#Tensorboard callback
callbacks = [
    tf.keras.callbacks.EarlyStopping(patience=3, monitor='val_loss'),
    tf.keras.callbacks.TensorBoard(log_dir='logs'),
    checkpointer
]

results = model.fit(x_train_aug, y_train_aug, validation_split=0.1, batch_size=16, epochs=100, callbacks=callbacks)


# ------------
#   results
# ------------

## check random training samples 
#No need to Load model if it is already in memory...

preds_test = model.predict(data_test_f, verbose=1)

#Each pixel is given a value between 0 and 1. He set a threshold .5 to binarizel 
# Apply threshold to binarize the image
preds_test_t = (preds_test > 0.45).astype(np.uint8)

#plt.imshow(np.squeeze((preds_test[1] > 0.25).astype(np.uint8)))

# check random training samples 
ix = random.randint(0, len(preds_test_t)) 
plt.imshow(np.squeeze(preds_test_t[ix]),cmap='gray')
plt.show()





# -----------------
#   saving results
# -----------------

'''
filename_I = NEW_TRAIN_DIC_PATH + '{0:03d}'.format(index*times) + '.jpg'            
imsave(filename_I, np.squeeze(I_))

filename_M = NEW_TRAIN_MASK_PATH + '{0:03d}'.format(index*times) + '.jpg'            
imsave(filename_M, np.squeeze(M_))
'''