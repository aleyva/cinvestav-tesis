import pydicom
from pydicom.pixel_data_handlers.util import apply_modality_lut
import numpy as np
import matplotlib.pyplot as plt
import sys
import glob


def print_general_info(dataset):
    # Normal mode:
    print()
    print("Filename.........:", filename)
    print("Storage type.....:", dataset.SOPClassUID)
    print()
    
    pat_name = dataset.PatientName
    display_name = pat_name.family_name + ", " + pat_name.given_name
    print("Patient's name...:", display_name)
    print("Patient id.......:", dataset.PatientID)
    print("Modality.........:", dataset.Modality)
    print("Study Date.......:", dataset.StudyDate)
    
    if 'PixelData' in dataset:
        rows = int(dataset.Rows)
        cols = int(dataset.Columns)
        print("Image size.......: {rows:d} x {cols:d}, {size:d} bytes".format(
            rows=rows, cols=cols, size=len(dataset.PixelData)))
        if 'PixelSpacing' in dataset:
            print("Pixel spacing....:", dataset.PixelSpacing)
    
    # use .get() if not sure the item exists, and want a default value if missing
    print("Slice location...:", dataset.get('SliceLocation', "(missing)"))
    
    print('slice shape..........: ' + str(dataset.pixel_array.shape) )
    print('slice data type......: ' + str(dataset.pixel_array.dtype) )
    print('min and max in array.: ' + str(np.amin(dataset.pixel_array)) + ' - ' +  str(np.amax(dataset.pixel_array)))



def get_mask_in_range(img, range_):    
    min_ = img.min()-1
    img[img < range_[0]] = min_
    img[img > range_[1]] = min_ 
    img[img != min_] = 1
    img[img == min_] = 0
    return np.array(img, dtype=np.bool)


def get_mask_greater_than(img, max_v):    
    min_ = img.min()-1
    img[img < max_v] = min_
    img[img >= max_v] = 1    
    img[img == min_] = 0
    return np.array(img, dtype=np.bool)

def get_mask_less_than(img, min_v):    
    max_ = img.max()+1
    img[img > min_v] = max_
    img[img <= min_v] = 1    
    img[img == max_] = 0
    return np.array(img, dtype=np.bool)



def plot_hu_histogram(img):
    plt.hist(img.flatten(), bins=50, color='c')
    plt.xlabel("Hounsfield Units (HU)")
    plt.ylabel("Frequency")
    plt.show()

#otro metodo sin usar pydicom
def get_pixels_hu(dataset):
    # image = np.stack([s.pixel_array for s in scans])
    image = dataset.pixel_array
    
    # Convert to int16 (from sometimes int16), 
    # should be possible as values should always be low enough (<32k)
    image = image.astype(np.int16)

    # Set outside-of-scan pixels to 1
    # The intercept is usually -1024, so air is approximately 0
    image[image == -2000] = 0
    
    # Convert to Hounsfield units (HU)
    intercept = dataset.RescaleIntercept
    slope = dataset.RescaleSlope
    
    if slope != 1:
        image = slope * image.astype(np.float64)
        image = image.astype(np.int16)
        
    image += np.int16(intercept)
    
    return np.array(image, dtype=np.int16)




# filename = '/home/spidey/Git/cinvestav-tesis/code/Unet/versions/train/dicom/dicom-001.dcm'
filename = '/mnt/Home/Proyectos/Maestria/14-tesis/dicoms/dicoms-v3/dicom/0001-L3-20190716-F-071Y-2mm-B.dcm'
dataset = pydicom.dcmread(filename)

print_general_info(dataset)

# plot the image using matplotlib
plt.imshow(dataset.pixel_array, cmap=plt.cm.bone)
plt.show()



# Normal option
# hu = pixel_value * slope + intercept
# hu_img = dataset.pixel_array*dataset.RescaleSlope+dataset.RescaleIntercept

# Direct option, only pydicom
# hu = apply_modality_lut(dataset.pixel_array, dataset)

# HU = CTNumber (7FE0,0010) * rescale slope (0028, 1053) + y intercept (0028, 1052). 
# These HU values have to be converted to pixel intensity f(x,y) using f(x,y) = ((HU-P1)*pow(2,i-1))/W, 
# where P1 is the window left border, i is the radiometric resolution and W is the window width. 


'''
Substance	HU
Air	−1000
Lung	−500
Fat	−100 to −50
Water	0
Blood	+30 to +70
Muscle	+10 to +40
Liver	+40 to +60
Bone	+700 (cancellous bone) to +3000 (cortical bone)
'''

# success option
hu_img = get_pixels_hu(dataset)
plot_hu_histogram(hu_img)

#img = hu_img
mask = get_mask_less_than(hu_img,-50)
plt.imshow(mask)








