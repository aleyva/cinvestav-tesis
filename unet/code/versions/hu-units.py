import pydicom
from pydicom.pixel_data_handlers.util import apply_modality_lut
import numpy as np
import matplotlib.pyplot as plt
import sys
import glob

# filename = '/home/spidey/Git/cinvestav-tesis/code/Unet/versions/train/dicom/dicom-001.dcm'
filename = '/mnt/Home/Proyectos/Maestria/14-tesis/dicoms/dicoms-v3/dicom/0001-L3-20190716-F-071Y-2mm-B.dcm'
dataset = pydicom.dcmread(filename)

# Normal mode:
print()
print("Filename.........:", filename)
print("Storage type.....:", dataset.SOPClassUID)
print()

pat_name = dataset.PatientName
display_name = pat_name.family_name + ", " + pat_name.given_name
print("Patient's name...:", display_name)
print("Patient id.......:", dataset.PatientID)
print("Modality.........:", dataset.Modality)
print("Study Date.......:", dataset.StudyDate)

if 'PixelData' in dataset:
    rows = int(dataset.Rows)
    cols = int(dataset.Columns)
    print("Image size.......: {rows:d} x {cols:d}, {size:d} bytes".format(
        rows=rows, cols=cols, size=len(dataset.PixelData)))
    if 'PixelSpacing' in dataset:
        print("Pixel spacing....:", dataset.PixelSpacing)

# use .get() if not sure the item exists, and want a default value if missing
print("Slice location...:", dataset.get('SliceLocation', "(missing)"))


print('slice shape..........: ' + str(dataset.pixel_array.shape) )
print('slice data type......: ' + str(dataset.pixel_array.dtype) )
print('min and max in array.: ' + str(np.amin(dataset.pixel_array)) + ' - ' +  str(np.amax(dataset.pixel_array)))

# plot the image using matplotlib
plt.imshow(dataset.pixel_array, cmap=plt.cm.bone)
plt.show()

'''
# plot the image using matplotlib
plt.imshow(dataset.pixel_array)
plt.show()
'''

# hu = pixel_value * slope + intercept
 # HU = CTNumber (7FE0,0010) * rescale slope (0028, 1053) + y intercept (0028, 1052). 
 # These HU values have to be converted to pixel intensity f(x,y) using f(x,y) = ((HU-P1)*pow(2,i-1))/W, 
 # where P1 is the window left border, i is the radiometric resolution and W is the window width. 


cols = dataset.Columns
rows = dataset.Rows

center_col = round(cols/2) - 1
center_row = round(rows/2) - 1


#hu_img = np.zeros( (cols, rows, 1), dtype=np.uint16)
hu = apply_modality_lut(dataset.pixel_array, dataset)
hu_img = dataset.pixel_array*dataset.RescaleSlope+dataset.RescaleIntercept



hu_img2 = hu_img.astype(np.uint16)



hu_img3 = window_ct(dataset, cols, dataset.pixel_array[center_col,center_row], 10, 40)



hu_img4 = get_pixels_hu(dataset)
plt.imshow(get_range(hu_img4,[-100, 50]),  cmap='gray')









'''
data = data.astype(np.float64) / info.max # normalize the data to 0 - 1
data = 255 * data # Now scale by 255
img = data.astype(np.uint8)
'''




np.zeros()
# Image1 = uint16(Image1); % This is very important, if you don't declare this, can make you troubles

plt.imshow(hu < 0,  cmap='gray', vmin=0, vmax=255)
plt.show()






def get_range(img, range):
    img[img < range[0]] = 0
    img[img > range[1]] = 0
    return img



#otro metodo sin usar pydicom
def get_pixels_hu(dataset):
    # image = np.stack([s.pixel_array for s in scans])
    image = dataset.pixel_array
    
    # Convert to int16 (from sometimes int16), 
    # should be possible as values should always be low enough (<32k)
    image = image.astype(np.int16)

    # Set outside-of-scan pixels to 1
    # The intercept is usually -1024, so air is approximately 0
    image[image == -2000] = 0
    
    # Convert to Hounsfield units (HU)
    intercept = dataset.RescaleIntercept
    slope = dataset.RescaleSlope
    
    if slope != 1:
        image = slope * image.astype(np.float64)
        image = image.astype(np.int16)
        
    image += np.int16(intercept)
    
    return np.array(image, dtype=np.int16)


'''
Substance	HU
Air	−1000
Lung	−500
Fat	−100 to −50
Water	0
Blood	+30 to +70
Muscle	+10 to +40
Liver	+40 to +60
Bone	+700 (cancellous bone) to +3000 (cortical bone)
'''


def window_ct(dcm, w, c, ymin, ymax):
    """Windows a CT slice.
    http://dicom.nema.org/medical/dicom/current/output/chtml/part03/sect_C.11.2.html

    Args:
        dcm (pydicom.dataset.FileDataset):
        w: Window Width parameter.
        c: Window Center parameter.
        ymin: Minimum output value.
        ymax: Maximum output value.

    Returns:
        Windowed slice.
    """
    # convert to HU
    b = dcm.RescaleIntercept
    m = dcm.RescaleSlope
    x = m * dcm.pixel_array + b

    # windowing C.11.2.1.2.1 Default LINEAR Function
    #
    y = np.zeros_like(x)
    y[x <= (c - 0.5 - (w - 1) / 2)] = ymin
    y[x > (c - 0.5 + (w - 1) / 2)] = ymax
    y[(x > (c - 0.5 - (w - 1) / 2)) & (x <= (c - 0.5 + (w - 1) / 2))] = \
        ((x[(x > (c - 0.5 - (w - 1) / 2)) & (x <= (c - 0.5 + (w - 1) / 2))] - (c - 0.5)) / (w - 1) + 0.5) * (
                ymax - ymin) + ymin

    return y
