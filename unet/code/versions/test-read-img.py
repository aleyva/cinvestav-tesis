
import os 
import numpy as np
#from skimage.io import imread
from imageio import imread
import pydicom
import imgaug as ia
from imgaug import augmenters as iaa
from imgaug.augmentables.segmaps import SegmentationMapsOnImage
import matplotlib.pyplot as plt


def image_to_rgb_uint(img):
    
    img_ = np.zeros( ( np.array(img).shape[0], np.array(img).shape[1], 3 ), dtype=img.dtype )
    
    if(len(img.shape) == 3):
        img_ = img
    else:
        if(len(img.shape) == 2 ):        
            img_gray = img
        elif (img.shape[2] == 1):
            img_gray = np.squeeze(img)
        
        img_[:,:,0] = img_gray # same value in each channel
        img_[:,:,1] = img_gray
        img_[:,:,2] = img_gray
           
    #if(img.dtype != np.uint8):
        #img_ = imageio.core.image_as_uint(img_)
    
    print('***********************')
    print('IMG Shape:{} - datatype: {}'.format(img_.shape, img_.dtype))

    return img_
    

# la imagen debe tener tres canales y ser de tipo uint8
# la mascara debe ser de tipo 'SegmentationMapsOnImage'
# para crear un 'SegmentationMapsOnImage' se le debe pasar un array con 1 canal
# parece que no importa el tipo de dato

def imshow_overlay(img, mask, filename='example.png'):        
    
    #img and mask, must has 3 channels and in dtype uint8
    img = image_to_rgb_uint(img_grayjpg)    

    # create the segmap of mask    
    segmap = np.zeros( (512, 512, 1), dtype=np.int32)
    segmap[mask > 100, 0] = 2    
    segmap = SegmentationMapsOnImage(segmap, shape=img.shape)
    
    
    cells = []    
    cells.append(img)                                         # column 1
    cells.append(segmap.draw_on_image(img)[0])                # column 2
    cells.append(segmap.draw(size=segmap.shape[:2])[0])                # column 2
    
    grid_image = ia.draw_grid(cells, cols=3)        
    plt.imshow(grid_image)
    plt.show()
    
    

basepath = '/home/spidey/Git/cinvestav-tesis/code/unet/dataset/test'
filepath_dicom = os.path.join( basepath, '0001-dicom.dcm' )
filepath_grayjpg = os.path.join( basepath, '0001-gray.jpg' )
filepath_graypng = os.path.join( basepath, '0001-gray.png' )
filepath_maskjpg = os.path.join( basepath, '0001-mask.jpg' )
filepath_maskpng = os.path.join( basepath, '0001-mask.png' )
filepath_maskcolor = os.path.join( basepath, '0001-maskcolor.png' )

img_dicom = pydicom.dcmread( filepath_dicom ).pixel_array.astype(np.float32)
#img_grayjpg = imread( filepath_grayjpg ).astype(np.float32)
img_grayjpg = imread( filepath_grayjpg )
img_graypng = imread( filepath_graypng )
img_maskjpg = imread( filepath_maskjpg )
#img_maskjpg = imread( filepath_maskjpg ).astype(np.float32)
img_maskpng = imread( filepath_maskpng )
img_maskcolor = imread( filepath_maskcolor )

print('Shape dicom:{} - datatype: {}'.format(img_dicom.shape, img_dicom.dtype))
print('Shape grayjpg:{} - datatype: {}'.format(img_grayjpg.shape, img_grayjpg.dtype))
print('Shape graypng:{} - datatype: {}'.format(img_graypng.shape, img_graypng.dtype))
print('Shape maskjpg:{} - datatype: {}'.format(img_maskjpg.shape, img_maskjpg.dtype))
print('Shape maskpng:{} - datatype: {}'.format(img_maskpng.shape, img_maskpng.dtype))
print('Shape maskcolor:{} - datatype: {}'.format(img_maskcolor.shape, img_maskcolor.dtype))

'''        
output
Shape dicom:(512, 512) - datatype: float32
Shape grayjpg:(512, 512) - datatype: uint8
Shape graypng:(512, 512) - datatype: uint8
Shape maskjpg:(512, 512) - datatype: uint8
Shape maskpng:(512, 511) - datatype: uint8
Shape maskcolor:(512, 511, 4) - datatype: uint8
'''

    
#imshow_overlay(img_grayjpg, img_maskjpg)


seq = iaa.Sequential([
    iaa.Crop(px=(0, 16)), # crop images from each side by 0 to 16px (randomly chosen)
    iaa.Fliplr(0.5), # horizontally flip 50% of the images
    iaa.GaussianBlur(sigma=(0, 3.0)) # blur images with a sigma of 0 to 3.0
])

#ex = img_grayjpg.astype(np.uint8)


# create the mask
segmap = np.where(img_maskjpg > 100, 2, 0)   
segmap = segmap.astype(np.uint8)
segmap = SegmentationMapsOnImage(segmap, shape=img_dicom.shape)


  
# Perform image and segmap augmentation
image_aug, segmap_aug = seq(image=img_dicom, segmentation_maps=segmap)
