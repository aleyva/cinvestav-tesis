#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 17 00:49:04 2020

@author: spidey
"""

import sys
import os
import pydicom
import numpy as np
import matplotlib.pyplot as plt
from skimage.io import imread


def print_general_info(dataset):
    # Normal mode:
    print()    
    print("Storage type.....:", dataset.SOPClassUID)
    print()
    
    pat_name = dataset.PatientName
    display_name = pat_name.family_name + ", " + pat_name.given_name
    print("Patient's name...:", display_name)
    print("Patient id.......:", dataset.PatientID)
    print("Modality.........:", dataset.Modality)
    print("Study Date.......:", dataset.StudyDate)
    
    if 'PixelData' in dataset:
        rows = int(dataset.Rows)
        cols = int(dataset.Columns)
        print("Image size.......: {rows:d} x {cols:d}, {size:d} bytes".format(
            rows=rows, cols=cols, size=len(dataset.PixelData)))
        if 'PixelSpacing' in dataset:
            print("Pixel spacing....:", dataset.PixelSpacing)
    
    # use .get() if not sure the item exists, and want a default value if missing
    print("Slice location...:", dataset.get('SliceLocation', "(missing)"))
    
    print('slice shape..........: ' + str(dataset.pixel_array.shape) )
    print('slice data type......: ' + str(dataset.pixel_array.dtype) )
    print('min and max in array.: ' + str(np.amin(dataset.pixel_array)) + ' - ' +  str(np.amax(dataset.pixel_array)))



def filter_dicom(dicom, bound):
    new_d = dicom    
    new_d[new_d<bound[0]] = 0;
    new_d[new_d>bound[1]] = 0;    
    return new_d - bound[0]; 


def get_dicom_pixel_spacing(dicom):
    print( str(dicom.PixelSpacing[0]) + ' / ' + str(dicom.PixelSpacing[1])  )
    return dicom.PixelSpacing[0] * dicom.PixelSpacing[1]    


'''
def overlay_mask(gray, mask, img_height, img_width):    
    # convert gray image from np.shape (img_height, img_width, 1) to RGB image
    ovl_mask = np.zeros([img_height, img_width, 3],dtype=np.uint8)
    mask[:,:,1] = mask*255    
    mask = Image.fromarray(mask, 'RGB')
    gray = np.squeeze(test_imgs[index])
    gray = Image.fromarray( cv2.merge([gray,gray,gray])  , 'RGB')    
    overlay = Image.blend(gray, mask, 0.3)

    # save the predicted mask and the overlay mask in dicom gray image representation
    filename_mask = test_mask_path + '{0:03d}'.format(index) + ext            
    mask.save(filename_mask)

    filename_overlay = test_mask_path + 'over_{0:03d}'.format(index) + ext            
    overlay.save(filename_overlay)
'''

def plot_hu_histogram(img):
    plt.hist(img.flatten(), bins=50, color='c')
    plt.xlabel("Hounsfield Units (HU)")
    plt.ylabel("Frequency")
    plt.show()

#otro metodo sin usar pydicom
def get_pixels_hu(dataset):
    # image = np.stack([s.pixel_array for s in scans])
    image = dataset.pixel_array
    
    # Convert to int16 (from sometimes int16), 
    # should be possible as values should always be low enough (<32k)
    image = image.astype(np.int16)

    # Set outside-of-scan pixels to 1
    # The intercept is usually -1024, so air is approximately 0
    image[image == -2000] = 0
    
    # Convert to Hounsfield units (HU)
    intercept = dataset.RescaleIntercept
    slope = dataset.RescaleSlope
    
    if slope != 1:
        image = slope * image.astype(np.float64)
        image = image.astype(np.int16)
        
    image += np.int16(intercept)
    
    return np.array(image, dtype=np.int8)



img_height = 512
img_width = 512


base_path = '/mnt/Home/Proyectos/Maestria/14-tesis/dicoms/dicoms-v3/measure-test/'

if(0):
    dicom_file = os.path.join(base_path,'0009-L3-20190702-M-051Y-2mm-B30f.dcm')
    mask_file = os.path.join(base_path,'0009-mask-20190702-M-051Y-2mm-B30f.png')
    gray_file = os.path.join(base_path,'0004-gray-20190715-F-072Y-1mm-B20f.png')
if(0):    
    dicom_file = os.path.join(base_path,'0007-L3-20190722-F-059Y-1mm-B20f.dcm')
    mask_file = os.path.join(base_path,'0007-mask-20190722-F-059Y-1mm-B20f.png')
if(1):    
    dicom_file = os.path.join(base_path,'0016-L3-20190703-F-070Y-2mm-B30f.dcm')
    mask_file = os.path.join(base_path,'0016-mask-20190703-F-070Y-2mm-B30f.png')
        

#reading dicom file        
dicom = pydicom.dcmread( dicom_file )
# print_general_info(dicom)
hu_dicom = get_pixels_hu(dicom)

# Normal option
# hu = pixel_value * slope + intercept
# hu_img = dataset.pixel_array*dataset.RescaleSlope+dataset.RescaleIntercept

# Direct option, only pydicom
# hu = apply_modality_lut(dataset.pixel_array, dataset)

# HU = CTNumber (7FE0,0010) * rescale slope (0028, 1053) + y intercept (0028, 1052). 
# These HU values have to be converted to pixel intensity f(x,y) using f(x,y) = ((HU-P1)*pow(2,i-1))/W, 
# where P1 is the window left border, i is the radiometric resolution and W is the window width. 

'''
Substance	HU
Air	−1000
Lung	−500
Fat	−100 to −50
Water	0
Blood	+30 to +70
Muscle	+10 to +40
Liver	+40 to +60
Bone	+700 (cancellous bone) to +3000 (cortical bone)
'''
'''
gray = hu_dicom
gray[gray < 50] = 0
gray[gray > 150] = 0
plt.imshow(gray, cmap='gray')
plt.plot()
'''

#gray_muscle = filter_dicom(dicom.pixel_array,[950,1120])
gray_muscle = dicom.pixel_array

#reading mask
#mask = imread( mask_file )
mask = imread(  mask_file  )[:,:,1]
#mask to bool
mask = mask.astype(np.bool)
mask = mask.astype(np.uint8)


#overlay = mask_overlay(dicom)

not_vat = hu_dicom>0
lean_muscle = mask & not_vat

# measure the cm2
'''
dinfo = dicominfo('TheFileName.dcm');
spacing = dinfo.PixelSpacing;
per_pixel_area = spacing(1) * spacing(2);
num_white_pixels = nnz(binarized_img);
total_white_area = num_white_pixels * per_pixel_area;
'''
per_pixel_area = get_dicom_pixel_spacing(dicom)
cm = np.count_nonzero(lean_muscle) * per_pixel_area
cm2 = cm**(1/2) 
print ('cm2: ' + str(cm2))



plot_hu_histogram(hu_dicom)

plt.figure(1)

plt.subplot(221)
plt.title('dicom in grayscale', fontsize=8)
plt.imshow(dicom.pixel_array, cmap=plt.cm.bone)
plt.gca().axes.get_yaxis().set_visible(False)
plt.gca().axes.get_xaxis().set_visible(False)

plt.subplot(222)
plt.title('dicom in grayscale', fontsize=8)
plt.imshow(dicom.pixel_array, cmap=plt.cm.bone)
plt.gca().axes.get_yaxis().set_visible(False)
plt.gca().axes.get_xaxis().set_visible(False)

# plot the image using matplotlib
#plt.imshow(dicom.pixel_array, cmap=plt.cm.bone)
plt.subplot(223)
plt.title('segmentation mask', fontsize=8)
plt.imshow(mask, cmap='gray')
plt.gca().axes.get_yaxis().set_visible(False)
plt.gca().axes.get_xaxis().set_visible(False)

plt.subplot(224)
plt.title('lean muscle cm²: ' + '{0:.2f}'.format(cm2), fontsize=8)
plt.imshow(lean_muscle, cmap='gray')
plt.gca().axes.get_yaxis().set_visible(False)
plt.gca().axes.get_xaxis().set_visible(False)

plt.show()

