'''
conda install numpy scikit-image spyder
conda install -c conda-forge python-lmdb
conda install -c anaconda protobuf
conda install -c anaconda tensorflow
'''
import os 
import numpy as np
import random
from tqdm import tqdm
from skimage.io import imread
from skimage.transform import resize 
import pydicom


# read two folders, "gray" and "mask", each folder 
def read_dataset(img_path, mask_path, img_width, img_height, number_channels, train_fraction):
    
    print("images path: " + img_path)
    print("masks path: " + mask_path)
    
    # count the number of files
    len_imgs = len(os.listdir(img_path))
    print(str(len_imgs) + "," + str(len(os.listdir(mask_path))) )
    assert len_imgs == len(os.listdir(mask_path)), 'El número de imagenes y de mascaras deben coincidir'
    assert len_imgs > 2, 'no existen suficientes imágenes para entrenar'

    #Create array with zeros of the same size as the resize image size.
    imgs = np.zeros((len_imgs, img_height, img_width, number_channels), dtype=np.uint8)
    masks = np.zeros((len_imgs, img_height, img_width, 1), dtype=np.bool)

    # get the extension of the first file in the directory
    extension = os.path.splitext( os.listdir(img_path)[0] )[1]

    print('\nReading and resizing DICOM images ... ')
    if( extension.lower() == '.dcm'):        
        for index, file in tqdm(enumerate(sorted(os.listdir(img_path)))) :                      
            imgs[index] = read_img( os.path.join(mask_path,file), 'dcm', img_width, img_height)    
    else:
        for index, file in tqdm(enumerate(sorted(os.listdir(img_path)))) :                      
            imgs[index] = read_img( os.path.join(mask_path,file), 'gray', img_width, img_height)    

    print('\nReading and resizing segmentation masks ... ')
    for index, file in tqdm(enumerate(sorted(os.listdir(train_img_path)))) :              
        # mask[mask < 40] = 0   #clean the mask
        masks[index] = read_img( os.path.join(mask_path,file), 'mask_color', img_width, img_height)    
   
        
    # suffle img and mask without breaking the correspondance
    #train, test = shuffle(train, test)    
    shuffle(imgs,masks)    
    
    # divide datasets into test and train
    idx = int(train_fraction * len_imgs)
    train_imgs = imgs[0:idx]
    train_masks = masks[0:idx]
    test_imgs = imgs[idx:]
    test_masks = masks[idx:]
    
    return train_imgs, train_masks, test_imgs, test_masks


def shuffle(a,b):
    assert len(a) == len(b)
    start_state = random.getstate()
    random.shuffle(a)
    random.setstate(start_state)
    random.shuffle(b)


def read_img(file_path, file_type, img_height, img_width)
    if( file_type == 'dcm'):
        img = pydicom.dcmread( file_path ).pixel_array
    if( file_type == 'gray'):
        img = imread( file_path )
    if( file_type == 'rgb'):
        img = imread( file_path )
    if( file_type == 'mask_gray'):
        img = imread(  file_path  )
    if( file_type == 'mask_color'):
        img = imread(  file_path  )[:,:,1]    

    # resize and expand_dims
    img = resize(img, (img_height, img_width), mode='constant',  preserve_range=True)
    img = np.expand_dims( img, axis=-1)            

    return img
    
    