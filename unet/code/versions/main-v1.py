import tensorflow as tf 
import os 
import random 
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
from skimage.io import imread, imshow, imsave
from skimage.transform import resize 

'''
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
config.gpu_options.per_process_gpu_memory_fraction = 0.5
session = tf.InteractiveSession(config=config)
'''


IMG_WIDTH = 512 
IMG_HEIGHT = 512
IMG_CHANNELS = 1

BASE_PATH = '/home/spidey/Git/cinvestav-tesis/code/unet'
             
TRAIN_DIC_PATH = BASE_PATH + '/dataset/v2-train/gray' 
TRAIN_MASK_PATH = BASE_PATH + '/dataset/v2-train/mask' 
TEST_DIC_PATH = BASE_PATH + '/dataset/v2-test/gray' 
TEST_MASK_PATH = BASE_PATH + '/dataset/v2-test/mask' 

seed = 30
np.random.seed = seed 
np.random.seed = seed


# Get train and test IDs
print(TRAIN_DIC_PATH)
train_ids = next(os.walk(TRAIN_DIC_PATH))[2]
test_ids = next(os.walk(TEST_DIC_PATH))[2]


#Create array with zeros of the same size as the resize image size.
X_train = np.zeros((len(train_ids), IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS), dtype=np.uint16) 
Y_train = np.zeros((len(train_ids), IMG_HEIGHT, IMG_WIDTH, 1), dtype=np.bool)
X_test = np.zeros((len(test_ids), IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS), dtype=np.uint16)


if 0 :
    mask = imread( '/home/spidey/Git/cinvestav-tesis/code/Unet/train/mask/image002-mask.jpg.png'  )[:,:,1]
    mask = np.expand_dims(resize(mask, (IMG_HEIGHT, IMG_WIDTH), mode='constant',  preserve_range=True), axis=-1)
    mask[mask < 40] = 0
    Y_train[0] = mask
    imshow(np.squeeze(Y_train[0]))


print('\nResizing training DICOM images ... ')
for index, file in tqdm(enumerate(sorted(os.listdir(TRAIN_DIC_PATH)))) :      
    #print(os.path.join(TRAIN_DIC_PATH,file))
    img = imread( os.path.join(TRAIN_DIC_PATH,file) )
    img = np.expand_dims(resize(img, (IMG_HEIGHT, IMG_WIDTH), mode='constant',  preserve_range=True), axis=-1)    
    img = resize(img, (IMG_HEIGHT, IMG_WIDTH), mode='constant', preserve_range=True)
    X_train[index] = img #Fill empty X_train with values from img
    

print('\nResizing training masks ... ')    
for index, file in tqdm(enumerate(sorted(os.listdir(TRAIN_MASK_PATH)))) :      
    #print(os.path.join(TRAIN_DIC_PATH,file))
    mask = imread(  os.path.join(TRAIN_MASK_PATH,file)  )[:,:,1]
    mask = np.expand_dims(resize(mask, (IMG_HEIGHT, IMG_WIDTH), mode='constant',  preserve_range=True), axis=-1)
    mask[mask < 40] = 0   #clean the mask
    Y_train[index] = mask
    

print('\nResizing test images ... ')
for index, file in tqdm(enumerate(sorted(os.listdir(TEST_DIC_PATH)))) :     
    img = imread( os.path.join(TEST_DIC_PATH,file) )
    img = np.expand_dims(resize(img, (IMG_HEIGHT, IMG_WIDTH), mode='constant',  preserve_range=True), axis=-1)    
    img = resize(img, (IMG_HEIGHT, IMG_WIDTH), mode='constant', preserve_range=True)
    X_test[index] = img #Fill empty X_train with values from img
    

# plot the image using matplotlib
index = random.randint(0, len(X_train)-1)
print('Dicom')
plt.imshow(np.squeeze(X_train[index]))
plt.show()
print('mask')
plt.imshow(np.squeeze(Y_train[index]))
plt.show()

   
    
# The U-net

#convert layers integers to float
inputs = tf.keras.layers.Input((IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS)) 
s = tf.keras.layers.Lambda(lambda x: x / 255)(inputs)

# Contracting Path
c1 = tf.keras.layers.Conv2D(16, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(s)
c1 = tf.keras.layers.Dropout(0.1)(c1)
c1 = tf.keras.layers.Conv2D(16, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c1)
p1 = tf.keras.layers.MaxPooling2D((2, 2))(c1)

c2 = tf.keras.layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(p1)
c2 = tf.keras.layers.Dropout(0.1)(c2)
c2 = tf.keras.layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c2)
p2 = tf.keras.layers.MaxPooling2D((2, 2))(c2)

c3 = tf.keras.layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(p2)
c3 = tf.keras.layers.Dropout(0.2)(c3)
c3 = tf.keras.layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c3)
p3 = tf.keras.layers.MaxPooling2D((2, 2))(c3)

c4 = tf.keras.layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(p3)
c4 = tf.keras.layers.Dropout(0.2)(c4)
c4 = tf.keras.layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c4)
p4 = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(c4)

c5 = tf.keras.layers.Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(p4)
c5 = tf.keras.layers.Dropout(0.3)(c5)
c5 = tf.keras.layers.Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c5)

#Expansive path
u6 = tf.keras.layers.Conv2DTranspose(128, (2, 2), strides=(2, 2), padding='same')(c5) 
u6 = tf.keras.layers.concatenate([u6, c4])
c6 = tf.keras.layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(u6)
c6 = tf.keras.layers.Dropout(0.2)(c6)
c6 = tf.keras.layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c6)

u7 = tf.keras.layers.Conv2DTranspose(64, (2, 2), strides=(2, 2), padding='same')(c6) 
u7 = tf.keras.layers.concatenate([u7, c3])
c7 = tf.keras.layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(u7)
c7 = tf.keras.layers.Dropout(0.2)(c7)
c7 = tf.keras.layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c7)

u8 = tf.keras.layers.Conv2DTranspose(32, (2, 2), strides=(2, 2), padding='same')(c7) 
u8 = tf.keras.layers.concatenate([u8, c2])
c8 = tf.keras.layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(u8)
c8 = tf.keras.layers.Dropout(0.1)(c8)
c8 = tf.keras.layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c8)

u9 = tf.keras.layers.Conv2DTranspose(16, (2, 2), strides=(2, 2), padding='same')(c8) 
u9 = tf.keras.layers.concatenate([u9, c1], axis=3)
c9 = tf.keras.layers.Conv2D(16, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(u9)
c9 = tf.keras.layers.Dropout(0.1)(c9)
c9 = tf.keras.layers.Conv2D(16, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c9)


outputs = tf.keras.layers.Conv2D(1, (1, 1), activation='sigmoid')(c9)

model = tf.keras.Model(inputs=[inputs], outputs=[outputs])
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy']) 
# model.summary()



##############################################################################
#Model checkpoint - save model after every epoch.
#Save best only

checkpointer = tf.keras.callbacks.ModelCheckpoint(BASE_PATH + '/model/model_for_l3.h5', verbose=1, save_best_only=True)

#Early stopping 
#Tensorboard callback
callbacks = [
    tf.keras.callbacks.EarlyStopping(patience=1, monitor='val_loss'),
    tf.keras.callbacks.TensorBoard(log_dir='logs'),
    checkpointer
]

results = model.fit(X_train, Y_train, validation_split=0.1, batch_size=32, epochs=100, callbacks=callbacks)


##############################################################################

## check random training samples 
#No need to Load model if it is already in memory...
#idx = random.randint(0, len(X_train))
# preds_train = model.predict(X_train[:int(X_train.shape[0]*0.9)], verbose=1) 
# preds_val = model.predict(X_train[int(X_train.shape[0]*0.9):], verbose=1) 
preds_test = model.predict(X_test, verbose=1)


#Each pixel is given a value between 0 and 1. He set a threshold .5 to binarizel 
# Apply threshold to binarize the image

#preds_train_t = (preds_train > 0.5).astype(np.uint8) 
#preds_val_t = (preds_val > 0.5).astype(np.uint8) 
preds_test_t = (preds_test > 0.45).astype(np.uint8)


# check random training samples 
ix = random.randint(0, len(preds_test_t)) 
imshow(np.squeeze(preds_test_t[ix]),cmap='gray')
plt.show()


#Guardar mascaras
for index, mask in tqdm(enumerate(preds_test_t)) : 
    filename = TEST_MASK_PATH + '{0:03d}'.format(index) + '.jpg'            
    imsave(filename, np.squeeze(mask)*255)
    #imshow(np.squeeze(mask)*255)
    #plt.show()
