#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 18 19:45:20 2020

@author: spidey
"""




import sys
if sys.version_info[0] < 3:
    raise Exception('Python3 required')
import os
import numpy as np
#import scipy
#import scipy.ndimage
#import scipy.signal
import skimage.io
import skimage.transform
import matplotlib.pyplot as plt


def apply_affine_transformation(I, orientation, reflect_x, reflect_y, jitter_x, jitter_y, scale_x, scale_y):

    if orientation is not 0:
        I = skimage.transform.rotate(I, orientation, preserve_range=True, mode='reflect')

    tform = skimage.transform.AffineTransform(translation=(jitter_x, jitter_y),
                                              scale=(scale_x, scale_y))
    I = skimage.transform.warp(I, tform._inv_matrix, mode='reflect', preserve_range=True)

    if reflect_x:
        I = np.fliplr(I)
    if reflect_y:
        I = np.flipud(I)

    return I


def plot(img, mask, img_t, mask_t, name_1, name_2, name_3, name_4):
        
    plt.subplots_adjust(left=0, bottom=.05, right=1, top=.90, wspace=.1, hspace=.15)
    
    f_size = 10
    
    plt.subplot(221)
    plt.title(name_1, fontsize=f_size)
    plt.imshow(img)
    plt.gca().axes.get_yaxis().set_visible(False)
    plt.gca().axes.get_xaxis().set_visible(False)
    
    plt.subplot(222)
    plt.title(name_2, fontsize=f_size)
    plt.imshow(img_t)
    plt.gca().axes.get_yaxis().set_visible(False)
    plt.gca().axes.get_xaxis().set_visible(False)
    
    # plot the image using matplotlib
    #plt.imshow(dicom.pixel_array, cmap=plt.cm.bone)
    plt.subplot(223)
    plt.title(name_3, fontsize=f_size)
    plt.imshow(mask)
    plt.gca().axes.get_yaxis().set_visible(False)
    plt.gca().axes.get_xaxis().set_visible(False)
    
    plt.subplot(224)
    plt.title(name_4, fontsize=f_size)
    plt.imshow(mask_t)
    plt.gca().axes.get_yaxis().set_visible(False)
    plt.gca().axes.get_xaxis().set_visible(False)
    
    plt.show()
    
def transform(img, mask, orientation=0, reflect_x=False, reflect_y=False, jitter_x=0, jitter_y=0, scale_x=1, scale_y = 1,
              name_1='dicom ground truth', name_2='transform', name_3='mask ground truth', name_4='transform'):
    
    # apply the affine transformation
    img_t = apply_affine_transformation(img, orientation, reflect_x, reflect_y, jitter_x, jitter_y, scale_x, scale_y)
    if mask is not None:
        mask_t = apply_affine_transformation(mask, orientation, reflect_x, reflect_y, jitter_x, jitter_y, scale_x, scale_y)
        
    plot(img, mask, img_t, mask_t, name_1, name_2, name_3, name_4)






base_path = '/mnt/Home/Proyectos/Maestria/14-tesis/dicoms/dicoms-v3/measure-test/'
if(1):
    dicom_file = os.path.join(base_path,'0009-L3-20190702-M-051Y-2mm-B30f.dcm')
    mask_file = os.path.join(base_path,'0007-mask-20190722-F-059Y-1mm-B20f.png')
    gray_file = os.path.join(base_path,'0007-sgray-20190722-F-059Y-1mm-B20f.png')
if(0):    
    dicom_file = os.path.join(base_path,'0007-L3-20190722-F-059Y-1mm-B20f.dcm')
    mask_file = os.path.join(base_path,'0007-mask-20190722-F-059Y-1mm-B20f.png')
if(0):    
    dicom_file = os.path.join(base_path,'0016-L3-20190703-F-070Y-2mm-B30f.dcm')
    mask_file = os.path.join(base_path,'0016-mask-20190703-F-070Y-2mm-B30f.png')
    

orientation = 0
reflect_x = False
reflect_y = False
jitter_x = 0
jitter_y = 0
scale_x = 1.2
scale_y = 1.2
name_1 = 'dicom ground truth'
name_2 = 'zoom 20 %'
name_3 = 'mask ground truth'
name_4 = 'zoom 20 %'



img = skimage.io.imread(  gray_file  )
mask = skimage.io.imread(  mask_file  )[:,:,1]
        

transform(img, mask, orientation=0, reflect_x=False, reflect_y=False, jitter_x=0, jitter_y=0, scale_x=1.2, scale_y = 1.2,
              name_1='dicom ground truth', name_2='zoom 20%', name_3='mask ground truth', name_4='zoom 20%')

transform(img, mask, orientation=0, reflect_x=False, reflect_y=False, jitter_x=0, jitter_y=0, scale_x=.8, scale_y = .9,
              name_1='dicom ground truth', name_2='zoom -20%', name_3='mask ground truth', name_4='zoom -20%')
    
transform(img, mask, orientation=10, reflect_x=False, reflect_y=False, jitter_x=0, jitter_y=0, scale_x=1, scale_y = 1,
              name_1='dicom ground truth', name_2='rotate 10°', name_3='mask ground truth', name_4='rotate 10°')


transform(img, mask, orientation=0, reflect_x=False, reflect_y=False, jitter_x=-19, jitter_y=-19, scale_x=1, scale_y = 1,
              name_1='dicom ground truth', name_2='translation -19/-19', name_3='mask ground truth', name_4='translation -19/-19')


'''
para el proyecto solo se usaron

rotación +/- 10°
traslación en 'x' y 'y'
reescalamiento +/- 10%
reflexión en 'y'
'''