import tensorflow as tf 
import random 
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
'''
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
config.gpu_options.per_process_gpu_memory_fraction = 0.7
session = tf.InteractiveSession(config=config)
'''

IMG_WIDTH = 512 
IMG_HEIGHT = 512
IMG_CHANNELS = 1

BASE_PATH = '/home/spidey/Git/cinvestav-tesis/code/unet'
TEST_MASK_PATH = BASE_PATH + '/dataset/v2-test/mask/' 

seed = 30
np.random.seed = seed 
np.random.seed = seed

max_value = 1120
min_value = 900

#load np files to array, dtype is uint16
X_train = np.load(BASE_PATH + '/dataset/x_train.npy')
Y_train = np.load(BASE_PATH + '/dataset/y_train.npy')
X_test = np.load(BASE_PATH + '/dataset/x_test.npy')

# code to clip the array
X_train = np.clip(X_train, min_value, max_value) 
X_test = np.clip(X_test, min_value, max_value) 

# plot the image using matplotlib
index = random.randint(0, len(X_train)-1)
print('Dicom')
plt.imshow(np.squeeze(X_train[index]))
plt.show()
print('mask')
plt.imshow(np.squeeze(Y_train[index]))
plt.show()
 

###############
# 250 ?????
#################
    
    
# The U-net

#convert layers integers to float
inputs = tf.keras.layers.Input((IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS)) 
s = tf.keras.layers.Lambda(lambda x: x / 255)(inputs)

# Contracting Path
c1 = tf.keras.layers.Conv2D(16, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(s)
c1 = tf.keras.layers.Dropout(0.1)(c1)
c1 = tf.keras.layers.Conv2D(16, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c1)
p1 = tf.keras.layers.MaxPooling2D((2, 2))(c1)

c2 = tf.keras.layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(p1)
c2 = tf.keras.layers.Dropout(0.1)(c2)
c2 = tf.keras.layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c2)
p2 = tf.keras.layers.MaxPooling2D((2, 2))(c2)

c3 = tf.keras.layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(p2)
c3 = tf.keras.layers.Dropout(0.2)(c3)
c3 = tf.keras.layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c3)
p3 = tf.keras.layers.MaxPooling2D((2, 2))(c3)

c4 = tf.keras.layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(p3)
c4 = tf.keras.layers.Dropout(0.2)(c4)
c4 = tf.keras.layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c4)
p4 = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(c4)

c5 = tf.keras.layers.Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(p4)
c5 = tf.keras.layers.Dropout(0.3)(c5)
c5 = tf.keras.layers.Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c5)

#Expansive path
u6 = tf.keras.layers.Conv2DTranspose(128, (2, 2), strides=(2, 2), padding='same')(c5) 
u6 = tf.keras.layers.concatenate([u6, c4])
c6 = tf.keras.layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(u6)
c6 = tf.keras.layers.Dropout(0.2)(c6)
c6 = tf.keras.layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c6)

u7 = tf.keras.layers.Conv2DTranspose(64, (2, 2), strides=(2, 2), padding='same')(c6) 
u7 = tf.keras.layers.concatenate([u7, c3])
c7 = tf.keras.layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(u7)
c7 = tf.keras.layers.Dropout(0.2)(c7)
c7 = tf.keras.layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c7)

u8 = tf.keras.layers.Conv2DTranspose(32, (2, 2), strides=(2, 2), padding='same')(c7) 
u8 = tf.keras.layers.concatenate([u8, c2])
c8 = tf.keras.layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(u8)
c8 = tf.keras.layers.Dropout(0.1)(c8)
c8 = tf.keras.layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c8)

u9 = tf.keras.layers.Conv2DTranspose(16, (2, 2), strides=(2, 2), padding='same')(c8) 
u9 = tf.keras.layers.concatenate([u9, c1], axis=3)
c9 = tf.keras.layers.Conv2D(16, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(u9)
c9 = tf.keras.layers.Dropout(0.1)(c9)
c9 = tf.keras.layers.Conv2D(16, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c9)


outputs = tf.keras.layers.Conv2D(1, (1, 1), activation='sigmoid')(c9)

model = tf.keras.Model(inputs=[inputs], outputs=[outputs])
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy']) 
# model.summary()



##############################################################################
#Model checkpoint - save model after every epoch.
#Save best only

checkpointer = tf.keras.callbacks.ModelCheckpoint(BASE_PATH + '/model/model_for_l3.h5', verbose=1, save_best_only=True)

#Early stopping 
#Tensorboard callback
callbacks = [
    tf.keras.callbacks.EarlyStopping(patience=1, monitor='val_loss'),
    tf.keras.callbacks.TensorBoard(log_dir='logs'),
    checkpointer
]

results = model.fit(X_train, Y_train, validation_split=0.1, batch_size=1, epochs=100, callbacks=callbacks)


##############################################################################

## check random training samples 
#No need to Load model if it is already in memory...


preds_test = model.predict(X_test, verbose=1)

#Each pixel is given a value between 0 and 1. He set a threshold .5 to binarizel 
# Apply threshold to binarize the image
preds_test_t = (preds_test > 0.45).astype(np.uint8)

#plt.imshow(np.squeeze((preds_test[1] > 0.25).astype(np.uint8)))

# check random training samples 
ix = random.randint(0, len(preds_test_t)) 
plt.imshow(np.squeeze(preds_test_t[ix]),cmap='gray')
plt.show()



#Guardar mascaras
for index, mask in tqdm(enumerate(preds_test_t)) : 
    filename = TEST_MASK_PATH + '{0:03d}'.format(index) + '.jpg'        
    #imshow(np.squeeze(mask)*255)
    #plt.show()
    plt.imsave(filename, np.squeeze(mask)*255)
