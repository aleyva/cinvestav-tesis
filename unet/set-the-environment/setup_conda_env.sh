#!/bin/bash

# start up an interactive session
# srun -p debug -n 1 -t 60:00 --gres=gpu:1 --pty bash

#source /opt/anaconda3/etc/profile.d/conda.sh
conda create --name tf2-gpu python=3.6 -y
conda activate tf2-gpu

########################################################################
#-------------------------------------------
# best option to install tensfowflow GPU, even better than conda install -c anaconda tensorflow-gpu
conda install cudatoolkit=10.0 cudnn=7.6 cupti=10.0 blas pip scipy
pip install --upgrade pip
pip install tensorflow-gpu==2.0
conda install tensorboard -y
# or (but not recommended)
# conda install tensorflow-gpu -y
#-------------------------------------------
# common packages
conda install setuptools -y
conda install numpy -y
conda install pillow -y
conda install scikit-image -y
conda install scikit-learn -y       # for jaccard
conda install h5py -y               
conda install spyder -y             #or jupyter
# common packages from conda-forge channel
# conda config --add channels conda-forge
#conda install -c conda-forge imgaug            #parece que no ya no se usa
#conda install -c conda-forge mkl_random -y     #parece que no ya no se usa
conda install -c conda-forge tqdm -y
conda install -c conda-forge matplotlib -y
conda install -c conda-forge pydicom gdcm -y #for using pydicom
#-------------------------------------------
# install lmdb
conda install -c conda-forge python-lmdb -y
conda install -c anaconda protobuf -y
conda install -c anaconda absl-py
pip install imagecodecs
#-------------------------------------------
########################################################################

conda config --add channels conda-forge
conda install imgaug
pip install imagecorruptions

# or
# conda install -y -c conda-forge --file requirements.txt





