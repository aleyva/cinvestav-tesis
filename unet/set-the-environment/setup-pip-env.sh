
# Use Pip

# first change the path to the environmets folder like
cd ~/Environmets
python3 -m venv tf20
source ./tf/bin/activate

pip3 install --upgrade pip
pip3 install --upgrade -r requirements.txt

#pip3 install --upgrade pip
#pip3 install --upgrade -r requirements.txt



# tenia un problema con la libreria cublas, para solucionar hice esto:
# $ pip3 install --upgrade tensorflow-gpu==1.4
# $ python main-v2.py 
# $ pip3 install --upgrade tensorflow-gpu



###################################
### install pydicom with pip (not recomended, better use conda)
###################################
# $ pip3 install pydicom

# Installing CharPyLS
# CharPyLS is a Python interface to the CharLS C++ library and can decompress JPEG-LS images.
# $ pip3 install cython
# $ pip3 install git+https://github.com/Who8MyLunch/CharPyLS

# Installing GDCM
# sudo apt install python3-gdcm
# pip3 install --no-cache-dir git+https://github.com/pydicom/pydicom.git
