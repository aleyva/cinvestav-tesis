
<HTML>
<HEAD>
<style>
body {
  font-family: Arial, Helvetica, sans-serif;
  font-size: 16px;
}
</style>
<TITLE>Borrar estudios</TITLE>
</HEAD>
<BODY>

<?php
//EJECUTAR SCRIPT CON HASTA 150 PACIENTES

//importar clase para leer archivo DICOM
require_once('./read_dicom/class_dicom/class_dicom.php');


//Leer todos los archivos de la ruta $path (ponerl el final del path sin la diagonal invertida
$path    = 'G:\Tesis-estudios\Todos-los-estudios-unzip\2019-07-2';
$files = glob($path.'\*' , GLOB_ONLYDIR);

// path
//   |-- ($dir,$files) Estudios de diferentes pacientes
//			|-- ($sub_dir,$sub_files) Varios estudios del mismo paciente
//						|-- ($sub_sub_files) Archivos DICOM (slices)

//recorrer estudios (todos los pacientes)
foreach($files as $dir){	
	//leer los sub_folders
	echo $dir."<br>";
	$sub_files = glob($dir.'\*' , GLOB_ONLYDIR);	
	//recorrer cada sub_folder (los diferentes estudios de cada paciente)
	foreach($sub_files as $sub_dir){					
		
		$sub_dir_name = basename($sub_dir);
		
		//este es el que si funciona 1.2.840.113704.1
		//obtiene el nombre del sub_folder desde la posicion 0 a la 17			
		if( sub_string($sub_dir,0, 17) == "1.3.12.2.1107.5.8" || 
			sub_string($sub_dir,0, 16) == "1.2.840.113704.7"){
			// si el nombre del folder (estudio de paciente) inicia con "1.3.12.2.1107.5.8", se borra
			echo "&nbsp&nbsp|--&nbsp&nbsp<font color='red'>$sub_dir_name</font><br>";
			delete_directory($sub_dir);	
		}else{
				
			$sub_sub_files = glob( $sub_dir."\*" ); 		  		
			$num_sub_sub_files = count($sub_sub_files);
			
			$slice_thickness = get_thickness($sub_sub_files[0]);
			if(count($sub_sub_files) <= 40 || $slice_thickness > 2) {				
				//eliminar los estudios del paciente:
				//si tiene menos de 40 archivos 
				//si el tamaño de la rebanada del estudio es mayor a 2mm, si es así, borrar el estudio
				echo "&nbsp&nbsp|--&nbsp&nbsp<font color='red'>$sub_dir_name</font>&nbspthick-$slice_thickness<br>";
				delete_directory($sub_dir);				
			}else{
				echo "&nbsp&nbsp|--&nbsp&nbsp<font color='green'>$sub_dir_name</font><br>";
			}			
		}		
	}					
}

function delete_directory($dirname) {
    if (is_dir($dirname))
		$dir_handle = opendir($dirname);
	if (!$dir_handle)
		return false;
	while($file = readdir($dir_handle)) {
		if ($file != "." && $file != "..") {
			if (!is_dir($dirname."/".$file))
				unlink($dirname."/".$file);
			else
				delete_directory($dirname.'/'.$file);
           }
     }
     closedir($dir_handle);
     rmdir($dirname);
     return true;
}

function sub_string($sub_dir,$start,$end){
	return substr ( basename($sub_dir).PHP_EOL, $start, $end);		
}

function get_thickness($file_path){		
	if(!file_exists($file_path)) {
	  print "$file_path: does not exist<br>";
	  exit;
	}

	$d = new dicom_tag($file_path);	
	$d->load_tags();

	return $d->get_tag('0018', '0050');	
}

?>

</body>
