
<HTML>
<HEAD>
<style>
body {
  font-family: Arial, Helvetica, sans-serif;
  font-size: 16px;
}
</style>
<TITLE>Borrar estudios</TITLE>
</HEAD>
<BODY>

<?php
//EJECUTAR SCRIPT CON HASTA 150 PACIENTES

//importar clase para leer archivo DICOM
require_once('./read_dicom/class_dicom/class_dicom.php');


//Leer todos los archivos de la ruta $path (ponerl el final del path sin la diagonal invertida
$path    = 'G:\Tesis-estudios\Todos-los-estudios-unzip\2020-01';
$files = glob($path.'\*' , GLOB_ONLYDIR);

// path
//   |-- ($dir,$files) Estudios de diferentes pacientes
//			|-- ($sub_dir,$sub_files) Varios estudios del mismo paciente
//						|-- ($sub_sub_files) Archivos DICOM (slices)


foreach($files as $dir){	
	//leer los sub_folders
	//echo $dir."<br>";
	$sub_files = glob($dir.'\*' , GLOB_ONLYDIR);	
	//recorrer cada sub_folder (los diferentes estudios de cada paciente)
	foreach($sub_files as $sub_dir){					
		
		$sub_sub_files = glob( $sub_dir."\*" ); 		  		
		$num_sub_sub_files = count($sub_sub_files);
				
		//$file_name = pathinfo($sub_sub_files[0]);				
		// Folder del grupo de estudio
		// Folder del estudio
		// Nombre Archivo
		// meta_data
		$file_info = basename($dir).'&oplus;'.basename($sub_dir);
		$meta_data = $file_info.'&oplus;'.get_metadata($sub_sub_files[0]).'<br>';		
		
		echo $meta_data;
	}					
}


function get_metadata($file_path){		
	if(!file_exists($file_path)) {
	  print "$file_path: does not exist<br>";
	  exit;
	}

	$d = new dicom_tag($file_path);	
	$d->load_tags();
	
	$meta_data = "";
// Datos de paciente
	//$meta_data .= $d->get_tag('0002', '0003').'&oplus;';	// Media storage SOP instance
	$meta_data .= $d->get_tag('0010', '0010').'&oplus;';	// Patience name
	$meta_data .= $d->get_tag('0010', '0020').'&oplus;';	// Patience ID
	$meta_data .= $d->get_tag('0010', '0030').'&oplus;';	// Patience BirthDay
	$meta_data .= $d->get_tag('0010', '0040').'&oplus;';	// Patience sex
	$meta_data .= $d->get_tag('0010', '1010').'&oplus;';	// Patience Age
// Datos de tiempo
	$meta_data .= $d->get_tag('0008', '0022').'&oplus;';	// AcquisitionDate
	$meta_data .= $d->get_tag('0008', '0032').'&oplus;';	// AcquisitionTime
	//$meta_data .= $d->get_tag('0008', '002A').'&oplus;';	// AcquisitionDateTime
// Datos de caracteristicas
	$meta_data .= $d->get_tag('0018', '0015').'&oplus;';	// BodyPartExamined
	$meta_data .= $d->get_tag('0018', '1030').'&oplus;';	// ProtocolName
	$meta_data .= $d->get_tag('0018', '0050').'&oplus;';	// SliceThickness
	$meta_data .= $d->get_tag('0018', '0088').'&oplus;';	// SpaceBetweenSlices
	$meta_data .= $d->get_tag('0018', '1210').'&oplus;';	// ConvolutionKernel
	//$meta_data .= $d->get_tag('0028', '0030').'&oplus;';	// Width
	//$meta_data .= $d->get_tag('2020', '0030').'&oplus;';	// Height
	//$meta_data .= $d->get_tag('0048', '0003').'&oplus;';	// BitDepth
	$meta_data .= $d->get_tag('0018', '1200').'&oplus;';	// DateOfLastCalibration
	
	return $meta_data;
}

?>

</body>
