
<HTML>
<HEAD>
<style>
body {
  font-family: Arial, Helvetica, sans-serif;
  font-size: 16px;
}
</style>
<TITLE>Borrar sub_directorios con menos de 10 archivos</TITLE>
</HEAD>
<BODY>

<?php
//borrar directorio que contiene menos de 10 archivos

//Leer todos los archivos de la ruta $path (ponerl el final del path sin la diagonal invertida
$path    = 'F:\estudios-2020\unzip';
$files = glob($path.'\*' , GLOB_ONLYDIR);

// path
//   |-- ($dir,$files) Estudios de diferentes pacientes
//			|-- ($sub_dir,$sub_files) Varios estudios del mismo paciente
//						|-- ($sub_sub_files) Archivos DICOM (slices)

//recorrer estudios (todos los pacientes)
foreach($files as $dir){	
	//leer los sub_folders
	echo $dir."<br>";
	$sub_files = glob($dir.'\*' , GLOB_ONLYDIR);	
	//recorrer cada sub_folder (los estudios de cada estudio)
	foreach($sub_files as $sub_dir){				
		//obtiene el nombre del sub_folder desde la posicion 0 a la 17
		$sub_dir_name = substr ( basename($sub_dir).PHP_EOL, 0, 17);		
		if( $sub_dir_name == "1.3.12.2.1107.5.8"){
			echo "&nbsp&nbsp|--&nbsp&nbsp<font color='red'>$sub_dir</font><br>";
			delete_directory($sub_dir);	
		}else{
				
			$sub_sub_files = glob( $sub_dir."\*" ); 		  		
			$num_sub_sub_files = count($sub_sub_files);
			
			if(count($sub_sub_files) < 10) {				
				//eliminar los sub_sub_folders que tengan menos de 10 archivos
				echo "&nbsp&nbsp|--&nbsp&nbsp<font color='red'>$sub_dir</font><br>";
				delete_directory($sub_dir);				
			}else{	
				//elimina archivos frontera en donde probablemente no se encuentren las vertebras L3, L4 y L5
				$inf_bound = round($num_sub_sub_files*.3);
				$sup_bound = round($num_sub_sub_files*.8);				
				$deleted_files = $inf_bound-1+$num_sub_sub_files-$sup_bound;
				$blank_esp = "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp";
				
				echo "&nbsp&nbsp|--&nbsp&nbsp<font color='green'>$sub_dir</font><br>";;				
						
				
				
				echo $blank_esp."Archivos totales: ".$num_sub_sub_files." - frontera inferior: ".$inf_bound." - frontera superior: ".$sup_bound."<br>";					
				echo $blank_esp."Archivos Borrados: ".$deleted_files." - Archivos restantes: ".($num_sub_sub_files-$deleted_files)."<br>";
				
				for($i = $num_sub_sub_files-1; $i>=0; $i--){
					if($i > $sup_bound or $i < $inf_bound){				
						// Use unlink() function to delete a file  
						unlink($sub_sub_files[$i]);
					}
				}	
			}
		}
		
	}					
}

function delete_directory($dirname) {
    if (is_dir($dirname))
		$dir_handle = opendir($dirname);
	if (!$dir_handle)
		return false;
	while($file = readdir($dir_handle)) {
		if ($file != "." && $file != "..") {
			if (!is_dir($dirname."/".$file))
				unlink($dirname."/".$file);
			else
				delete_directory($dirname.'/'.$file);
           }
     }
     closedir($dir_handle);
     rmdir($dirname);
     return true;
}
?>

</body>
