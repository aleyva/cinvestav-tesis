
<HTML>
<HEAD>
<style>
body {
  font-family: Arial, Helvetica, sans-serif;
  font-size: 16px;
}
</style>
<TITLE>Elegir estudios</TITLE>
</HEAD>
<BODY>

<?php
$dicoms = array(
array(119,89,59),
array(161,131,96),
array(108,98,88),
array(73,58,41),
array(55,43,29),
array(61,51,39),
array(42,26,10),
array(49,34,19),
array(121,108,93),
array(131,116,103),
array(64,51,36),
array(61,50,36),
array(55,38,20),
array(61,48,35),
array(60,47,32),
array(44,29,14),
array(59,41,24),
array(68,55,40),
array(66,54,41),
array(27,20,14),
array(51,46,40),
array(30,24,18),
array(43,29,13),
array(51,35,19),
array(74,59,2),
array(62,47,30),
array(53,40,21),
array(76,63,49),
array(72,58,43),
array(98,67,31),
array(146,101,54),
array(141,97,57),
array(160,117,83),
array(36,25,10),
array(312,271,232),
array(103,94,84),
array(41,30,18),
array(59,46,30),
array(52,35,20),
array(76,61,45),
array(60,43,26),
array(380,337,287),
array(189,152,105),
array(176,131,83),
array(175,139,99),
array(75,59,43),
array(57,44,30),
array(69,54,36),
array(56,45,28),
array(173,129,83),
array(143,116,81),
array(209,164,121),
array(84,45,1),
array(58,44,30),
array(81,68,51),
array(140,125,110),
array(133,122,107),
array(172,121,76),
array(82,67,53),
array(72,57,38),
array(66,52,37),
array(51,40,30),
array(180,128,80),
array(111,97,81),
array(131,93,50),
array(222,184,140),
array(153,110,70),
array(49,37,23),
array(54,39,22),
array(185,132,95),
array(154,120,75),
array(67,51,35),
array(122,108,93),
array(124,90,56),
array(64,48,31),
array(77,64,51),
array(58,43,29),
array(58,43,27),
array(53,41,29),
array(77,62,46),
array(62,50,35),
array(66,51,35),
array(66,51,36),
array(112,97,81),
array(59,45,30),
array(50,37,23),
array(150,109,60),
array(136,123,106),
array(39,26,18),
array(57,43,28),
array(96,81,65),
array(72,59,48),
array(59,44,29),
array(71,54,40),
array(63,49,33),
array(142,113,69),
array(148,98,52),
array(72,56,41),
array(169,125,81),
array(110,67,28),
array(363,321,272),
array(70,57,40),
array(170,122,78),
array(281,253,215),
array(186,148,101),
array(185,138,92),
array(70,55,38),
array(114,73,29),
array(210,161,109),
array(106,68,28),
array(64,51,36),
array(65,50,33),
array(104,91,76),
array(147,104,62),
array(64,51,35),
array(78,56,36),
array(81,66,51),
array(52,37,23),
array(147,118,83),
array(67,53,43),
array(81,49,20),
array(71,58,41),
array(49,35,18),
array(48,32,18),
array(55,36,20),
array(55,41,25),
array(73,62,46),
array(69,51,35),
array(177,137,96),
array(64,51,36),
array(55,38,22),
array(208,170,117),
array(88,72,56),
array(50,37,21),
array(51,36,22),
array(141,99,51),
array(154,110,63),
array(178,136,92),
array(171,127,77),
array(145,108,76),
array(164,128,88),
array(166,125,83),
);

//Leer todos los archivos de la ruta $path (ponerl el final del path sin la diagonal invertida
$path    = 'G:\estudios-2020\unzip-renamed';
$path_dest    = 'G:\estudios-2020\L3-v2\\';
$files = glob($path.'\*' , GLOB_ONLYDIR);

// path
//   |-- ($dir,$files) Estudios de diferentes pacientes
//			|-- ($sub_dir,$sub_files) Varios estudios del mismo paciente
//						|-- ($sub_sub_files) Archivos DICOM (slices)

$i_estudios = 1;
//recorrer estudios (todos los pacientes)
foreach($files as $dir){	
	
	echo "<strong>$i_estudios</strong><br>";	
	
	//leer los sub_folders
	echo $dir."<br>";
	$sub_files = glob($dir.'\*' , GLOB_ONLYDIR);	
	
	//recorrer cada sub_folder (los diferentes estudios de cada paciente)
	foreach($sub_files as $sub_dir){							
		$sub_sub_files = glob( $sub_dir."\*" ); 		  		
		//$num_sub_sub_files = count($sub_sub_files);
				
		echo "&nbsp&nbsp|--&nbsp&nbsp$sub_dir<br>";
		$file_L3 ="";
		$file_L3 = $sub_sub_files[$dicoms[$i_estudios-1][2]-1];					
				
		$path_parts = pathinfo($file_L3);		
		$new_name = sprintf('%04d', $i_estudios);
		//$new_file = $path_dest.$new_name."-L3.".$path_parts['extension'];		
		$new_file = $path_dest.$new_name."-L3-".$path_parts['filename'].".".$path_parts['extension'];
		echo "&nbsp&nbsp&nbsp&nbsp|--&nbsp&nbsp$file_L3<br>";
		if(!copy($file_L3,$new_file)){
			echo "failed to copy $file";
		}
	}	
	
	$i_estudios++;
	sleep(.2);
	
	/*if($i_estudios > 2)
		break;
	*/
}

?>

</body>
