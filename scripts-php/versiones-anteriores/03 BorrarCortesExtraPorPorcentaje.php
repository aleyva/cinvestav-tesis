
<HTML>
<HEAD>
<style>
body {
  font-family: Arial, Helvetica, sans-serif;
  font-size: 16px;
}
</style>
<TITLE>Borrar cortes del estudio que no estan entre el 30% y 80% de las imagenes
</TITLE>
</HEAD>
<BODY>

<?php

//Leer todos los archivos de la ruta $path (ponerl el final del path sin la diagonal invertida
$path    = 'C:\Users\Spidey\Desktop\prueba';
$files = glob($path.'\*' , GLOB_ONLYDIR);

// path
//   |-- ($dir,$files) Estudios de diferentes pacientes
//			|-- ($sub_dir,$sub_files) Varios estudios del mismo paciente
//						|-- ($sub_sub_files) Archivos DICOM (slices)

//recorrer estudios (todos los pacientes)
foreach($files as $dir){	
	//leer los sub_folders
	echo $dir."<br>";
	$sub_files = glob($dir.'\*' , GLOB_ONLYDIR);	
	//recorrer cada sub_folder (los diferentes estudios de cada paciente)
	foreach($sub_files as $sub_dir){	
		//nombre del folder
		echo "&nbsp&nbsp|--&nbsp&nbsp".$dir."<br>";
		
		$sub_sub_files = glob( $sub_dir."\*" ); 
		$num_sub_sub_files = count($sub_sub_files);
		$inf_bound = round($num_sub_sub_files*.3);
		$sup_bound = round($num_sub_sub_files*.8);
		$blank_esp = "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp";
		$deleted_files = $inf_bound-1+$num_sub_sub_files-$sup_bound;
		echo $blank_esp."Archivos totales: ".$num_sub_sub_files." - frontera inferior: ".$inf_bound." - frontera superior: ".$sup_bound."<br>";					
		echo $blank_esp."Archivos Borrados: ".$deleted_files." - Archivos restantes: ".($num_sub_sub_files-$deleted_files)."<br>";
		
		for($i = $num_sub_sub_files-1; $i>=0; $i--){
			if($i > $sup_bound or $i < $inf_bound){				
				// Use unlink() function to delete a file  
				unlink($sub_sub_files[$i]);
			}
		}				
	}					
}

?>

</body>
