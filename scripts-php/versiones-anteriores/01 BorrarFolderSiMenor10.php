
<HTML>
<HEAD>
<style>
body {
  font-family: Arial, Helvetica, sans-serif;
  font-size: 16px;
}
</style>
<TITLE>Borrar sub_directorios con menos de 10 archivos</TITLE>
</HEAD>
<BODY>

<?php
//borrar directorio que contiene menos de 10 archivos

//Leer todos los archivos de la ruta $path (ponerl el final del path sin la diagonal invertida
$path    = 'F:\estudios-2020\unzip';
//$files = array_diff(scandir($path), array('.', '..'));
$files = glob($path.'\*' , GLOB_ONLYDIR);


//Para cada archivo
foreach($files as $dir){	
	//leer los subfolders
	echo "- ".$dir."<br>";
	$sub_files = glob($dir.'\*' , GLOB_ONLYDIR);	
	//recorrer cada subfolder
	foreach($sub_files as $sub_dir){		
		//eliminar los folders que tengan menos de 10 archivos
		$sub_sub_files = glob( $sub_dir."\*" ); 		  
		if( $sub_sub_files ) { 
			if(count($sub_sub_files) < 10) {				
				echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<font color='red'>$sub_dir</font><br>";
				delete_directory($sub_dir);				
			}else{				
				echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<font color='green'>$sub_dir</font><br>";;
			}
		}
	}					
}

function delete_directory($dirname) {
    if (is_dir($dirname))
		$dir_handle = opendir($dirname);
	if (!$dir_handle)
		return false;
	while($file = readdir($dir_handle)) {
		if ($file != "." && $file != "..") {
			if (!is_dir($dirname."/".$file))
				unlink($dirname."/".$file);
			else
				delete_directory($dirname.'/'.$file);
           }
     }
     closedir($dir_handle);
     rmdir($dirname);
     return true;
}
?>

</body>
