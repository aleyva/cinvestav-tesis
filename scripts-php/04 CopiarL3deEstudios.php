
<HTML>
<HEAD>
<style>
body {
  font-family: Arial, Helvetica, sans-serif;
  font-size: 16px;
}
</style>
<TITLE>Elegir estudios</TITLE>
</HEAD>
<BODY>

<?php
$dicoms = array(
array(119,89,59),
array(161,131,96),
);

//Leer todos los archivos de la ruta $path (ponerl el final del path sin la diagonal invertida
$path    = 'G:\estudios-2020\unzip-renamed';
$path_dest    = 'G:\estudios-2020\CortesL3\\';
$files = glob($path.'\*' , GLOB_ONLYDIR);

// path
//   |-- ($dir,$files) Estudios de diferentes pacientes
//			|-- ($sub_dir,$sub_files) Varios estudios del mismo paciente
//						|-- ($sub_sub_files) Archivos DICOM (slices)

$i_estudios = 1;
//recorrer estudios (todos los pacientes)
foreach($files as $dir){	
	
	echo "<strong>$i_estudios</strong><br>";	
	
	//leer los sub_folders
	echo $dir."<br>";
	$sub_files = glob($dir.'\*' , GLOB_ONLYDIR);	
	
	//recorrer cada sub_folder (los diferentes estudios de cada paciente)
	foreach($sub_files as $sub_dir){							
		$sub_sub_files = glob( $sub_dir."\*" ); 		  		
		//$num_sub_sub_files = count($sub_sub_files);
				
		echo "&nbsp&nbsp|--&nbsp&nbsp$sub_dir<br>";
		$new_name = sprintf('%04d', $i_estudios);
		
		$file_L3 ="";		
		$file_L3 = $sub_sub_files[$dicoms[$i_estudios-1][0]];					
		$path_parts_L3 = pathinfo($file_L3);				
		
		//$new_file = $path_dest.$new_name."-L3.".$path_parts['extension'];				
		$new_file_L3 = $path_dest.$new_name."-L3-".$path_parts_L3['filename'].".".$path_parts_L3['extension'];		
		echo "&nbsp&nbsp&nbsp&nbsp|--&nbsp&nbsp$file_L3<br>";
		
		if(!copy($file_L3,$new_file_L3)){ echo "failed to copy L3 <br>"; } sleep(.2);	
	}	
	
	$i_estudios++;
	
	/*if($i_estudios > 2)
		break;
	*/
}

?>

</body>
