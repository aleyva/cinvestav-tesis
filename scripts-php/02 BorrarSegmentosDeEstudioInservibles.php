
<HTML>
<HEAD>
<style>
body {
  font-family: Arial, Helvetica, sans-serif;
  font-size: 16px;
}
</style>
<TITLE>Borrar estudios</TITLE>
</HEAD>
<BODY>

<?php
//EJECUTAR SCRIPT CON HASTA 150 PACIENTES

$slices_arr = array(
array('1.3.76.13.1046.2.20200110000855.100276782.1',431),
array('1.3.76.13.1046.2.20200110092613.100276875.1',419),
array('1.3.76.13.1046.2.20200110131333.100277172.1',475),
array('1.3.76.13.1046.2.20200110233656.100277351.1',457),
array('1.3.76.13.1046.2.20200112124025.100277678.1',375),
array('1.3.76.13.1046.2.20200112131020.100277684.1',454),
array('1.3.76.13.1046.2.20200113022156.100277810.1',459),

);

//importar clase para leer archivo DICOM
require_once('./read_dicom/class_dicom/class_dicom.php');


//Leer todos los archivos de la ruta $path (ponerl el final del path sin la diagonal invertida
$path    = 'W:\tesis\Todos-os\2020-01';

// path
//   |-- ($dir,$files) Estudios de diferentes pacientes
//			|-- ($sub_dir,$sub_files) Varios estudios del mismo paciente
//						|-- ($sub_sub_files) Archivos DICOM (slices)

$files = glob($path.'\*' , GLOB_ONLYDIR);
$i_estudios = 0;
//recorrer estudios (todos los pacientes)
foreach($files as $dir){	
	//leer los sub_folders
	echo $dir."<br>";
	$sub_files = glob($dir.'\*' , GLOB_ONLYDIR);	
	
	$dir_name = basename($dir);
	if($slices_arr[$i_estudios][0] == $dir_name){		
		//recorrer cada sub_folder (los diferentes estudios de cada paciente)
		foreach($sub_files as $sub_dir){					
						
			$sub_dir_name = basename($sub_dir);
						
			$sub_sub_files = glob( $sub_dir."\*" ); 		  		
			$num_sub_sub_files = count($sub_sub_files);
					
			// calculo de fronteras
			$thickness = get_thickness($sub_sub_files[0]);		
			$sup_bound = $slices_arr[$i_estudios][1];
			$inf_bound = $sup_bound-120;
			if($thickness == 1.5) $inf_bound = $sup_bound-220;
			if($thickness <= 1) $inf_bound = $sup_bound-330;
			$sup_bound += 5;		
			//verificar que con las fronteras no se haya excedido el número de archivos
			if($inf_bound < 0) $inf_bound = 0;
			if($sup_bound >= $num_sub_sub_files) $sup_bound = $num_sub_sub_files -1;
				
			
			//for para borrar archivos de la frontera superior al final
			for($i = $num_sub_sub_files-1; $i>$sup_bound; $i--){				
				unlink($sub_sub_files[$i]);				
			}			
			//for para borrar archivos de la frontera inferior al inicio
			for($i = $inf_bound; $i>=0; $i--){				
				unlink($sub_sub_files[$i]);				
			}
			
			$blank_esp = "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp";
			echo "&nbsp&nbsp|--&nbsp&nbsp$sub_dir_name&nbsp-&nbspThickness:$thickness<br>";			
			echo $blank_esp."Archivos totales: ".$num_sub_sub_files." - frontera inferior: ".$inf_bound." - frontera superior: ".$sup_bound."<br>";					
		
		}
	}else{
		echo "&nbsp&nbsp|--&nbsp&nbsp<font color='red'>$dir_name</font> no cuadran los datos<br>";
	}
	$i_estudios++;	
}

function get_thickness($file_path){	
	if(!$file_path) {
	  print "USAGE: ./get_tags.php <FILE>\n";
	  exit;
	}
	if(!file_exists($file_path)) {
	  print "$file: does not exist\n";
	  exit;
	}

	$d = new dicom_tag($file_path);	
	$d->load_tags();

	return $d->get_tag('0018', '0050');	
}

?>

</body>
