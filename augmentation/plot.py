#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from imgaug import draw_grid                 #quitar esta opcion libreria
from skimage.io import imsave
#from matplotlib.image import imsave
from imgaug.augmentables.segmaps import SegmentationMapsOnImage


def normalize_to_uint8(img, boundaries = None):
    if (boundaries != None):
        img[img<boundaries[0]] = boundaries[0]
        #img[img>boundaries[1]] = boundaries[0]     # para no mostrar hueso
        img[img>boundaries[1]] = boundaries[1]      # para mostra hueso
    min_ = img.min()
    max_ = img.max()
      
    max_ -= min_
    img = ((img - min_)/max_) * 255
    return img.astype(np.uint8)

def image_to_rgb_uint8(img, boundaries = None):   
    img = normalize_to_uint8(img, boundaries)    
    img_ = np.zeros( ( np.array(img).shape[0], np.array(img).shape[1], 3 ), dtype=np.uint8 )
    img_gray = None
    
    if(len(img.shape) == 3):        
        if(img.shape[2] == 3):
            img_ = img
        elif(img.shape[2] == 1):        
            img_gray = np.squeeze(img)
    else:
        if(len(img.shape) == 2 ):        
            img_gray = img
        
    if img_gray is not None:
    #if(img_gray != None):
        img_[:,:,0] = img_gray # same value in each channel
        img_[:,:,1] = img_gray
        img_[:,:,2] = img_gray        
    
    #print('***********************')
    #print('IMG Shape:{} - datatype: {}'.format(img_.shape, img_.dtype))
    return img_



def histogram(image, bins = 256, xlabel = None, ylabel =None):    
    if(len(image.shape)==2):
        #plt.hist(img.flatten(), bins=50, color='c')
        plt.hist(image.ravel(), bins = bins)
    else:    
        plt.hist(image.ravel(), bins = bins, color = 'orange', )
        plt.hist(image[:, :, 0].ravel(), bins = bins, color = 'red', alpha = 0.5)
        plt.hist(image[:, :, 1].ravel(), bins = bins, color = 'Green', alpha = 0.5)
        plt.hist(image[:, :, 2].ravel(), bins = bins, color = 'Blue', alpha = 0.5)        
        plt.legend(['Total', 'Red_Channel', 'Green_Channel', 'Blue_Channel'])
    
    if(xlabel != None):
        plt.xlabel(xlabel)
    else:
        plt.ylabel(ylabel)
    plt.show()


def imshow(fig,title='', figsize = None):    
    figsize=(fig.shape[1]/100, fig.shape[0]/100)       
    plt.figure(1,figsize = figsize,dpi=300)    
    '''
    left  = 0.125  # the left side of the subplots of the figure
    right = 0.9    # the right side of the subplots of the figure
    bottom = 0.1   # the bottom of the subplots of the figure
    top = 0.9      # the top of the subplots of the figure
    wspace = 0.2   # the amount of width reserved for blank space between subplots
    hspace = 0.2   # the amount of height reserved for white space between subplots
    '''    
    if(title!=''):
        plt.subplots_adjust(left=0, bottom=0, right=1, top=.95, wspace=0, hspace=0)
        f_size = 30    
        plt.title(title, fontsize=f_size)
    else:        
        plt.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=0, hspace=0)

    plt.gca().axes.get_yaxis().set_visible(False)
    plt.gca().axes.get_xaxis().set_visible(False)    
    plt.imshow(fig)
    plt.show()


def imshow_pair(img, mask, title=''): 
    #img and mask, must has 3 channels and in dtype uint8
    img = image_to_rgb_uint8(np.copy(img), (800,1150))    
    mask = image_to_rgb_uint8(np.copy(mask))   
        
    cells = []    
    cells.append(img)
    cells.append(mask)
    
    grid_image = draw_grid(cells, cols=2)        
    imshow(grid_image, title)            



# la imagen debe tener tres canales y ser de tipo uint8
# la mascara debe ser de tipo 'SegmentationMapsOnImage'
# para crear un 'SegmentationMapsOnImage' se le debe pasar un array con 1 canal
# parece que no importa el tipo de dato
def imshow_overlay(img, mask, title='', save = False, index = 0, path = ''):    
    #img and mask, must has 3 channels and in dtype uint8
    img = image_to_rgb_uint8(np.copy(img), (800,1150))     
    
    # create the segmap of mask       
    segmap = SegmentationMapsOnImage(mask, shape=img.shape)
   
    cells = []    
    cells.append(img)                                         # column 1
    cells.append(segmap.draw_on_image(img)[0])                # column 2
    cells.append(segmap.draw(size=segmap.shape[:2])[0])                # column 2
    
    grid_image = draw_grid(cells, cols=3)        
    
    if(save):        
        ext = '.png'        
        mask = np.where(mask == 1, 2, 0) * 255   # convert mask to green color        
        
        imsave(path + '{0:03d}_comp'.format(index) + ext, mask)
        imsave(path + '{0:03d}_gray'.format(index) + ext, img)
        imsave(path + '{0:03d}_over'.format(index) + ext, grid_image)
    else:
        imshow(grid_image,title)
        
        
def imsave_res(img, mask, path, file_name):        
    #img and mask, must has 3 channels and in dtype uint8
    #img = image_to_rgb_uint8(np.copy(img), (800,1150))     
    img = image_to_rgb_uint8(np.copy(img), (900,1120))     
    
    # create the segmap of mask   
    mask_green = np.where(mask == 1, 2, 0).astype(np.uint8)  # convert mask to green color        
    segmap = SegmentationMapsOnImage(mask_green, shape=img.shape)
   
    cells = []                                            
    cells.append(segmap.draw_on_image(img)[0])    
    grid_image = draw_grid(cells, cols=1)        
    
    mask_green = image_to_rgb_uint8(mask)
    mask_green[:,:,0] = 0
    mask_green[:,:,2] = 0
    #imshow(mask_green)    
    #imshow(img)    
    
    #ext = '.png'               
    file_name = file_name.replace("dcm", "png")
    file_name = file_name.replace("L3", "mask")    
    imsave(path + file_name, mask_green)
    file_name = file_name.replace("mask", "gray")
    imsave(path + file_name, img)
    file_name = file_name.replace("gray", "overlay")
    imsave(path + file_name, grid_image)
    
    

def imshow_prediction(img, mask_gt, mask_pred, title='', file_name=None, show=True):    
    #img and mask, must has 3 channels and in dtype uint8
    img = image_to_rgb_uint8(np.copy(img), (900,1120))     
        
    # create the segmap of ground truth
    mask_gt_ = np.where(mask_gt == 1, 2, 0).astype(np.uint8)  # convert mask to green color        
    segmap_gt = SegmentationMapsOnImage(mask_gt_, shape=img.shape)
    
    # create the segmap of prediction
    #mask_pred_ = np.where(mask_pred == 1, 1, 0).astype(np.uint8)  # convert mask to green color        
    segmap_pred = SegmentationMapsOnImage(mask_pred, shape=img.shape)
    
    mask_overlapped = mask_gt_ + mask_pred
    segmap_overlapped = SegmentationMapsOnImage(mask_overlapped, shape=img.shape)
   
    cells = []    
    cells.append(segmap_gt.draw_on_image(img)[0])                
    cells.append(segmap_pred.draw_on_image(img)[0])                
    cells.append(segmap_overlapped.draw_on_image(img)[0])                
    cells.append(segmap_gt.draw(size=segmap_gt.shape[:2])[0])
    cells.append(segmap_pred.draw(size=segmap_pred.shape[:2])[0])
    cells.append(segmap_overlapped.draw(size=segmap_overlapped.shape[:2])[0])
    grid_image = draw_grid(cells, cols=3)    
        
    if(show == True):
        imshow(grid_image,title)
    if(file_name!=None):
        imsave(file_name, grid_image)



#the other methot tho plot overlay image
'''
ext = '.png'
prediction_path = base_path + '/v2-predicted/predicted/' 
    
for index, img in tqdm( enumerate(test_imgs) ):
    
    if(save_results):
      # convert gray image from np.shape (img_height, img_width, 1) to RGB image
      mask_comp = np.zeros([img_height, img_width, 3],dtype=np.uint8)    
      mask_comp[:,:,1] = (y_true[index] & y_pred[index])*255 # colide ground true and predicted    
      mask_comp[:,:,0] = (y_true[index] ^ y_pred[index])*255 # error (red color)
      mask_comp = Image.fromarray(mask_comp, 'RGB')

      # creating the overlay image gray + mask_comp
      gray = np.squeeze(test_imgs[index])
      gray = Image.fromarray( cv2.merge([gray,gray,gray])  , 'RGB')    
      overlay = Image.blend(gray, mask_comp, 0.3)

      # saving the images    
      mask_comp.save(prediction_path + '{0:03d}_comp'.format(index) + ext)
      overlay.save(prediction_path + '{0:03d}_over'.format(index) + ext)
      gray.save(prediction_path + '{0:03d}_gray'.format(index) + ext)
    '''


'''
left  = 0.125  # the left side of the subplots of the figure
right = 0.9    # the right side of the subplots of the figure
bottom = 0.1   # the bottom of the subplots of the figure
top = 0.9      # the top of the subplots of the figure
wspace = 0.2   # the amount of width reserved for blank space between subplots
hspace = 0.2   # the amount of height reserved for white space between subplots
'''
'''
plt.figure(1,figsize=(20,20),dpi=72)
plt.subplots_adjust(left=.1, bottom=.1, right=.95, top=.95, wspace=.07, hspace=.07)

f_size = 30

plt.subplot(221)
plt.title('dicom', fontsize=f_size)
plt.imshow(dicom.pixel_array, cmap=plt.cm.bone)
plt.gca().axes.get_yaxis().set_visible(False)
plt.gca().axes.get_xaxis().set_visible(False)

plt.subplot(222)
plt.title('Hu [10,40]', fontsize=f_size)
plt.imshow(hu_muscle, cmap='gray', vmin=0, vmax=255)
plt.gca().axes.get_yaxis().set_visible(False)
plt.gca().axes.get_xaxis().set_visible(False)

# plot the image using matplotlib
#plt.imshow(dicom.pixel_array, cmap=plt.cm.bone)
plt.subplot(223)
plt.title('segmentation mask', fontsize=f_size)
plt.imshow(mask, cmap='gray')
plt.gca().axes.get_yaxis().set_visible(False)
plt.gca().axes.get_xaxis().set_visible(False)

plt.subplot(224)
plt.title('lean muscle cm²: ' + '{0:.2f}'.format(cm2), fontsize=f_size)
plt.imshow(lean_muscle_mask, cmap='gray')
plt.gca().axes.get_yaxis().set_visible(False)
plt.gca().axes.get_xaxis().set_visible(False)

plt.show()
'''


'''
def overlay_mask(gray, mask, img_height, img_width):    
    # convert gray image from np.shape (img_height, img_width, 1) to RGB image
    ovl_mask = np.zeros([img_height, img_width, 3],dtype=np.uint8)
    mask[:,:,1] = mask*255    
    mask = Image.fromarray(mask, 'RGB')
    gray = np.squeeze(test_imgs[index])
    gray = Image.fromarray( cv2.merge([gray,gray,gray])  , 'RGB')    
    overlay = Image.blend(gray, mask, 0.3)

    # save the predicted mask and the overlay mask in dicom gray image representation
    filename_mask = test_mask_path + '{0:03d}'.format(index) + ext            
    mask.save(filename_mask)

    filename_overlay = test_mask_path + 'over_{0:03d}'.format(index) + ext            
    overlay.save(filename_overlay)
'''
