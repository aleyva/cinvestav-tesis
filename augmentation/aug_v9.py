
import os
from skimage.io import imread
import numpy as np
import plot
from pydicom import dcmread
import imgaug as ia
from imgaug import draw_grid                 #quitar esta opcion libreria
import imgaug.augmenters as iaa
from imgaug.augmentables.segmaps import SegmentationMapsOnImage


def rand():
    return np.random.random_sample()

def load_imgs():
    base_path = '/mnt/Home/Proyectos/Maestria/14-tesis/dicoms/dicoms-v3/dataset-model-expertSeg/'
    dicom_file = os.path.join(base_path, 'dicom/0001-L3-20190716-F-071Y-2mm-B.dcm')
    mask_file = os.path.join(base_path,'mask-v3_4 (tesis)/mask/0001-mask-20190716-F-071Y-2mm-B.png')    
    gray_file = os.path.join(base_path,'gray-representation/0001-tgray-20190716-F-071Y-2mm-B.png')
        
    dicom = dcmread( dicom_file )            
    dicom.pixel_array[dicom.pixel_array>2500] = 2500   # mandatory step to normalize the dynamic range            
    dicom = dicom.pixel_array  
    #gray = imread(  gray_file  )
    #mask = imread(  mask_file  )
    mask = imread(  mask_file  )[:,:,1]

    return dicom, mask
'''
def save_image(images_aug, segmaps_aug, filename='example_segmaps.jpg'):
    # We want to generate an image containing the original input image and
    # segmentation maps before/after augmentation. (Both multiple times for
    # multiple augmentations.)
    #
    # The whole image is supposed to have five columns:
    # (1) original image,
    # (2) original image with segmap,
    # (3) augmented image,
    # (4) augmented segmap on augmented image,
    # (5) augmented segmap on its own in.
    #
    # We now generate the cells of these columns.
    #
    # Note that draw_on_image() and draw() both return lists of drawn
    # images. Assuming that the segmentation map array has shape (H,W,C),
    # the list contains C items.
    cells = []
    for image_aug, segmap_aug in zip(images_aug, segmaps_aug):
        cells.append(gray)                                         # column 1
        cells.append(segmap.draw_on_image(gray)[0])                # column 2
        cells.append(image_aug)                                     # column 3
        cells.append(segmap_aug.draw_on_image(image_aug)[0])        # column 4
        cells.append(segmap_aug.draw(size=image_aug.shape[:2])[0])  # column 5
    
    # Convert cells to a grid image and save.
    grid_image = ia.draw_grid(cells, cols=5)
    imageio.imwrite(filename, grid_image)
 '''   

# load images
dicom, mask = load_imgs()

# set the seeds
np.random.seed(1)
ia.seed(20)


#------------------
#   probability
#------------------
# set value between 0 and 1, ex 0.3

prob_scale = 0
prob_rotate = 0
prob_shear = 0
prob_translate = 0
prob_flipx = 0
prob_perspective = 0
prob_piecewise = 0
prob_blur = 0  # Be careful, blur and sharpen could be applied in the same image              
prob_sharpen = 0 # esta haciendo las imagenes grises en el fondo
prob_noise = 1
prob_coarse_dropout = 0  # cutout, dropout or coarseDropout?


'''
# set value between 0 and 1, ex 0.3
prob_scale = .5
prob_rotate = .5
prob_shear = .2
prob_translate = .4
prob_flipx = .3
prob_perspective = .3
prob_piecewise = .2
prob_blur = .15  # if blur is set, the sharpen will not be enable
prob_sharpen = .3
prob_noise = .2
prob_coarse_dropout = .2  # cutout, dropout or coarseDropout?
'''

prob_scale = .7
prob_rotate = .7
prob_translate = .7
prob_piecewise = .6
prob_shear = .4
prob_perspective = .4
prob_flipx = .2
prob_blur = .2  # Be careful, blur and sharpen could be applied in the same image              
prob_sharpen = .2 # esta haciendo las imagenes grises en el fondo
prob_noise = .2
prob_coarse_dropout = .2  # cutout, dropout or coarseDropout?

#------------------
#   parameters
#------------------
val_scale = (0.8, 1.15) # ¿separar en x y y?  scale={"x": (0.5, 1.5), "y": (0.5, 1.5)}
val_rotate = (-20, 20)
val_shear = (-16, 16)
val_translate_percent= {"x": (-0.1, 0.1), "y": (-0.25, 0.15)}
val_perspective = (0.02, 0.04)
val_piecewise = (0.01, 0.03)
val_blur = (1, 4)  #programar para aplicar blur o sharpen
val_sharpen = (0.03, 0.11)
val_noise = (0.005,0.1)
val_coarse_dropout = (0.01, 0.05)
val_coarse_dropout_percent = (0.01, 0.1)

#------------------
#   sequence the transformations
#------------------

# create the secuential transform
seq = iaa.Sequential([    
    iaa.Sometimes( prob_scale, iaa.Affine( scale=val_scale ) ),
    iaa.Sometimes( prob_rotate, iaa.Affine( rotate=val_rotate ) ),
    iaa.Sometimes( prob_shear, iaa.Affine( shear=val_shear ) ),
    iaa.Sometimes( prob_translate, iaa.Affine( translate_percent=val_translate_percent ) ),
    iaa.Fliplr( prob_flipx ) ,
    iaa.Sometimes( prob_perspective, iaa.PerspectiveTransform( scale=val_perspective ) ),
    iaa.Sometimes( prob_piecewise, iaa.PiecewiseAffine( scale=val_piecewise ) ),
    
    
    iaa.Sometimes( prob_blur, iaa.GaussianBlur(sigma=val_blur) ),     
    iaa.Sometimes( prob_sharpen, iaa.Sharpen(alpha=val_sharpen) ),

#	AdditiveGaussianNoise
    #iaa.AdditiveGaussianNoise(scale=(0, 0.1*255))
    #	AdditiveLaplaceNoise   between salt and peper and gaussian
    #iaa.AdditiveLaplaceNoise(scale=(0, 0.08*255))
    #	ReplaceElementwise
    #iaa.ReplaceElementwise(0.02, [0, 255])
    #	SaltAndPepper
    #iaa.SaltAndPepper(0.05)
    #	Salt
    #iaa.Salt(0.03)
    #	Pepper
    iaa.Sometimes( prob_noise, iaa.AdditiveGaussianNoise(scale=(10, 50)) ),     
    #iaa.Sometimes( prob_noise, iaa.Pepper(val_noise) ),      
    iaa.Sometimes( prob_coarse_dropout, iaa.CoarseDropout(val_coarse_dropout, size_percent=val_coarse_dropout_percent) ),
], random_order=True)


#seq = iaa.Sequential(iaa.Rotate((0, 90)) , random_order=True)

# create the mask
segmap = np.zeros((512, 512, 1), dtype=np.int32)
segmap[mask > 100] = 2
segmap = SegmentationMapsOnImage(segmap, shape=dicom.shape)

   
# Augment images and segmaps.
'''
images_aug = []
segmaps_aug = []
for _ in range(5):
    images_aug_i, segmaps_aug_i = seq(image=gray, segmentation_maps=segmap)
    images_aug.append(images_aug_i)
    segmaps_aug.append(segmaps_aug_i)
save_image(images_aug, segmaps_aug, filename='example_segmaps.jpg')
'''

dicom_3d = plot.image_to_rgb_uint8(dicom, (800,1150))   
cells = []    
for _ in range(5):
    images_aug_i, segmaps_aug_i = seq(image=dicom, segmentation_maps=segmap)        
    images_aug_i_3d = plot.image_to_rgb_uint8(images_aug_i, (800,1150))
    
    
    cells.append(dicom_3d)                                   
    cells.append(segmap.draw_on_image(dicom_3d)[0])      
    cells.append(segmap.draw(size=segmap.shape[:2])[0])     
    cells.append(segmaps_aug_i.draw_on_image(images_aug_i_3d)[0])      
    cells.append(segmaps_aug_i.draw(size=segmap.shape[:2])[0])  
    
    '''
    cells.append(images_aug_i_3d)               
    cells.append(segmaps_aug_i.draw(size=segmap.shape[:2])[0])  
    cells.append(segmaps_aug_i.draw_on_image(images_aug_i_3d)[0])      
    '''
   
grid_image = draw_grid(cells, cols=5)    
   
plot.imshow(grid_image)

