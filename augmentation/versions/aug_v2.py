
import os
from skimage.io import imread
import imgaug.augmenters as iaa
import matplotlib.pyplot as plt
import numpy as np


def load_imgs():
    base_path = '/mnt/Home/Proyectos/Maestria/14-tesis/dicoms/dicoms-v3/measure-test/'
    dicom_file = os.path.join(base_path,'0007-L3-20190722-F-059Y-1mm-B20f.dcm')    
    mask_file = os.path.join(base_path,'0007-mask-20190722-F-059Y-1mm-B20f.png')
    gray_file = os.path.join(base_path,'0007-sgray-20190722-F-059Y-1mm-B20f.png')

    gray = imread(  gray_file  )
    mask = imread(  mask_file  )

    return gray, mask

def save_image(gt, aug, name_gt='ground truth', name_aug='aug', filename='./test.png'):
    '''
    left  = 0.125  # the left side of the subplots of the figure
    right = 0.9    # the right side of the subplots of the figure
    bottom = 0.1   # the bottom of the subplots of the figure
    top = 0.9      # the top of the subplots of the figure
    wspace = 0.2   # the amount of width reserved for blank space between subplots
    hspace = 0.2   # the amount of height reserved for white space between subplots
    '''
    plt.figure(1,figsize=(10,5.3),dpi=72)
    plt.subplots_adjust(left=0, bottom=0, right=1, top=.95, wspace=0, hspace=0)
    f_size = 18

    plt.subplot(121)
    plt.title(name_gt, fontsize=f_size)
    plt.imshow(gt)
    plt.gca().axes.get_yaxis().set_visible(False)
    plt.gca().axes.get_xaxis().set_visible(False)

    plt.subplot(122)
    plt.title(name_aug, fontsize=f_size)
    #plt.imshow(hu_muscle, cmap='gray', vmin=0, vmax=255)
    plt.imshow(aug)
    plt.gca().axes.get_yaxis().set_visible(False)
    plt.gca().axes.get_xaxis().set_visible(False)
    
    plt.show()
    #plt.savefig(filename)

def create_transform(img_gt, transformation, transformation_name, i_img):        
    filename = '{0:03d}-'.format(i_img) + str(transformation_name) + '.png'
    save_image(img_gt, transformation.augment_image(gray), name_gt='ground truth', name_aug=transformation_name, filename=filename)
    return i_img + 1


plt.ioff()
gray, mask = load_imgs()
i_img=1



apply_scale = False
apply_rotate = False
apply_shear = False
apply_translate = False
apply_flip = False
apply_perspective = False
apply_piecewise = False

apply_blur = False
apply_sharpen = False
apply_noise = False

#------------------
# duda para ocupar
#------------------
apply_erosion = False #modifica la correspondencia con la máscara?
apply_dilation = False #modifica la correspondencia con la máscara?
apply_bright = False # es pruedente aplicar brillo cuando ?
apply_cutout = False
apply_dropout = False
apply_jigsaw = False


#------------------
# values
#------------------
val_scale = (0.2, 0.3) # ¿separar en x y y?  scale={"x": (0.5, 1.5), "y": (0.5, 1.5)}
val_rotate = (-20, 20)
val_shear = (-16, 16)
val_translate_percent= {"x": (-0.1, 0.1), "y": (-0.1, 0.1)}
val_flip = 1
val_perspective = (0.03, 0.075)
val_piecewise = (0.01, 0.05)




if not apply_scale:         val_scale = None
if not apply_rotate:        val_rotate = None
if not apply_shear:         val_shear = None
if not apply_translate:     val_translate_percent = None
if not apply_flip:          val_flip = None
if not apply_perspective:   val_perspective = None
if not apply_piecewise:     val_perspective = None


# order ?
# mode="constant"  # cuando rellena las imagenes lo hace con el valor 0
trans_geometric = iaa.Affine( scale=val_scale, rotate=val_rotate, shear=val_shear, 
                             translate_percent=val_translate_percent, mode="constant" )



trans = iaa.Sequential([iaa.Affine(translate_px={"x":-40}), iaa.AdditiveGaussianNoise(scale=0.1*255)])


#i_img = create_transform(gray, trans, 'Secuential', i_img)

#	SomeOf
# i_img = create_transform(gray, trans, 'SomeOf', i_img)
#	Sometimes
# i_img = create_transform(gray, trans, 'Sometimes', i_img)



'''
#------------------
#	geometric		
#------------------
#	Affine
#	ScaleX
#	ScaleY
#	TranslateX
#	TranslateY
#	Rotate
#	ShearX
#	ShearY


trans = iaa.Affine(scale=(0.2, 0.3))
aug = iaa.Affine(translate_percent={"x": (-0.2, 0.2), "y": (-0.2, 0.2)})
aug = iaa.Affine(rotate=(-45, 45))
aug = iaa.Affine(shear=(-16, 16))
aug = iaa.Affine(translate_percent={"x": -0.20}, mode=ia.ALL, cval=(0, 255))
i_img = create_transform(gray, trans, 'Geometric transforms', i_img)


#------------------
#	flip		
#------------------
trans = iaa.Fliplr(1)
i_img = create_transform(gray, trans, 'HorizontalFlip', i_img)

#	PerspectiveTransform
trans = iaa.PerspectiveTransform(scale=(0.03, 0.075))
i_img = create_transform(gray, trans, 'PerspectiveTransform', i_img)


trans = iaa.PiecewiseAffine(scale=(0.01, 0.05))
i_img = create_transform(gray, trans, 'PerspectiveTransform', i_img)



#------------------
#	blur		
#------------------

#	BilateralBlur
trans = iaa.BilateralBlur(d=(4, 7))
i_img = create_transform(gray, trans, 'BilateralBlur', i_img)


#------------------
#	sharpen		
#------------------

trans = iaa.Sharpen(alpha=(0.03, 0.12))
i_img = create_transform(gray, trans, 'BilateralBlur', i_img)

#------------
# "Noise"
#------------
#	AdditiveGaussianNoise
trans = iaa.AdditiveGaussianNoise(scale=(0, 0.1*255))
i_img = create_transform(gray, trans, 'AdditiveGaussianNoise', i_img)
#	AdditiveLaplaceNoise   between salt and peper and gaussian
trans = iaa.AdditiveLaplaceNoise(scale=(0, 0.08*255))
i_img = create_transform(gray, trans, 'AdditiveLaplaceNoise', i_img)




#	PiecewiseAffine  # deformación
trans = iaa.PiecewiseAffine(scale=(0.02, 0.05))
i_img = create_transform(gray, trans, 'PiecewiseAffine', i_img)



#------------------
#	blend		
#------------------
#	BlendAlpha
trans = iaa.BlendAlpha(0.5, iaa.Grayscale(1.0))
i_img = create_transform(gray, trans, 'BlendAlpha', i_img)
#	BlendAlphaElementwise
trans = iaa.BlendAlphaElementwise(0.5, iaa.Grayscale(1.0))
i_img = create_transform(gray, trans, 'BlendAlphaElementwise', i_img)
#	BlendAlphaHorizontalLinearGradient
trans = iaa.BlendAlphaHorizontalLinearGradient(iaa.AddToHue((-100, 100)))
i_img = create_transform(gray, trans, 'BlendAlphaHorizontalLinearGradient', i_img)
#	BlendAlphaVerticalLinearGradient
trans = iaa.BlendAlphaVerticalLinearGradient(iaa.AddToHue((-100, 100)))
i_img = create_transform(gray, trans, 'BlendAlphaVerticalLinearGradient', i_img)




#	Jigsaw  #puede no ser una buena idea
trans = iaa.Jigsaw(nb_rows=10, nb_cols=10)
i_img = create_transform(gray, trans, 'Jigsaw', i_img)



# es similar a la dilatacion y erosion, tal vez se pueda ocupar
#------------------
#	pooling		
#------------------
#	MaxPooling # hace brillosa la imagen
trans = iaa.MaxPooling(2)
i_img = create_transform(gray, trans, 'MaxPooling', i_img)
#	MinPooling # quita brillo a la imanen
trans = iaa.MinPooling(2)
i_img = create_transform(gray, trans, 'MinPooling', i_img)


'''








'''


#------------
# Bright
#------------
# aplicar brillo sería incorrecto por que está aprendiendo a segmentar con base en unidades hounsfield
#	Multiply
trans = iaa.Multiply((0.5, 1.5))
i_img = create_transform(gray, trans, 'Multiply', i_img)

#------------------
#	arithmetic bright
#------------------
#	Add as bright
trans = iaa.Add((-100, 100))
i_img = create_transform(gray, trans, 'Add', i_img)
#	AddElementwise


#------------------
#	Cutout / dropout
#------------------
#	CoarseDropout    
# esta transformación se lo aplicaría a la mascara y eso sería incorrecto
trans = iaa.CoarseDropout((0.08, 0.1), size_percent=(0.1, 0.4))
i_img = create_transform(gray, trans, 'CoarseDropout', i_img)


#------------
# "Noise"
#------------
#	ReplaceElementwise
trans = iaa.ReplaceElementwise(0.08, [0, 255])
i_img = create_transform(gray, trans, 'ReplaceElementwise', i_img)
#	ImpulseNoise

#	SaltAndPepper
trans = iaa.SaltAndPepper(0.05)
i_img = create_transform(gray, trans, 'SaltAndPepper', i_img)
#	Salt
trans = iaa.Salt(0.03)
i_img = create_transform(gray, trans, 'Salt', i_img)
#	Pepper
trans = iaa.Pepper(0.03)
i_img = create_transform(gray, trans, 'Pepper', i_img)




'''