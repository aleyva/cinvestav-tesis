
import os
from skimage.io import imread
import imgaug.augmenters as iaa
import matplotlib.pyplot as plt
import numpy as np

from tqdm import tqdm
from skimage.transform import resize 


def load_imgs():
    
    base_path = '/mnt/Home/Proyectos/Maestria/14-tesis/dicoms/dicoms-v3/test/'
    mask_file = os.path.join(base_path,'mask/0007-mask-20190722-F-059Y-1mm-B20f.png')
    gray_file = os.path.join(base_path,'gray/0007-sgray-20190722-F-059Y-1mm-B20f.png')

    gray = imread(  gray_file  )    
    mask = imread(  mask_file  )

    return gray, mask


def load_imgs_2():
    imgs = np.zeros((2, img_height, img_width, number_channels), dtype=np.uint8)
    
    base_path = '/mnt/Home/Proyectos/Maestria/14-tesis/dicoms/dicoms-v3/test/'    
    

    imgs[0] = imread(  os.path.join(base_path,'gray/0001-sgray-20190716-F-071Y-2mm-B.png')  )    
    imgs[1] = imread(  os.path.join(base_path,'gray/0007-sgray-20190722-F-059Y-1mm-B20f.png')  )    
    mask = imread(  os.path.join(base_path,'mask/0007-mask-20190722-F-059Y-1mm-B20f.png')  )

    return imgs, mask


# the train dataset and the test dataset are in the same folder
def read_gray_dataset_v2(img_path, mask_path, img_width, img_height, number_channels, train_fraction):
    
    print("images path: " + img_path)
    print("masks path: " + mask_path)
    # count the number of files
    len_imgs = len(os.listdir(img_path))    
    print(str(len_imgs) + "," + str(len(os.listdir(mask_path))) )
    assert len_imgs == len(os.listdir(mask_path)), 'El número de imagenes y de mascaras deben coincidir'
    assert len_imgs > 2, 'no existen suficientes imágenes para entrenar'
    

    #Create array with zeros of the same size as the resize image size.
    imgs = np.zeros((len_imgs, img_height, img_width, number_channels), dtype=np.uint8)
    masks = np.zeros((len_imgs, img_height, img_width, 3), dtype=np.bool)
    

    print('\nReading and resizing training DICOM images ... ')
    for index, file in tqdm(enumerate(sorted(os.listdir(img_path)))) :              
        img = imread( os.path.join(img_path,file) )
        #img = np.expand_dims(resize(img, (img_height, img_width), mode='constant',  preserve_range=True), axis=-1)    
        #img = resize(img, (img_height, img_width), mode='constant', preserve_range=True)
        imgs[index] = img #Fill empty X_train with values from img
        

    print('\nReading and resizing training masks ... ')    
    for index, file in tqdm(enumerate(sorted(os.listdir(mask_path)))) :              
        # mask = imread(  os.path.join(train_mask_path,file)  )[:,:,1]
        mask = imread(  os.path.join(mask_path,file)  )
        #mask = np.expand_dims(resize(mask, (img_height, img_width), mode='constant',  preserve_range=True), axis=-1)
        #mask[mask < 40] = 0   #clean the mask
        masks[index] = mask
        
    # suffle img and mask without breaking the correspondance   

    #shuffle(imgs,masks)    
    
    # divide datasets into test and train
    idx = int(train_fraction * len_imgs)
    train_imgs = imgs[0:idx]
    train_masks = masks[0:idx]
    test_imgs = imgs[idx:]
    test_masks = masks[idx:]
    
    return train_imgs, train_masks, test_imgs, test_masks




def save_image(gt, aug, name_gt='ground truth', name_aug='aug', filename='./test.png'):
    '''
    left  = 0.125  # the left side of the subplots of the figure
    right = 0.9    # the right side of the subplots of the figure
    bottom = 0.1   # the bottom of the subplots of the figure
    top = 0.9      # the top of the subplots of the figure
    wspace = 0.2   # the amount of width reserved for blank space between subplots
    hspace = 0.2   # the amount of height reserved for white space between subplots
    '''
    plt.figure(1,figsize=(10,5.3),dpi=72)
    plt.subplots_adjust(left=0, bottom=0, right=1, top=.95, wspace=0, hspace=0)
    f_size = 18

    plt.subplot(121)
    plt.title(name_gt, fontsize=f_size)
    plt.imshow(gt)
    plt.gca().axes.get_yaxis().set_visible(False)
    plt.gca().axes.get_xaxis().set_visible(False)

    plt.subplot(122)
    plt.title(name_aug, fontsize=f_size)
    #plt.imshow(hu_muscle, cmap='gray', vmin=0, vmax=255)
    plt.imshow(aug)
    plt.gca().axes.get_yaxis().set_visible(False)
    plt.gca().axes.get_xaxis().set_visible(False)
    
    plt.show()
    #plt.savefig(filename)

def create_transform(img_gt, transformation, transformation_name, i_img):        
    filename = '{0:03d}-'.format(i_img) + str(transformation_name) + '.png'
    save_image(img_gt, transformation.augment_image(img_gt), name_gt='ground truth', name_aug=transformation_name, filename=filename)
    return i_img + 1


plt.ioff()
img_width = 512
img_height = 512
number_channels = 3
train_fraction = .2
img_path = '/mnt/Home/Proyectos/Maestria/14-tesis/dicoms/dicoms-v3/test/gray/'
mask_path = '/mnt/Home/Proyectos/Maestria/14-tesis/dicoms/dicoms-v3/test/mask/'
# load dataset
#gray, mask = load_imgs()
grays, mask = load_imgs_2()
#train_imgs, train_masks, test_imgs, test_masks = read_gray_dataset_v2(img_path, mask_path, img_width, img_height, number_channels, train_fraction)

i_img=1



apply_scale = False
apply_rotate = False
apply_shear = False
apply_translate = False
apply_flip = False
apply_perspective = False
apply_piecewise = False

apply_blur = False
apply_sharpen = False
apply_noise = False

#------------------
# duda para ocupar
#------------------
apply_blend = True
apply_erosion = False #modifica la correspondencia con la máscara?
apply_dilation = False #modifica la correspondencia con la máscara?
apply_bright = False # es pruedente aplicar brillo cuando ?
apply_cutout = False
apply_dropout = False
apply_jigsaw = False



#------------------
#      tolerance values
#------------------
val_scale = (0.2, 0.3) # ¿separar en x y y?  scale={"x": (0.5, 1.5), "y": (0.5, 1.5)}
val_rotate = (-20, 20)
val_shear = (-16, 16)
val_translate_percent= {"x": (-0.1, 0.1), "y": (-0.1, 0.1)}
val_flip = 1
val_perspective = (0.03, 0.075)
val_piecewise = (0.01, 0.05)


# join the transformations

transform_arr = []

'''
#	geometric		
if not apply_scale:         val_scale = None
if not apply_rotate:        val_rotate = None
if not apply_shear:         val_shear = None
if not apply_translate:     val_translate_percent = None
# order ?
# mode="constant"  # cuando rellena las imagenes lo hace con el valor 0

print ( 'val_scale: ' + str(val_scale))

transform_arr.append(iaa.Affine( scale=val_scale, rotate=val_rotate, shear=val_shear, 
                             translate_percent=val_translate_percent, mode="constant" ))
'''

if apply_scale:
    transform_arr.append(iaa.Affine( scale=val_scale ))
if apply_rotate:        
    transform_arr.append(iaa.Affine( rotate=val_rotate ))
if apply_shear:         
    transform_arr.append(iaa.Affine( shear=val_shear ))
if apply_translate:     
    transform_arr.append(iaa.Affine( translate_percent=val_translate_percent ))
if apply_flip:          
    transform_arr.append( iaa.Fliplr( 1 ) )
if apply_perspective:   
    transform_arr.append( iaa.PerspectiveTransform( scale=val_perspective ) )
if apply_piecewise:     
    transform_arr.append( iaa.PiecewiseAffine( scale=val_piecewise ) )

if apply_blend:
    #	BlendAlphaVerticalLinearGradient
    #transform_arr.append( iaa.BlendAlphaVerticalLinearGradient(iaa.AddToHue((-100, 100))) )
    # transform_arr.append( iaa.BlendAlphaElementwise(0.5, iaa.Grayscale(1.0)) )
    
    transform_arr.append(  iaa.BlendAlpha( (0.3, 0.5),iaa.Affine(rotate=(-20, 20)) ) )
    
    

if( len(transform_arr)>0 ) :
    # create the secuential transform
    seq = iaa.Sequential(transform_arr)
    
    # do the agumentation
    #gray_aug = seq.augment_image( gray )
    gray_aug = seq(images=grays)
    
    save_image(grays[0], gray_aug[0], name_gt='ground truth', name_aug='aug', filename='./test.png')
else :
    print ('Nothing to transform')


#	SomeOf
# i_img = create_transform(gray, trans, 'SomeOf', i_img)
#	Sometimes
# i_img = create_transform(gray, trans, 'Sometimes', i_img)


'''
i_img = create_transform(gray, trans, 'Geometric transforms', i_img)



#------------------
#	blur		
#------------------

#	BilateralBlur
trans = iaa.BilateralBlur(d=(4, 7))
i_img = create_transform(gray, trans, 'BilateralBlur', i_img)


#------------------
#	sharpen		
#------------------

trans = iaa.Sharpen(alpha=(0.03, 0.12))
i_img = create_transform(gray, trans, 'BilateralBlur', i_img)

#------------
# "Noise"
#------------
#	AdditiveGaussianNoise
trans = iaa.AdditiveGaussianNoise(scale=(0, 0.1*255))
i_img = create_transform(gray, trans, 'AdditiveGaussianNoise', i_img)
#	AdditiveLaplaceNoise   between salt and peper and gaussian
trans = iaa.AdditiveLaplaceNoise(scale=(0, 0.08*255))
i_img = create_transform(gray, trans, 'AdditiveLaplaceNoise', i_img)




#	PiecewiseAffine  # deformación
trans = iaa.PiecewiseAffine(scale=(0.02, 0.05))
i_img = create_transform(gray, trans, 'PiecewiseAffine', i_img)



#------------------
#	blend		
#------------------
#	BlendAlpha
trans = iaa.BlendAlpha(0.5, iaa.Grayscale(1.0))
i_img = create_transform(gray, trans, 'BlendAlpha', i_img)
#	BlendAlphaElementwise
trans = iaa.BlendAlphaElementwise(0.5, iaa.Grayscale(1.0))
i_img = create_transform(gray, trans, 'BlendAlphaElementwise', i_img)
#	BlendAlphaHorizontalLinearGradient
trans = iaa.BlendAlphaHorizontalLinearGradient(iaa.AddToHue((-100, 100)))
i_img = create_transform(gray, trans, 'BlendAlphaHorizontalLinearGradient', i_img)
#	BlendAlphaVerticalLinearGradient
trans = iaa.BlendAlphaVerticalLinearGradient(iaa.AddToHue((-100, 100)))
i_img = create_transform(gray, trans, 'BlendAlphaVerticalLinearGradient', i_img)




#	Jigsaw  #puede no ser una buena idea
trans = iaa.Jigsaw(nb_rows=10, nb_cols=10)
i_img = create_transform(gray, trans, 'Jigsaw', i_img)



# es similar a la dilatacion y erosion, tal vez se pueda ocupar
#------------------
#	pooling		
#------------------
#	MaxPooling # hace brillosa la imagen
trans = iaa.MaxPooling(2)
i_img = create_transform(gray, trans, 'MaxPooling', i_img)
#	MinPooling # quita brillo a la imanen
trans = iaa.MinPooling(2)
i_img = create_transform(gray, trans, 'MinPooling', i_img)


'''








'''


#------------
# Bright
#------------
# aplicar brillo sería incorrecto por que está aprendiendo a segmentar con base en unidades hounsfield
#	Multiply
trans = iaa.Multiply((0.5, 1.5))
i_img = create_transform(gray, trans, 'Multiply', i_img)

#------------------
#	arithmetic bright
#------------------
#	Add as bright
trans = iaa.Add((-100, 100))
i_img = create_transform(gray, trans, 'Add', i_img)
#	AddElementwise


#------------------
#	Cutout / dropout
#------------------
#	CoarseDropout    
# esta transformación se lo aplicaría a la mascara y eso sería incorrecto
trans = iaa.CoarseDropout((0.08, 0.1), size_percent=(0.1, 0.4))
i_img = create_transform(gray, trans, 'CoarseDropout', i_img)


#------------
# "Noise"
#------------
#	ReplaceElementwise
trans = iaa.ReplaceElementwise(0.08, [0, 255])
i_img = create_transform(gray, trans, 'ReplaceElementwise', i_img)
#	ImpulseNoise

#	SaltAndPepper
trans = iaa.SaltAndPepper(0.05)
i_img = create_transform(gray, trans, 'SaltAndPepper', i_img)
#	Salt
trans = iaa.Salt(0.03)
i_img = create_transform(gray, trans, 'Salt', i_img)
#	Pepper
trans = iaa.Pepper(0.03)
i_img = create_transform(gray, trans, 'Pepper', i_img)




'''