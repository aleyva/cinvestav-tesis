
import os
from skimage.io import imread
import imgaug.augmenters as iaa
import matplotlib.pyplot as plt
import numpy as np


def load_imgs():
    base_path = '/mnt/Home/Proyectos/Maestria/14-tesis/dicoms/dicoms-v3/measure-test/'
    dicom_file = os.path.join(base_path,'0007-L3-20190722-F-059Y-1mm-B20f.dcm')    
    mask_file = os.path.join(base_path,'0007-mask-20190722-F-059Y-1mm-B20f.png')
    gray_file = os.path.join(base_path,'0007-sgray-20190722-F-059Y-1mm-B20f.png')

    gray = imread(  gray_file  )
    mask = imread(  mask_file  )

    return gray, mask

def save_image(gt, aug, name_gt='ground truth', name_aug='aug', filename='./test.png'):
    '''
    left  = 0.125  # the left side of the subplots of the figure
    right = 0.9    # the right side of the subplots of the figure
    bottom = 0.1   # the bottom of the subplots of the figure
    top = 0.9      # the top of the subplots of the figure
    wspace = 0.2   # the amount of width reserved for blank space between subplots
    hspace = 0.2   # the amount of height reserved for white space between subplots
    '''
    plt.figure(1,figsize=(10,5.3),dpi=72)
    plt.subplots_adjust(left=0, bottom=0, right=1, top=.95, wspace=0, hspace=0)
    f_size = 18

    plt.subplot(121)
    plt.title(name_gt, fontsize=f_size)
    plt.imshow(gt)
    plt.gca().axes.get_yaxis().set_visible(False)
    plt.gca().axes.get_xaxis().set_visible(False)

    plt.subplot(122)
    plt.title(name_aug, fontsize=f_size)
    #plt.imshow(hu_muscle, cmap='gray', vmin=0, vmax=255)
    plt.imshow(aug)
    plt.gca().axes.get_yaxis().set_visible(False)
    plt.gca().axes.get_xaxis().set_visible(False)
    
    #plt.show()
    plt.savefig(filename)

def create_transform(img_gt, transformation, transformation_name, i_img):        
    filename = '{0:03d}-'.format(i_img) + str(transformation_name) + '.png'
    save_image(img_gt, transformation.augment_image(gray), name_gt='ground truth', name_aug=transformation_name, filename=filename)
    return i_img + 1


plt.ioff()
gray, mask = load_imgs()
i_img=1



#------------
#	meta		
#------------
#	Sequential
#trans = iaa.Sequential([iaa.Affine(translate_px={"x":-40}), iaa.AdditiveGaussianNoise(scale=0.1*255)])
#i_img = create_transform(gray, trans, 'Secuential', i_img)
#	SomeOf
# i_img = create_transform(gray, trans, 'SomeOf', i_img)
#	OneOf
# i_img = create_transform(gray, trans, 'OneOf', i_img)
#	Sometimes
# i_img = create_transform(gray, trans, 'Sometimes', i_img)
#	WithChannels
# i_img = create_transform(gray, trans, 'WithChannels', i_img)
#	Identity
# i_img = create_transform(gray, trans, 'Identity', i_img)
#	Noop
# i_img = create_transform(gray, trans, 'Noop', i_img)
#	Lambda
# i_img = create_transform(gray, trans, 'Lambda', i_img)
#	AssertLambda
# i_img = create_transform(gray, trans, 'AssertLambda', i_img)
#	AssertShape
# i_img = create_transform(gray, trans, 'AssertShape', i_img)
#	ChannelShuffle
# i_img = create_transform(gray, trans, 'ChannelShuffle', i_img)
#	RemoveCBAsByOutOfImageFraction
# i_img = create_transform(gray, trans, 'RemoveCBAsByOutOfImageFraction', i_img)
#	ClipCBAsToImagePlanes
# i_img = create_transform(gray, trans, 'ClipCBAsToImagePlanes', i_img)

#------------------
#	arithmetic		
#------------------
#	Add
trans = iaa.Add((-100, 100))
i_img = create_transform(gray, trans, 'Add', i_img)
#	AddElementwise
trans = iaa.AddElementwise((-100, 100))
i_img = create_transform(gray, trans, 'AddElementwise', i_img)
#	AdditiveGaussianNoise
trans = iaa.AdditiveGaussianNoise(scale=(0, 0.2*255))
i_img = create_transform(gray, trans, 'AdditiveGaussianNoise', i_img)
#	AdditiveLaplaceNoise
trans = iaa.AdditiveLaplaceNoise(scale=(0, 0.2*255))
i_img = create_transform(gray, trans, 'AdditiveLaplaceNoise', i_img)
#	AdditivePoissonNoise
trans = iaa.AdditivePoissonNoise(lam=(0, 40*255))
i_img = create_transform(gray, trans, 'AdditivePoissonNoise', i_img)
#	Multiply
trans = iaa.Multiply((0.5, 1.5))
i_img = create_transform(gray, trans, 'Multiply', i_img)
#	MultiplyElementwise
trans = iaa.Multiply((0.5, 1.5))
i_img = create_transform(gray, trans, 'MultiplyElementwise', i_img)
#	Cutout
trans = iaa.Cutout(nb_iterations=2)
i_img = create_transform(gray, trans, 'Cutout', i_img)
#	Dropout
trans = iaa.Dropout(p=(0, 0.2))
i_img = create_transform(gray, trans, 'Dropout', i_img)
#	CoarseDropout
trans = iaa.CoarseDropout((0.0, 0.05), size_percent=(0.02, 0.25))
i_img = create_transform(gray, trans, 'CoarseDropout', i_img)
#	Dropout2D
trans = iaa.Dropout2d(p=0.5)
i_img = create_transform(gray, trans, 'Dropout2D', i_img)
#	TotalDropout
trans = iaa.TotalDropout(1.0)
i_img = create_transform(gray, trans, 'TotalDropout', i_img)
#	ReplaceElementwise
trans = iaa.ReplaceElementwise(0.1, [0, 255])
i_img = create_transform(gray, trans, 'ReplaceElementwise', i_img)
#	ImpulseNoise
trans = iaa.ImpulseNoise(0.1)
i_img = create_transform(gray, trans, 'ImpulseNoise', i_img)
#	SaltAndPepper
trans = iaa.SaltAndPepper(0.1)
i_img = create_transform(gray, trans, 'SaltAndPepper', i_img)
#	CoarseSaltAndPepper
trans = iaa.CoarseSaltAndPepper(0.05, size_percent=(0.01, 0.1))
i_img = create_transform(gray, trans, 'CoarseSaltAndPepper', i_img)
#	Salt
trans = iaa.Salt(0.1)
i_img = create_transform(gray, trans, 'Salt', i_img)
#	CoarseSalt
trans = iaa.CoarseSalt(0.05, size_percent=(0.01, 0.1))
i_img = create_transform(gray, trans, 'CoarseSalt', i_img)
#	Pepper
trans = iaa.Pepper(0.1)
i_img = create_transform(gray, trans, 'Pepper', i_img)
#	CoarsePepper
trans = iaa.CoarsePepper(0.05, size_percent=(0.01, 0.1))
i_img = create_transform(gray, trans, 'CoarsePepper', i_img)
#	Invert
trans = iaa.Invert(0.5)
i_img = create_transform(gray, trans, 'Invert', i_img)
#	Solarize
trans = iaa.Solarize(0.5, threshold=(32, 128))
i_img = create_transform(gray, trans, 'Solarize', i_img)
#	JpegCompression
trans = iaa.JpegCompression(compression=(40, 50))
i_img = create_transform(gray, trans, 'JpegCompression', i_img)

'''
#------------------
#	artistic		
#------------------
#	Cartoon
trans = iaa.Cartoon()
i_img = create_transform(gray, trans, 'Cartoon', i_img)

#------------------
#	blend		
#------------------
#	BlendAlpha
trans = iaa.BlendAlpha(0.5, iaa.Grayscale(1.0))
i_img = create_transform(gray, trans, 'BlendAlpha', i_img)
#	BlendAlphaMask
trans = iaa.BlendAlphaMask(    iaa.InvertMaskGen(0.5, iaa.VerticalLinearGradientMaskGen()),    iaa.Clouds())
i_img = create_transform(gray, trans, 'BlendAlphaMask', i_img)
#	BlendAlphaElementwise
trans = iaa.BlendAlphaElementwise(0.5, iaa.Grayscale(1.0))
i_img = create_transform(gray, trans, 'BlendAlphaElementwise', i_img)
#	BlendAlphaSimplexNoise
trans = iaa.BlendAlphaSimplexNoise(iaa.EdgeDetect(1.0))
i_img = create_transform(gray, trans, 'BlendAlphaSimplexNoise', i_img)
#	BlendAlphaFrequencyNoise
#trans = iaa.BlendAlphaFrequencyNoise(first=iaa.EdgeDetect(1.0))
#i_img = create_transform(gray, trans, 'BlendAlphaFrequencyNoise', i_img)
#	BlendAlphaSomeColors
trans = iaa.BlendAlphaSomeColors(iaa.Grayscale(1.0))
i_img = create_transform(gray, trans, 'BlendAlphaSomeColors', i_img)
#	BlendAlphaHorizontalLinearGradient
trans = iaa.BlendAlphaHorizontalLinearGradient(iaa.AddToHue((-100, 100)))
i_img = create_transform(gray, trans, 'BlendAlphaHorizontalLinearGradient', i_img)
#	BlendAlphaVerticalLinearGradient
trans = iaa.BlendAlphaVerticalLinearGradient(iaa.AddToHue((-100, 100)))
i_img = create_transform(gray, trans, 'BlendAlphaVerticalLinearGradient', i_img)
#	BlendAlphaRegularGrid
trans = iaa.BlendAlphaRegularGrid(nb_rows=(4, 6), nb_cols=(1, 4),foreground=iaa.Multiply(0.0))
i_img = create_transform(gray, trans, 'BlendAlphaRegularGrid', i_img)
#	BlendAlphaCheckerboard
trans = iaa.BlendAlphaCheckerboard(nb_rows=2, nb_cols=(1, 4), foreground=iaa.AddToHue((-100, 100)))
i_img = create_transform(gray, trans, 'BlendAlphaCheckerboard', i_img)
#	BlendAlphaSegMapClassIds
#trans = iaa.BlendAlphaSegMapClassIds([1, 3], foreground=iaa.AddToHue((-100, 100)))
#i_img = create_transform(gray, trans, 'BlendAlphaSegMapClassIds', i_img)
#	BlendAlphaBoundingBoxes
#trans = iaa.BlendAlphaBoundingBoxes("person",  foreground=iaa.Grayscale(1.0))
#i_img = create_transform(gray, trans, 'BlendAlphaBoundingBoxes', i_img)

#------------------
#	blur		
#------------------
#	GaussianBlur
trans = iaa.GaussianBlur(sigma=(1.0, 3.0))
i_img = create_transform(gray, trans, 'GaussianBlur', i_img)
#	AverageBlur
trans = iaa.AverageBlur(k=(2, 11))
i_img = create_transform(gray, trans, 'AverageBlur', i_img)
#	MedianBlur
trans = iaa.MedianBlur(k=(3, 11))
i_img = create_transform(gray, trans, 'MedianBlur', i_img)
#	BilateralBlur
trans = iaa.BilateralBlur(    d=(3, 10), sigma_color=(10, 250), sigma_space=(10, 250))
i_img = create_transform(gray, trans, 'BilateralBlur', i_img)
#	MotionBlur
trans = iaa.MotionBlur(k=15)
i_img = create_transform(gray, trans, 'MotionBlur', i_img)
#	MeanShiftBlur
trans = iaa.MeanShiftBlur()
i_img = create_transform(gray, trans, 'MeanShiftBlur', i_img)

#------------------
#	collections		
#------------------
#	RandAugment
# i_img = create_transform(gray, trans, 'RandAugment', i_img)


#------------------
#	color		
#------------------
#	WithColorspace
trans = iaa.WithColorspace(
    to_colorspace="HSV",
    from_colorspace="RGB",
    children=iaa.WithChannels(0, iaa.Add((0, 50)) )
)
i_img = create_transform(gray, trans, 'WithColorspace', i_img)
#	WithBrightnessChannels
trans = iaa.WithBrightnessChannels(iaa.Add((-50, 50)))
i_img = create_transform(gray, trans, 'WithBrightnessChannels', i_img)
#	MultiplyAndAddToBrightness
trans = iaa.MultiplyAndAddToBrightness(mul=(0.5, 1.5), add=(-30, 30))
i_img = create_transform(gray, trans, 'MultiplyAndAddToBrightness', i_img)
#	MultiplyBrightness
trans = iaa.MultiplyBrightness((0.5, 1.5))
i_img = create_transform(gray, trans, 'MultiplyBrightness', i_img)
#	AddToBrightness
trans = iaa.AddToBrightness((-30, 30))
i_img = create_transform(gray, trans, 'AddToBrightness', i_img)
#	WithHueAndSaturation
trans = iaa.WithHueAndSaturation( iaa.WithChannels(0, iaa.Add((0, 50))) )
i_img = create_transform(gray, trans, 'WithHueAndSaturation', i_img)
#	MultiplyHueAndSaturation
trans = iaa.MultiplyHueAndSaturation((0.5, 1.5), per_channel=True)
i_img = create_transform(gray, trans, 'MultiplyHueAndSaturation', i_img)
#	MultiplyHue
trans = iaa.MultiplyHue((0.5, 1.5))
i_img = create_transform(gray, trans, 'MultiplyHue', i_img)
#	MultiplySaturation
trans = iaa.MultiplySaturation((0.5, 1.5))
i_img = create_transform(gray, trans, 'MultiplySaturation', i_img)
#	RemoveSaturation
trans = iaa.RemoveSaturation()
i_img = create_transform(gray, trans, 'RemoveSaturation', i_img)
#	AddToHueAndSaturation
trans = iaa.AddToHueAndSaturation((-50, 50), per_channel=True)
i_img = create_transform(gray, trans, 'AddToHueAndSaturation', i_img)
#	AddToHue
trans = iaa.AddToHue((-50, 50))
i_img = create_transform(gray, trans, 'AddToHue', i_img)
#	AddToSaturation
trans = iaa.AddToSaturation((-50, 50))
i_img = create_transform(gray, trans, 'AddToSaturation', i_img)
#	ChangeColorspace
trans = iaa.Sequential([
    iaa.ChangeColorspace(from_colorspace="RGB", to_colorspace="HSV"),
    iaa.WithChannels(0, iaa.Add((50, 100))),
    iaa.ChangeColorspace(from_colorspace="HSV", to_colorspace="RGB")
])
i_img = create_transform(gray, trans, 'ChangeColorspace', i_img)
#	Grayscale
trans = iaa.Grayscale(alpha=(0.5, 0.6))
i_img = create_transform(gray, trans, 'Grayscale', i_img)
#	ChangeColorTemperature
trans = iaa.ChangeColorTemperature((1100, 10000))
i_img = create_transform(gray, trans, 'ChangeColorTemperature', i_img)
#	KMeansColorQuantization
trans = iaa.KMeansColorQuantization()
i_img = create_transform(gray, trans, 'KMeansColorQuantization', i_img)
#	UniformColorQuantization
trans = iaa.UniformColorQuantization()
i_img = create_transform(gray, trans, 'UniformColorQuantization', i_img)
#	UniformColorQuantizationToNBits
trans = iaa.UniformColorQuantizationToNBits()
i_img = create_transform(gray, trans, 'UniformColorQuantizationToNBits', i_img)
#	Posterize
# i_img = create_transform(gray, trans, 'Posterize', i_img)

#------------------
#	contrast		
#------------------
#	GammaContrast
trans = iaa.GammaContrast((0.5, 2.0))
i_img = create_transform(gray, trans, 'GammaContrast', i_img)
#	SigmoidContrast
trans = iaa.SigmoidContrast(gain=(3, 10), cutoff=(0.4, 0.6))
i_img = create_transform(gray, trans, 'SigmoidContrast', i_img)
#	LogContrast
trans = iaa.LogContrast(gain=(0.6, 1.4))
i_img = create_transform(gray, trans, 'LogContrast', i_img)
#	LinearContrast
trans = iaa.LinearContrast((0.4, 1.6))
i_img = create_transform(gray, trans, 'LinearContrast', i_img)
#	AllChannelsCLAHE
trans = iaa.AllChannelsCLAHE()
i_img = create_transform(gray, trans, 'AllChannelsCLAHE', i_img)
#	CLAHE
trans = iaa.CLAHE()
i_img = create_transform(gray, trans, 'CLAHE', i_img)
#	AllChannelsHistogramEqualization
trans = iaa.AllChannelsHistogramEqualization()
i_img = create_transform(gray, trans, 'AllChannelsHistogramEqualization', i_img)
#	HistogramEqualization
trans = iaa.HistogramEqualization()
i_img = create_transform(gray, trans, 'HistogramEqualization', i_img)

#------------------
#	convolutional	
#------------------	
#	Convolve
matrix = np.array([[0, -1, 0],
                   [-1, 4, -1],
                   [0, -1, 0]])
trans = iaa.Convolve(matrix=matrix)
i_img = create_transform(gray, trans, 'Convolve', i_img)
#	Sharpen
trans = iaa.Sharpen(alpha=(0.5, 0.6), lightness=(0.75, 2.0))
i_img = create_transform(gray, trans, 'Sharpen', i_img)
#	Emboss
trans = iaa.Emboss(alpha=(0.5, 0.6), strength=(0.5, 1.5))
i_img = create_transform(gray, trans, 'Emboss', i_img)
#	EdgeDetect
trans = iaa.EdgeDetect(alpha=(0.5, 0.6))
i_img = create_transform(gray, trans, 'EdgeDetect', i_img)
#	DirectedEdgeDetect
trans = iaa.DirectedEdgeDetect(alpha=(0.5, 0.6), direction=(0.5, 0.6))
i_img = create_transform(gray, trans, 'DirectedEdgeDetect', i_img)

#------------------	
#	debug		
#------------------	
#	SaveDebugImageEveryNBatches
# i_img = create_transform(gray, trans, 'SaveDebugImageEveryNBatches', i_img)

#------------------	
#	edges		
#------------------	
#	Canny
trans = iaa.Canny()
i_img = create_transform(gray, trans, 'Canny', i_img)

#------------------
#	flip		
#------------------
trans = iaa.Fliplr(1)
i_img = create_transform(gray, trans, 'HorizontalFlip', i_img)
#	Flipud
trans = iaa.Flipud(1)
i_img = create_transform(gray, trans, 'VericalFlip', i_img)


#------------------
#	geometric		
#------------------
#	Affine
trans = iaa.Affine(scale=(0.5, 1.5))
i_img = create_transform(gray, trans, 'Affine', i_img)
#	ScaleX
trans = iaa.ScaleX((0.5, 1.5))
i_img = create_transform(gray, trans, 'ScaleX', i_img)
#	ScaleY
trans = iaa.ScaleY((0.5, 1.5))
i_img = create_transform(gray, trans, 'ScaleY', i_img)
#	TranslateX
trans = iaa.TranslateX(px=(-20, 20))
i_img = create_transform(gray, trans, 'TranslateX', i_img)
#	TranslateY
trans = iaa.TranslateY(px=(-20, 20))
i_img = create_transform(gray, trans, 'TranslateY', i_img)
#	Rotate
trans = iaa.Rotate((-45, 45))
i_img = create_transform(gray, trans, 'Rotate', i_img)
#	ShearX
trans = iaa.ShearX((-20, 20))
i_img = create_transform(gray, trans, 'ShearX', i_img)
#	ShearY
trans = iaa.ShearY((-20, 20))
i_img = create_transform(gray, trans, 'ShearY', i_img)
#	PiecewiseAffine
trans = iaa.PiecewiseAffine(scale=(0.01, 0.05))
i_img = create_transform(gray, trans, 'PiecewiseAffine', i_img)
#	PerspectiveTransform
trans = iaa.PerspectiveTransform(scale=(0.1, 0.15))
i_img = create_transform(gray, trans, 'PerspectiveTransform', i_img)
#	ElasticTransformation
trans = iaa.ElasticTransformation(alpha=(0, 5.0), sigma=0.25)
i_img = create_transform(gray, trans, 'ElasticTransformation', i_img)
#	Rot90
trans = iaa.Rot90(1)
i_img = create_transform(gray, trans, 'Rot90', i_img)
#	WithPolarWarping
trans = iaa.WithPolarWarping(
    iaa.Affine(
        translate_percent={"x": (-0.1, 0.1), "y": (-0.1, 0.1)}
    )
)
i_img = create_transform(gray, trans, 'WithPolarWarping', i_img)

trans = iaa.WithPolarWarping(iaa.AveragePooling((2, 8)))
i_img = create_transform(gray, trans, 'WithPolarWarping2', i_img)
#	Jigsaw
trans = iaa.Jigsaw(nb_rows=10, nb_cols=10)
i_img = create_transform(gray, trans, 'Jigsaw', i_img)


#------------------
#	imgcorruptlike		
#------------------
#	GaussianNoise
trans = iaa.imgcorruptlike.GaussianNoise(severity=2)
i_img = create_transform(gray, trans, 'GaussianNoise', i_img)
#	ShotNoise
trans = iaa.imgcorruptlike.ShotNoise(severity=2)
i_img = create_transform(gray, trans, 'ShotNoise', i_img)
#	ImpulseNoise
trans = iaa.imgcorruptlike.ImpulseNoise(severity=2)
i_img = create_transform(gray, trans, 'ImpulseNoise', i_img)
#	SpeckleNoise
trans = iaa.imgcorruptlike.SpeckleNoise(severity=2)
i_img = create_transform(gray, trans, 'SpeckleNoise', i_img)
#	GaussianBlur
trans = iaa.imgcorruptlike.GaussianBlur(severity=2)
i_img = create_transform(gray, trans, 'GaussianBlur', i_img)
#	GlassBlur
trans = iaa.imgcorruptlike.GlassBlur(severity=2)
i_img = create_transform(gray, trans, 'GlassBlur', i_img)
#	DefocusBlur
trans = iaa.imgcorruptlike.DefocusBlur(severity=2)
i_img = create_transform(gray, trans, 'DefocusBlur', i_img)
#	MotionBlur
trans = iaa.imgcorruptlike.MotionBlur(severity=2)
i_img = create_transform(gray, trans, 'MotionBlur', i_img)
#	ZoomBlur
trans = iaa.imgcorruptlike.ZoomBlur(severity=2)
i_img = create_transform(gray, trans, 'ZoomBlur', i_img)
#	Fog
trans = iaa.imgcorruptlike.Fog(severity=2)
i_img = create_transform(gray, trans, 'Fog', i_img)
#	Frost
trans = iaa.imgcorruptlike.Frost(severity=2)
i_img = create_transform(gray, trans, 'Frost', i_img)
#	Snow
trans = iaa.imgcorruptlike.Snow(severity=2)
i_img = create_transform(gray, trans, 'Snow', i_img)
#	Spatter
trans = iaa.imgcorruptlike.Spatter(severity=2)
i_img = create_transform(gray, trans, 'Spatter', i_img)
#	Contrast
trans = iaa.imgcorruptlike.Contrast(severity=2)
i_img = create_transform(gray, trans, 'Contrast', i_img)
#	Brightness
trans = iaa.imgcorruptlike.Brightness(severity=2)
i_img = create_transform(gray, trans, 'Brightness', i_img)
#	Saturate
trans = iaa.imgcorruptlike.Saturate(severity=2)
i_img = create_transform(gray, trans, 'Saturate', i_img)
#	JpegCompression
trans = iaa.imgcorruptlike.JpegCompression(severity=2)
i_img = create_transform(gray, trans, 'JpegCompression', i_img)
#	Pixelate
trans = iaa.imgcorruptlike.Pixelate(severity=2)
i_img = create_transform(gray, trans, 'Pixelate', i_img)
#	ElasticTransform
trans = iaa.imgcorruptlike.ElasticTransform(severity=2)
i_img = create_transform(gray, trans, 'ElasticTransform', i_img)


#------------------
#	pillike		
#------------------
#	Solarize
trans = iaa.Solarize(0.5, threshold=(32, 128))
i_img = create_transform(gray, trans, 'Solarize', i_img)
#	Posterize
# i_img = create_transform(gray, trans, 'Posterize', i_img)
#	Equalize
trans = iaa.pillike.Equalize()
i_img = create_transform(gray, trans, 'Equalize', i_img)
#	Autocontrast
trans = iaa.pillike.Autocontrast()
i_img = create_transform(gray, trans, 'Autocontrast', i_img)
#	EnhanceColor
trans = iaa.pillike.EnhanceColor()
i_img = create_transform(gray, trans, 'EnhanceColor', i_img)
#	EnhanceContrast
trans = iaa.pillike.EnhanceContrast()
i_img = create_transform(gray, trans, 'EnhanceContrast', i_img)
#	EnhanceBrightness
trans = iaa.pillike.EnhanceBrightness()
i_img = create_transform(gray, trans, 'EnhanceBrightness', i_img)
#	EnhanceSharpness
trans = iaa.pillike.EnhanceSharpness()
i_img = create_transform(gray, trans, 'EnhanceSharpness', i_img)
#	FilterBlur
trans = iaa.pillike.FilterBlur()
i_img = create_transform(gray, trans, 'FilterBlur', i_img)
#	FilterSmooth
trans = iaa.pillike.FilterSmooth()
i_img = create_transform(gray, trans, 'FilterSmooth', i_img)
#	FilterSmoothMore
trans = iaa.pillike.FilterSmoothMore()
i_img = create_transform(gray, trans, 'FilterSmoothMore', i_img)
#	FilterEdgeEnhance
trans = iaa.pillike.FilterEdgeEnhance()
i_img = create_transform(gray, trans, 'FilterEdgeEnhance', i_img)
#	FilterEdgeEnhanceMore
trans = iaa.pillike.FilterEdgeEnhanceMore()
i_img = create_transform(gray, trans, 'FilterEdgeEnhanceMore', i_img)
#	FilterFindEdges
trans = iaa.pillike.FilterFindEdges()
i_img = create_transform(gray, trans, 'FilterFindEdges', i_img)
#	FilterContour
trans = iaa.pillike.FilterContour()
i_img = create_transform(gray, trans, 'FilterContour', i_img)
#	FilterEmboss
trans = iaa.pillike.FilterEmboss()
i_img = create_transform(gray, trans, 'FilterEmboss', i_img)
#	FilterSharpen
trans = iaa.pillike.FilterSharpen()
i_img = create_transform(gray, trans, 'FilterSharpen', i_img)
#	FilterDetail
trans = iaa.pillike.FilterDetail()
i_img = create_transform(gray, trans, 'FilterDetail', i_img)
#	Affine
trans = iaa.pillike.Affine(scale={"x": (0.8, 1.2), "y": (0.5, 1.5)})
i_img = create_transform(gray, trans, 'Affine', i_img)

#------------------
#	pooling		
#------------------
#	AveragePooling
trans = iaa.AveragePooling([4, 6])
i_img = create_transform(gray, trans, 'AveragePooling', i_img)
#	MaxPooling
trans = iaa.MaxPooling(2)
i_img = create_transform(gray, trans, 'MaxPooling', i_img)
#	MinPooling
trans = iaa.MinPooling(2)
i_img = create_transform(gray, trans, 'MinPooling', i_img)
#	MedianPooling
trans = iaa.MedianPooling(2)
i_img = create_transform(gray, trans, 'MedianPooling', i_img)

#------------------
#	segmentation	
#------------------	
#	Superpixels
trans = iaa.Superpixels(p_replace=0.5, n_segments=64)
i_img = create_transform(gray, trans, 'Superpixels', i_img)
#	Voronoi
points_sampler = iaa.RegularGridPointsSampler(n_cols=20, n_rows=40)
trans = iaa.Voronoi(points_sampler)
i_img = create_transform(gray, trans, 'Voronoi', i_img)
#	UniformVoronoi
trans = iaa.UniformVoronoi((100, 500))
i_img = create_transform(gray, trans, 'UniformVoronoi', i_img)
#	RegularGridVoronoi
trans = iaa.RegularGridVoronoi(10, 20)
i_img = create_transform(gray, trans, 'RegularGridVoronoi', i_img)
#	RelativeRegularGridVoronoi
trans = iaa.RelativeRegularGridVoronoi(0.1, 0.25)
i_img = create_transform(gray, trans, 'RelativeRegularGridVoronoi', i_img)

#------------------
#	size		
#------------------
#	Resize
trans = iaa.Resize((0.5, 0.8))
i_img = create_transform(gray, trans, 'Resize', i_img)
#	CropAndPad
trans = iaa.CropAndPad(percent=(-0.25, 0.25))
i_img = create_transform(gray, trans, 'CropAndPad', i_img)
#	Pad
# i_img = create_transform(gray, trans, 'Pad', i_img)
#	Crop
# i_img = create_transform(gray, trans, 'Crop', i_img)
#	PadToFixedSize
trans = iaa.PadToFixedSize(width=100, height=100)
i_img = create_transform(gray, trans, 'PadToFixedSize', i_img)
#	CropToFixedSize
trans = iaa.CropToFixedSize(width=100, height=100)
i_img = create_transform(gray, trans, 'CropToFixedSize', i_img)
#	PadToMultiplesOf
trans = iaa.PadToMultiplesOf(height_multiple=10, width_multiple=6)
i_img = create_transform(gray, trans, 'PadToMultiplesOf', i_img)
#	CropToMultiplesOf
trans = iaa.CropToMultiplesOf(height_multiple=10, width_multiple=6)
i_img = create_transform(gray, trans, 'CropToMultiplesOf', i_img)
#	CropToPowersOf
trans = iaa.CropToPowersOf(height_base=3, width_base=2)
i_img = create_transform(gray, trans, 'CropToPowersOf', i_img)
#	PadToPowersOf
trans = iaa.PadToPowersOf(height_base=3, width_base=2)
i_img = create_transform(gray, trans, 'PadToPowersOf', i_img)
#	CropToAspectRatio
trans = iaa.CropToAspectRatio(2.0)
i_img = create_transform(gray, trans, 'CropToAspectRatio', i_img)
#	PadToAspectRatio
trans = iaa.PadToAspectRatio(2.0)
i_img = create_transform(gray, trans, 'PadToAspectRatio', i_img)
#	CropToSquare
trans = iaa.CropToSquare()
i_img = create_transform(gray, trans, 'CropToSquare', i_img)
#	PadToSquare
trans = iaa.PadToSquare()
i_img = create_transform(gray, trans, 'PadToSquare', i_img)
#	CenterPadToFixedSize
trans = iaa.CenterPadToFixedSize(height=20, width=30)
i_img = create_transform(gray, trans, 'CenterPadToFixedSize', i_img)
#	CenterCropToFixedSize
crop = iaa.CenterCropToFixedSize(height=20, width=10)
i_img = create_transform(gray, trans, 'CenterCropToFixedSize', i_img)
#	CenterCropToMultiplesOf
trans = iaa.CenterCropToMultiplesOf(height_multiple=10, width_multiple=6)
i_img = create_transform(gray, trans, 'CenterCropToMultiplesOf', i_img)
#	CenterPadToMultiplesOf
trans = iaa.CenterPadToMultiplesOf(height_multiple=10, width_multiple=6)
i_img = create_transform(gray, trans, 'CenterPadToMultiplesOf', i_img)
#	CenterCropToPowersOf
trans = iaa.CropToPowersOf(height_base=3, width_base=2)
i_img = create_transform(gray, trans, 'CenterCropToPowersOf', i_img)
#	CenterPadToPowersOf
trans = iaa.CenterPadToPowersOf(height_base=3, width_base=2)
i_img = create_transform(gray, trans, 'CenterPadToPowersOf', i_img)
#	CenterCropToAspectRatio
trans = iaa.CenterCropToAspectRatio(2.0)
i_img = create_transform(gray, trans, 'CenterCropToAspectRatio', i_img)
#	CenterPadToAspectRatio
trans = iaa.PadToAspectRatio(2.0)
i_img = create_transform(gray, trans, 'CenterPadToAspectRatio', i_img)
#	CenterCropToSquare
trans = iaa.CenterCropToSquare()
i_img = create_transform(gray, trans, 'CenterCropToSquare', i_img)
#	CenterPadToSquare
trans = iaa.CenterPadToSquare()
i_img = create_transform(gray, trans, 'CenterPadToSquare', i_img)
#	KeepSizeByResize
trans = iaa.KeepSizeByResize(
    iaa.Crop((20, 40), keep_size=False)
)
i_img = create_transform(gray, trans, 'KeepSizeByResize', i_img)


#------------------
#	weather
#------------------
#	FastSnowyLandscape
trans = iaa.FastSnowyLandscape(
    lightness_threshold=140,
    lightness_multiplier=2.5
)
i_img = create_transform(gray, trans, 'FastSnowyLandscape', i_img)
#	Clouds
trans = iaa.Clouds()
i_img = create_transform(gray, trans, 'Clouds', i_img)
#	Fog
trans = iaa.Fog()
i_img = create_transform(gray, trans, 'Fog', i_img)
#	CloudLayer
#i_img = create_transform(gray, trans, 'CloudLayer', i_img)
#	Snowflakes
trans = iaa.Snowflakes(flake_size=(0.1, 0.4), speed=(0.01, 0.05))
i_img = create_transform(gray, trans, 'Snowflakes', i_img)
#	SnowflakesLayer
i_img = create_transform(gray, trans, 'SnowflakesLayer', i_img)
#	Rain
trans = iaa.Rain(speed=(0.1, 0.3))
i_img = create_transform(gray, trans, 'Rain', i_img)
#	RainLayer
#i_img = create_transform(gray, trans, 'RainLayer', i_img)


'''