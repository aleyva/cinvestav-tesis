
import os
from skimage.io import imread
import numpy as np
import imageio
import imgaug as ia
import imgaug.augmenters as iaa
from imgaug.augmentables.segmaps import SegmentationMapsOnImage
from datetime import datetime


def rand():
    return np.random.random_sample()

def load_imgs():
    base_path = '/mnt/Home/Proyectos/Maestria/14-tesis/dicoms/dicoms-v3/test/'
    mask_file = os.path.join(base_path,'mask/0007-mask-20190722-F-059Y-1mm-B20f.png')    
    gray_file = os.path.join(base_path,'gray/0007-sgray-20190722-F-059Y-1mm-B20f.png')

    gray = imread(  gray_file  )
    #mask = imread(  mask_file  )
    mask = imread(  mask_file  )[:,:,1]

    return gray, mask

def save_image(images_aug, segmaps_aug, filename='example_segmaps.jpg'):
    # We want to generate an image containing the original input image and
    # segmentation maps before/after augmentation. (Both multiple times for
    # multiple augmentations.)
    #
    # The whole image is supposed to have five columns:
    # (1) original image,
    # (2) original image with segmap,
    # (3) augmented image,
    # (4) augmented segmap on augmented image,
    # (5) augmented segmap on its own in.
    #
    # We now generate the cells of these columns.
    #
    # Note that draw_on_image() and draw() both return lists of drawn
    # images. Assuming that the segmentation map array has shape (H,W,C),
    # the list contains C items.
    cells = []
    for image_aug, segmap_aug in zip(images_aug, segmaps_aug):
        cells.append(gray)                                         # column 1
        cells.append(segmap.draw_on_image(gray)[0])                # column 2
        cells.append(image_aug)                                     # column 3
        cells.append(segmap_aug.draw_on_image(image_aug)[0])        # column 4
        cells.append(segmap_aug.draw(size=image_aug.shape[:2])[0])  # column 5
    
    # Convert cells to a grid image and save.
    grid_image = ia.draw_grid(cells, cols=5)
    imageio.imwrite(filename, grid_image)
    

# load images
gray, mask = load_imgs()

# set the seeds
np.random.seed(1)
ia.seed(1)

#------------------
#   probability
#------------------
# set value between 0 and 1, ex 0.3
prob_scale = 0
prob_rotate = 1
prob_shear = 0
prob_translate = 0
prob_flip = 0
prob_perspective = 0
prob_piecewise = 0
prob_blur = 0  # if blur is set, the sharpen will not be enable
prob_sharpen = 0
prob_noise = 0
prob_coarse_dropout = 0  # cutout, dropout or coarseDropout?


#------------------
#   parameters
#------------------
val_scale = (0.8, 1.2) # ¿separar en x y y?  scale={"x": (0.5, 1.5), "y": (0.5, 1.5)}
val_rotate = (-90, 90)
val_shear = (-16, 16)
val_translate_percent= {"x": (-0.1, 0.1), "y": (-0.1, 0.1)}
val_flip = 1
val_perspective = (0.03, 0.075)
val_piecewise = (0.01, 0.05)
val_blur = (4, 7)  #programar para aplicar blur o sharpen
val_sharpen = (0.03, 0.12)
val_noise = False
val_coarse_dropout = False


#------------------
#   sequence the transformations
#------------------
transform_arr = []

if rand() <= prob_scale:
    transform_arr.append( iaa.Affine( scale=val_scale ))
if rand() <= prob_rotate:        
    transform_arr.append( iaa.Affine( rotate=val_rotate ))
if rand() <= prob_shear:         
    transform_arr.append( iaa.Affine( shear=val_shear ))
if rand() <=  prob_translate:     
    transform_arr.append( iaa.Affine( translate_percent=val_translate_percent ))
if rand() <=  prob_flip:          
    transform_arr.append( iaa.Fliplr( 1 ) )
if rand() <=  prob_perspective:   
    transform_arr.append( iaa.PerspectiveTransform( scale=val_perspective ) )
if rand() <=  prob_piecewise:     
    transform_arr.append( iaa.PiecewiseAffine( scale=val_piecewise ) )
if rand() <=  prob_blur:
    transform_arr.append( iaa.BilateralBlur(d=val_blur ) )
if rand() <=  prob_sharpen:
    transform_arr.append( iaa.Sharpen(alpha=val_sharpen) )
if rand() <=  prob_noise:
    #	AdditiveGaussianNoise
    #transform_arr.append( iaa.AdditiveGaussianNoise(scale=(0, 0.1*255)) )
    #	AdditiveLaplaceNoise   between salt and peper and gaussian
    #transform_arr.append( iaa.AdditiveLaplaceNoise(scale=(0, 0.08*255)) )
    #	ReplaceElementwise
    #transform_arr.append( iaa.ReplaceElementwise(0.02, [0, 255]) )
    #	SaltAndPepper
    #transform_arr.append( iaa.SaltAndPepper(0.05) )
    #	Salt
    #transform_arr.append( iaa.Salt(0.03) )
    #	Pepper
    transform_arr.append( iaa.Pepper(0.05) ) # este podría ser
if rand() <=  prob_coarse_dropout:    
    # esta transformación se lo aplicaría a la mascara y eso sería incorrecto
    transform_arr.append( iaa.CoarseDropout((0.08, 0.1), size_percent=(0.1, 0.4)) )



assert len(transform_arr)>0, 'Nothing to augment'

# create the secuential transform
seq = iaa.Sequential(iaa.Rotate((0, 90)))

# create the mask
segmap = np.zeros((512, 512, 1), dtype=np.int32)
segmap[mask > 100] = 2
segmap = SegmentationMapsOnImage(segmap, shape=gray.shape)

   
# Augment images and segmaps.
images_aug = []
segmaps_aug = []
for _ in range(5):
    images_aug_i, segmaps_aug_i = seq(image=gray, segmentation_maps=segmap)
    images_aug.append(images_aug_i)
    segmaps_aug.append(segmaps_aug_i)



save_image(images_aug, segmaps_aug, filename='example_segmaps.jpg')
